/**
 * @file rpk_zapi.h
 *
 * RAPACK double complex precision header.
 *
 * @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Esragul Korkmaz
 * @author Nolan Bredel
 * @author Abel Calluaud
 * @date 2024-03-11
 * @precisions normal z -> c d s
 *
 */
#ifndef _rpk_zapi_h_
#define _rpk_zapi_h_

#include "rapack.h"

/**
 *
 * @addtogroup kernel_lr
 * @{
 *    This module contains all the low-rank kernels working on rpk_ctx_t
 *    matrix representations.
 *
 *    @name RapackComplex64 low-rank kernels
 *    @{
 */
void rpk_zlralloc( rpk_int_t M, rpk_int_t N, rpk_int_t rkmax, rpk_matrix_t *A );

void rpk_zlrfree( rpk_matrix_t *A );

int rpk_zlrsze( int           copy,
                rpk_int_t     M,
                rpk_int_t     N,
                rpk_matrix_t *A,
                rpk_int_t     newrk,
                rpk_int_t     newrkmax,
                rpk_int_t     rklimit );
int rpk_zlr2ge( rpk_trans_t         trans,
                rpk_int_t           M,
                rpk_int_t           N,
                const rpk_matrix_t *Alr,
                rpk_complex64_t    *A,
                rpk_int_t           lda );

void rpk_zlrcpy( rpk_trans_t         transA,
                 rpk_complex64_t     alpha,
                 rpk_int_t           M1,
                 rpk_int_t           N1,
                 const rpk_matrix_t *A,
                 rpk_int_t           M2,
                 rpk_int_t           N2,
                 rpk_matrix_t       *B,
                 rpk_int_t           offx,
                 rpk_int_t           offy );

void rpk_zlrconcatenate_u( rpk_complex64_t     alpha,
                           rpk_int_t           M1,
                           rpk_int_t           N1,
                           const rpk_matrix_t *A,
                           rpk_int_t           M2,
                           rpk_matrix_t       *B,
                           rpk_int_t           offx,
                           rpk_complex64_t    *u1u2 );
void rpk_zlrconcatenate_v( rpk_trans_t         transA1,
                           rpk_complex64_t     alpha,
                           rpk_int_t           M1,
                           rpk_int_t           N1,
                           const rpk_matrix_t *A,
                           rpk_int_t           N2,
                           rpk_matrix_t       *B,
                           rpk_int_t           offy,
                           rpk_complex64_t    *v1v2 );

double rpk_zlrnrm( rpk_normtype_t      ntype,
                   int                 transV,
                   rpk_int_t           M,
                   rpk_int_t           N,
                   const rpk_matrix_t *A );

size_t      rpk_zlrgetsize( rpk_int_t M, rpk_int_t N, rpk_matrix_t *A );
char       *rpk_zlrpack( rpk_int_t M, rpk_int_t N, const rpk_matrix_t *A, char *buffer );
char       *rpk_zlrunpack( rpk_int_t M, rpk_int_t N, rpk_matrix_t *A, char *buffer );
const char *rpk_zlrunpack2( rpk_int_t     M,
                            rpk_int_t     N,
                            rpk_matrix_t *A,
                            const char   *input,
                            char        **outptr );

/**
 *    @}
 * @}
 *
 * @addtogroup kernel_lr_gemm
 * @{
 *    This are the kernels to compute the low-rank updates
 *
 *    @name RapackComplex64 LRMM low-rank kernels
 *    @{
 */

/**
 * @brief Structure to store all the parameters of the rpk_zgemm family functions
 */
typedef struct rpk_zgemm_s {
    rpk_trans_t         transA;  /**< Specify op(A) and is equal to RapackNoTrans, RapackTrans, or RapackConjTrans */
    rpk_trans_t         transB;  /**< Specify op(B) and is equal to RapackNoTrans, RapackTrans, or RapackConjTrans */
    rpk_int_t           M;       /**< Number of rows     of the A matrix                                    */
    rpk_int_t           N;       /**< Number of columns  of the B matrix                                    */
    rpk_int_t           K;       /**< Number of columns  of the A matrix (= number of rows of the B matrix) */
    rpk_int_t           Cm;      /**< Number of rows     of the C matrix that receives the AB contribution  */
    rpk_int_t           Cn;      /**< Number of columns  of the C matrix that receives the AB contribution  */
    rpk_int_t           offx;    /**< Horizontal offsets of the AB product in the C matrix                  */
    rpk_int_t           offy;    /**< Vertical   offsets of the AB product in the C matrix                  */
    rpk_complex64_t     alpha;   /**< The alpha factor                                                      */
    const rpk_matrix_t *A;       /**< The A matrix described in a low-rank structure                        */
    const rpk_matrix_t *B;       /**< The B matrix described in a low-rank structure                        */
    rpk_complex64_t     beta;    /**< The beta factor                                                       */
    rpk_matrix_t       *C;       /**< The C matrix described in a low-rank structure                        */
    rpk_complex64_t    *work;    /**< The pointer to an available workspace                                 */
    rpk_int_t           lwork;   /**< The size of the given workspace                                       */
    rpk_int_t           lwused;  /**< The size of the workspace that is already used                        */
    void               *lock;    /**< The lock to protect the concurrent accesses on the C matrix           */
} rpk_zgemm_t;

/**
 * @brief Initialize all the parameters of the rpk_zgemm family functions to ease the access
 */
#define PASTE_RPK_ZLRMM_PARAMS(_a_)             \
    rpk_trans_t         transA = (_a_)->transA; \
    rpk_trans_t         transB = (_a_)->transB; \
    rpk_int_t           M      = (_a_)->M;      \
    rpk_int_t           N      = (_a_)->N;      \
    rpk_int_t           K      = (_a_)->K;      \
    rpk_int_t           Cm     = (_a_)->Cm;     \
    rpk_int_t           Cn     = (_a_)->Cn;     \
    rpk_int_t           offx   = (_a_)->offx;   \
    rpk_int_t           offy   = (_a_)->offy;   \
    rpk_complex64_t     alpha  = (_a_)->alpha;  \
    const rpk_matrix_t *A      = (_a_)->A;      \
    const rpk_matrix_t *B      = (_a_)->B;      \
    rpk_complex64_t     beta   = (_a_)->beta;   \
    rpk_matrix_t       *C      = (_a_)->C;      \
    rpk_complex64_t    *work   = (_a_)->work;   \
    rpk_int_t           lwork  = (_a_)->lwork;  \
    void               *lock   = (_a_)->lock;

/**
 * @brief Void all the parameters of the rpk_zgemm family functions to silent warnings
 */
#define PASTE_RPK_ZLRMM_VOID			\
    (void)transA;                               \
    (void)transB;                               \
    (void)M;                                    \
    (void)N;                                    \
    (void)K;                                    \
    (void)Cm;                                   \
    (void)Cn;                                   \
    (void)offx;                                 \
    (void)offy;                                 \
    (void)alpha;                                \
    (void)A;                                    \
    (void)B;                                    \
    (void)beta;                                 \
    (void)C;                                    \
    (void)work;                                 \
    (void)lwork;                                \
    (void)lock

/**
 *    @}
 *    @name add_lr Functions to add the AB contribution in a low-rank format to any C matrix
 *    @{
 */
rpk_fixdbl_t rpk_zlradd( rpk_zgemm_t *params, const rpk_matrix_t *AB, int infomask );

/**
 *    @}
 */
rpk_fixdbl_t rpk_zgemm( rpk_zgemm_t *params );

/**
 * @}
 *
 * @addtogroup kernel_lr_svd
 * @{
 *    This is the SVD implementation of the low-rank kernels based on the LAPACK
 *    GESVD function.
 *
 *    @name RapackComplex64 SVD low-rank kernels
 *    @{
 */
rpk_fixdbl_t rpk_zge2lr_svd( rpk_int_t     rklimit,
                             rpk_int_t     m,
                             rpk_int_t     n,
                             const void   *Avoid,
                             rpk_int_t     lda,
                             rpk_matrix_t *Alr );
rpk_fixdbl_t rpk_zrradd_svd( rpk_trans_t         transAu,
                             rpk_trans_t         transAv,
                             const void         *alphaptr,
                             rpk_int_t           M1,
                             rpk_int_t           N1,
                             const rpk_matrix_t *A,
                             rpk_int_t           M2,
                             rpk_int_t           N2,
                             rpk_matrix_t       *B,
                             rpk_int_t           offx,
                             rpk_int_t           offy );

/**
 *    @}
 * @}
 *
 * @addtogroup kernel_lr_rrqr
 * @{
 *    These are the rank-revealing QR implementations to generate the low-rank
 *    representations of a full rank matrix.
 *
 *    @name RapackComplex64 main template to convert a full rank matrix to low-rank
 *    @{
 */

/**
 * @brief TODO
 */
typedef int ( *rpk_zrrqr_cp_t )( double           tol,
                                 rpk_int_t        maxrank,
                                 int              refine,
                                 rpk_int_t        nb,
                                 rpk_int_t        m,
                                 rpk_int_t        n,
                                 rpk_complex64_t *A,
                                 rpk_int_t        lda,
                                 rpk_int_t       *jpvt,
                                 rpk_complex64_t *tau,
                                 rpk_complex64_t *work,
                                 rpk_int_t        lwork,
                                 double          *rwork );

/**
 * @brief TODO
 */
typedef int ( *rpk_zrrqr_rt_t )( double           tol,
                                 rpk_int_t        maxrank,
                                 rpk_int_t        nb,
                                 rpk_int_t        m,
                                 rpk_int_t        n,
                                 rpk_complex64_t *A,
                                 rpk_int_t        lda,
                                 rpk_complex64_t *tau,
                                 rpk_complex64_t *B,
                                 rpk_int_t        ldb,
                                 rpk_complex64_t *tau_b,
                                 rpk_complex64_t *work,
                                 rpk_int_t        lwork,
                                 double           normA );

/**
 *    @}
 *    @name RapackComplex64 Rank Revealing QR kernels for low-rank
 *    @{
 */
rpk_fixdbl_t rpk_zge2lr_pqrcp( rpk_int_t     rklimit,
                               rpk_int_t     m,
                               rpk_int_t     n,
                               const void   *Avoid,
                               rpk_int_t     lda,
                               rpk_matrix_t *Alr );
rpk_fixdbl_t rpk_zrradd_pqrcp( rpk_trans_t         transAu,
                               rpk_trans_t         transAv,
                               const void         *alphaptr,
                               rpk_int_t           M1,
                               rpk_int_t           N1,
                               const rpk_matrix_t *A,
                               rpk_int_t           M2,
                               rpk_int_t           N2,
                               rpk_matrix_t       *B,
                               rpk_int_t           offx,
                               rpk_int_t           offy );

rpk_fixdbl_t rpk_zge2lr_rqrcp( rpk_int_t     rklimit,
                               rpk_int_t     m,
                               rpk_int_t     n,
                               const void   *Avoid,
                               rpk_int_t     lda,
                               rpk_matrix_t *Alr );
rpk_fixdbl_t rpk_zrradd_rqrcp( rpk_trans_t         transAu,
                               rpk_trans_t         transAv,
                               const void         *alphaptr,
                               rpk_int_t           M1,
                               rpk_int_t           N1,
                               const rpk_matrix_t *A,
                               rpk_int_t           M2,
                               rpk_int_t           N2,
                               rpk_matrix_t       *B,
                               rpk_int_t           offx,
                               rpk_int_t           offy );

rpk_fixdbl_t rpk_zge2lr_tqrcp( rpk_int_t     rklimit,
                               rpk_int_t     m,
                               rpk_int_t     n,
                               const void   *Avoid,
                               rpk_int_t     lda,
                               rpk_matrix_t *Alr );
rpk_fixdbl_t rpk_zrradd_tqrcp( rpk_trans_t         transAu,
                               rpk_trans_t         transAv,
                               const void         *alphaptr,
                               rpk_int_t           M1,
                               rpk_int_t           N1,
                               const rpk_matrix_t *A,
                               rpk_int_t           M2,
                               rpk_int_t           N2,
                               rpk_matrix_t       *B,
                               rpk_int_t           offx,
                               rpk_int_t           offy );

rpk_fixdbl_t rpk_zge2lr_rqrrt( rpk_int_t     rklimit,
                               rpk_int_t     m,
                               rpk_int_t     n,
                               const void   *Avoid,
                               rpk_int_t     lda,
                               rpk_matrix_t *Alr );
rpk_fixdbl_t rpk_zge2lr_qrcp( rpk_zrrqr_cp_t rrqrfct,
                              rpk_int_t      rklimit,
                              rpk_int_t      m,
                              rpk_int_t      n,
                              const void    *Avoid,
                              rpk_int_t      lda,
                              rpk_matrix_t  *Alr );
rpk_fixdbl_t rpk_zge2lr_qrrt( rpk_zrrqr_rt_t rrqrfct,
                              rpk_int_t      rklimit,
                              rpk_int_t      m,
                              rpk_int_t      n,
                              const void    *Avoid,
                              rpk_int_t      lda,
                              rpk_matrix_t  *Alr );

rpk_fixdbl_t rpk_zrradd_qr( rpk_zrrqr_cp_t      rrqrfct,
                            rpk_trans_t         transAu,
                            rpk_trans_t         transAv,
                            const void         *alphaptr,
                            rpk_int_t           M1,
                            rpk_int_t           N1,
                            const rpk_matrix_t *A,
                            rpk_int_t           M2,
                            rpk_int_t           N2,
                            rpk_matrix_t       *B,
                            rpk_int_t           offx,
                            rpk_int_t           offy );

void rpk_zgetmo( rpk_int_t              m,
                 rpk_int_t              n,
                 const rpk_complex64_t *A,
                 rpk_int_t              lda,
                 rpk_complex64_t       *B,
                 rpk_int_t              ldb );
int  rpk_zgeadd( rpk_trans_t            trans,
                 rpk_int_t              M,
                 rpk_int_t              N,
                 rpk_complex64_t        alpha,
                 const rpk_complex64_t *A,
                 rpk_int_t              LDA,
                 rpk_complex64_t        beta,
                 rpk_complex64_t       *B,
                 rpk_int_t              LDB );

int rpk_zpqrcp( double           tol,
                rpk_int_t        maxrank,
                int              full_update,
                rpk_int_t        nb,
                rpk_int_t        m,
                rpk_int_t        n,
                rpk_complex64_t *A,
                rpk_int_t        lda,
                rpk_int_t       *jpvt,
                rpk_complex64_t *tau,
                rpk_complex64_t *work,
                rpk_int_t        lwork,
                double          *rwork );
/**
 *    @}
 * @}
 */

void rpk_zgemmt( rpk_uplo_t             uplo,
                 rpk_trans_t            transA,
                 rpk_trans_t            transB,
                 rpk_int_t              m,
                 rpk_int_t              n,
                 rpk_int_t              k,
                 rpk_complex64_t        alpha,
                 const rpk_complex64_t *A,
                 rpk_int_t              lda,
                 const rpk_complex64_t *B,
                 rpk_int_t              ldb,
                 rpk_complex64_t        beta,
                 rpk_complex64_t       *C,
                 rpk_int_t              ldc,
                 rpk_complex64_t       *W );

rpk_fixdbl_t rpk_zlrscl( rpk_complex64_t alpha,
                         rpk_int_t       mA,
                         rpk_int_t       nA,
                         rpk_int_t       mB,
                         rpk_int_t       nB,
                         rpk_matrix_t   *Bra,
                         rpk_int_t       offx,
                         rpk_int_t       offy );

rpk_int_t rpk_zlascal( rpk_uplo_t       uplo,
                       rpk_int_t        m,
                       rpk_int_t        n,
                       rpk_complex64_t  alpha,
                       rpk_complex64_t *A,
                       rpk_int_t        lda );

int rpk_ztradd( rpk_uplo_t             uplo,
                rpk_trans_t            trans,
                rpk_int_t              M,
                rpk_int_t              N,
                rpk_complex64_t        alpha,
                const rpk_complex64_t *A,
                rpk_int_t              LDA,
                rpk_complex64_t        beta,
                rpk_complex64_t       *B,
                rpk_int_t              LDB );

rpk_fixdbl_t rpk_ztrsm( rpk_side_t             side,
                        rpk_uplo_t             uplo,
                        rpk_trans_t            trans,
                        rpk_diag_t             diag,
                        rpk_int_t              m,
                        rpk_int_t              n,
                        rpk_complex64_t        alpha,
                        const rpk_complex64_t *A,
                        rpk_int_t              lda,
                        rpk_matrix_t          *B );

rpk_fixdbl_t rpk_zherk( rpk_uplo_t          uplo,
                        rpk_trans_t         trans,
                        rpk_int_t           n,
                        rpk_int_t           k,
                        double              alpha,
                        const rpk_matrix_t *A,
                        double              beta,
                        rpk_complex64_t    *C,
                        int                 ldc,
                        rpk_complex64_t    *W );

#endif /* _rpk_zapi_h_ */
