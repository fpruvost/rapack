/**
 * @file rpkx_zapi.h
 *
 * RAPACK advanced double complex precision header.
 *
 * @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Abel Calluaud
 * @date 2024-03-11
 * @precisions normal z -> c d s
 *
 */
#ifndef _rpkx_zapi_h_
#define _rpkx_zapi_h_

#include "rapack.h"

/**
 *
 * @addtogroup kernel_lr
 * @{
 *    This module contains all the low-rank kernels working on rpk_ctx_t
 *    matrix representations.
 *
 *    @name RapackComplex64 low-rank kernels
 *    @{
 */
int rpkx_zlrsze( const rpk_ctx_t *ctx,
                 int              copy,
                 rpk_int_t        M,
                 rpk_int_t        N,
                 rpk_matrix_t    *A,
                 rpk_int_t        newrk,
                 rpk_int_t        newrkmax,
                 rpk_int_t        rklimit );

void rpkx_zlrcpy( const rpk_ctx_t    *ctx,
                  rpk_trans_t         transA,
                  rpk_complex64_t     alpha,
                  rpk_int_t           M1,
                  rpk_int_t           N1,
                  const rpk_matrix_t *A,
                  rpk_int_t           M2,
                  rpk_int_t           N2,
                  rpk_matrix_t       *B,
                  rpk_int_t           offx,
                  rpk_int_t           offy );

/**
 *    @}
 * @}
 *
 * @addtogroup kernel_lr_gemm
 * @{
 *    @name add_lr Functions to add the AB contribution in a low-rank format to any C matrix
 *    @{
 */
rpk_fixdbl_t rpkx_zlradd( const rpk_ctx_t    *ctx,
                          rpk_zgemm_t        *params,
                          const rpk_matrix_t *AB,
                          int                 infomask );

/**
 *    @}
 */
rpk_fixdbl_t rpkx_zgemm( const rpk_ctx_t *ctx, rpk_zgemm_t *params );

/**
 * @}
 *
 * @addtogroup kernel_lr_svd
 * @{
 *    This is the SVD implementation of the low-rank kernels based on the LAPACK
 *    GESVD function.
 *
 *    @name RapackComplex64 SVD low-rank kernels
 *    @{
 */
rpk_fixdbl_t rpkx_zge2lr_svd( const rpk_ctx_t *ctx,
                              rpk_int_t        rklimit,
                              rpk_int_t        m,
                              rpk_int_t        n,
                              const void      *Avoid,
                              rpk_int_t        lda,
                              rpk_matrix_t    *Alr );
rpk_fixdbl_t rpkx_zrradd_svd( const rpk_ctx_t    *ctx,
                              rpk_trans_t         transAu,
                              rpk_trans_t         transAv,
                              const void         *alphaptr,
                              rpk_int_t           M1,
                              rpk_int_t           N1,
                              const rpk_matrix_t *A,
                              rpk_int_t           M2,
                              rpk_int_t           N2,
                              rpk_matrix_t       *B,
                              rpk_int_t           offx,
                              rpk_int_t           offy );

/**
 *    @}
 * @}
 *
 * @addtogroup kernel_lr_rrqr
 * @{
 *    These are the rank-revealing QR implementations to generate the low-rank
 *    representations of a full rank matrix.
 *
 *    @name RapackComplex64 main template to convert a full rank matrix to low-rank
 *    @{
 */

/**
 *    @}
 *    @name RapackComplex64 Rank Revealing QR kernels for low-rank
 *    @{
 */
rpk_fixdbl_t rpkx_zge2lr_pqrcp( const rpk_ctx_t *ctx,
                                rpk_int_t        rklimit,
                                rpk_int_t        m,
                                rpk_int_t        n,
                                const void      *Avoid,
                                rpk_int_t        lda,
                                rpk_matrix_t    *Alr );
rpk_fixdbl_t rpkx_zrradd_pqrcp( const rpk_ctx_t    *ctx,
                                rpk_trans_t         transAu,
                                rpk_trans_t         transAv,
                                const void         *alphaptr,
                                rpk_int_t           M1,
                                rpk_int_t           N1,
                                const rpk_matrix_t *A,
                                rpk_int_t           M2,
                                rpk_int_t           N2,
                                rpk_matrix_t       *B,
                                rpk_int_t           offx,
                                rpk_int_t           offy );

rpk_fixdbl_t rpkx_zge2lr_rqrcp( const rpk_ctx_t *ctx,
                                rpk_int_t        rklimit,
                                rpk_int_t        m,
                                rpk_int_t        n,
                                const void      *Avoid,
                                rpk_int_t        lda,
                                rpk_matrix_t    *Alr );
rpk_fixdbl_t rpkx_zrradd_rqrcp( const rpk_ctx_t    *ctx,
                                rpk_trans_t         transAu,
                                rpk_trans_t         transAv,
                                const void         *alphaptr,
                                rpk_int_t           M1,
                                rpk_int_t           N1,
                                const rpk_matrix_t *A,
                                rpk_int_t           M2,
                                rpk_int_t           N2,
                                rpk_matrix_t       *B,
                                rpk_int_t           offx,
                                rpk_int_t           offy );

rpk_fixdbl_t rpkx_zge2lr_tqrcp( const rpk_ctx_t *ctx,
                                rpk_int_t        rklimit,
                                rpk_int_t        m,
                                rpk_int_t        n,
                                const void      *Avoid,
                                rpk_int_t        lda,
                                rpk_matrix_t    *Alr );
rpk_fixdbl_t rpkx_zrradd_tqrcp( const rpk_ctx_t    *ctx,
                                rpk_trans_t         transAu,
                                rpk_trans_t         transAv,
                                const void         *alphaptr,
                                rpk_int_t           M1,
                                rpk_int_t           N1,
                                const rpk_matrix_t *A,
                                rpk_int_t           M2,
                                rpk_int_t           N2,
                                rpk_matrix_t       *B,
                                rpk_int_t           offx,
                                rpk_int_t           offy );

rpk_fixdbl_t rpkx_zge2lr_rqrrt( const rpk_ctx_t *ctx,
                                rpk_int_t        rklimit,
                                rpk_int_t        m,
                                rpk_int_t        n,
                                const void      *Avoid,
                                rpk_int_t        lda,
                                rpk_matrix_t    *Alr );
rpk_fixdbl_t rpkx_zge2lr_qrcp( const rpk_ctx_t *ctx,
                               rpk_zrrqr_cp_t   rrqrfct,
                               rpk_int_t        rklimit,
                               rpk_int_t        m,
                               rpk_int_t        n,
                               const void      *Avoid,
                               rpk_int_t        lda,
                               rpk_matrix_t    *Alr );
rpk_fixdbl_t rpkx_zge2lr_qrrt( const rpk_ctx_t *ctx,
                               rpk_zrrqr_rt_t   rrqrfct,
                               rpk_int_t        rklimit,
                               rpk_int_t        m,
                               rpk_int_t        n,
                               const void      *Avoid,
                               rpk_int_t        lda,
                               rpk_matrix_t    *Alr );

rpk_fixdbl_t rpkx_zrradd_qr( const rpk_ctx_t    *ctx,
                             rpk_zrrqr_cp_t      rrqrfct,
                             rpk_trans_t         transAu,
                             rpk_trans_t         transAv,
                             const void         *alphaptr,
                             rpk_int_t           M1,
                             rpk_int_t           N1,
                             const rpk_matrix_t *A,
                             rpk_int_t           M2,
                             rpk_int_t           N2,
                             rpk_matrix_t       *B,
                             rpk_int_t           offx,
                             rpk_int_t           offy );

/**
 *    @}
 * @}
 */

rpk_fixdbl_t rpkx_zlrscl( const rpk_ctx_t *ctx,
                          rpk_complex64_t  alpha,
                          rpk_int_t        m,
                          rpk_int_t        n,
                          rpk_int_t        lm,
                          rpk_int_t        ln,
                          rpk_matrix_t    *A,
                          rpk_int_t        i,
                          rpk_int_t        j );

rpk_fixdbl_t rpkx_ztrsm( const rpk_ctx_t       *ctx,
                         rpk_side_t             side,
                         rpk_uplo_t             uplo,
                         rpk_trans_t            trans,
                         rpk_diag_t             diag,
                         rpk_int_t              m,
                         rpk_int_t              n,
                         rpk_complex64_t        alpha,
                         const rpk_complex64_t *A,
                         rpk_int_t              lda,
                         rpk_matrix_t          *B );

rpk_fixdbl_t rpkx_zherk( const rpk_ctx_t    *ctx,
                         rpk_uplo_t          uplo,
                         rpk_trans_t         trans,
                         rpk_int_t           n,
                         rpk_int_t           k,
                         double              alpha,
                         const rpk_matrix_t *A,
                         double              beta,
                         rpk_complex64_t    *C,
                         int                 ldc,
                         rpk_complex64_t    *W );

#endif /* _rpkx_zapi_h_ */
