/**
 *
 * @file testrpk_z.h
 *
 * Tests functions header.
 *
 * @copyright 2018-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Mathieu Faverge
 * @date 2024-03-11
 *
 * @precisions normal z -> z c d s
 *
 **/
#ifndef _testrpk_z_h_
#define _testrpk_z_h_

#include "tests.h"

extern rpk_ctx_t rpk_zctx;

int  testrpk_zgenmat( int mode, double tolerance, double threshold, test_matrix_t *A );
void testrpk_zgenmat_comp( const rpk_ctx_t *lowrank, int mode, double threshold, test_matrix_t *A );

int testrpk_zcheck_ge2lr( rpk_ctx_t *lowrank,
			  rpk_compmeth_t compmeth,
			  test_matrix_t *C );

int testrpk_zcheck_rradd( rpk_ctx_t *lowrank,
			  rpk_compmeth_t compmeth,
			  rpk_int_t offx, rpk_int_t offy,
			  rpk_complex64_t zalpha,
			  const test_matrix_t *A,
			  const test_matrix_t *B,
			  test_matrix_t *C );

int testrpk_zcheck_gemm( rpk_ctx_t *lowrank,
			 rpk_compmeth_t compmeth,
			 rpk_int_t offx, rpk_int_t offy,
			 rpk_complex64_t   alpha,
			 const test_matrix_t *A,
			 const test_matrix_t *B,
			 rpk_complex64_t   beta,
			 const test_matrix_t *C );

#endif /* _testrpk_z_h_ */
