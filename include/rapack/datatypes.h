/**
 *
 * @file rapack/datatypes.h
 *
 * @copyright 2013-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * Definitions of the datatypes used in RAPACK
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Tony Delarue
 * @date 2023-12-14
 *
 */
#ifndef _rapack_datatypes_h_
#define _rapack_datatypes_h_

#include <inttypes.h>
#include "rapack/config.h"

BEGIN_C_DECLS

/**
 * @addtogroup rapack
 * @{
 *   @def RAPACK_INT_MAX
 *   @brief The maximum rpk_int_t value
 *
 *   @typedef rpk_int_t
 *   @brief The main integer datatype used in rapack arrays
 *
 *   @typedef rpk_uint_t
 *   @brief The main unsigned integer datatype used in rapack arrays
 *
 *   @typedef rpk_complex64_t
 *   @brief The double complex arithmetic datatype
 *
 *   @typedef rpk_complex32_t
 *   @brief The real complex arithmetic datatype
 */
#if defined(RAPACK_INT64)

typedef int64_t  rpk_int_t;
typedef uint64_t rpk_uint_t;
#define RAPACK_INT_MAX INT64_MAX

#elif defined(RAPACK_INT32)

typedef int32_t  rpk_int_t;
typedef uint32_t rpk_uint_t;
#define RAPACK_INT_MAX INT32_MAX

#elif defined(RAPACK_LONG)

typedef long          rpk_int_t;
typedef unsigned long rpk_uint_t;
#define RAPACK_INT_MAX LONG_MAX

#else

typedef int          rpk_int_t;
typedef unsigned int rpk_uint_t;
#define RAPACK_INT_MAX INT_MAX

#endif
/**
 *@}
 */

/**
 *******************************************************************************
 *
 * @ingroup rapack_dev
 * @brief Internal function to compute min(a,b)
 *
 *******************************************************************************
 *
 * @param[in] a
 * @param[in] b
 *
 *******************************************************************************
 *
 * @return min( a, b )
 *
 ********************************************************************************/
static inline rpk_int_t
rpk_imin( rpk_int_t a, rpk_int_t b )
{
    return ( a < b ) ? a : b;
}

/**
 *******************************************************************************
 *
 * @ingroup rapack_dev
 * @brief Internal function to compute max(a,b)
 *
 *******************************************************************************
 *
 * @param[in] a
 * @param[in] b
 *
 *******************************************************************************
 *
 * @return max( a, b )
 *
 ********************************************************************************/
static inline rpk_int_t
rpk_imax( rpk_int_t a, rpk_int_t b )
{
    return ( a > b ) ? a : b;
}

/**
 *******************************************************************************
 *
 * @ingroup rpk_dev
 * @brief Internal function to compute ceil(a,b)
 *
 *******************************************************************************
 *
 * @param[in] a
 * @param[in] b
 *
 *******************************************************************************
 *
 * @return ceil( a, b )
 *
 ********************************************************************************/
static inline rpk_int_t
rpk_iceil( rpk_int_t a, rpk_int_t b )
{
    return ( a + b - 1 ) / b;
}

/**
 * @ingroup rpk_dev
 * @brief Double datatype that is not converted through precision generator functions
 */
typedef double rpk_fixdbl_t;

/**
 * Complex numbers (Extracted from PaRSEC project)
 **/
#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
/* Windows and non-Intel compiler */
#include <complex>
typedef std::complex<float>  rpk_complex32_t;
typedef std::complex<double> rpk_complex64_t;
#else
typedef float  _Complex      rpk_complex32_t;
typedef double _Complex      rpk_complex64_t;
#endif

#if !defined(__cplusplus) && defined(HAVE_COMPLEX_H)
#include <complex.h>
#else

/**
 * These declarations will not clash with what C++ provides because
 * the names in C++ are name-mangled.
 */
#ifndef DOXYGEN_SHOULD_SKIP_THIS
extern double cabs ( rpk_complex64_t z );
extern double creal( rpk_complex64_t z );
extern double cimag( rpk_complex64_t z );

extern float cabsf ( rpk_complex32_t z );
extern float crealf( rpk_complex32_t z );
extern float cimagf( rpk_complex32_t z );

extern rpk_complex64_t conj ( rpk_complex64_t z );
extern rpk_complex64_t csqrt( rpk_complex64_t z );

extern rpk_complex32_t conjf ( rpk_complex32_t z );
extern rpk_complex32_t csqrtf( rpk_complex32_t z );
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

#endif /* HAVE_COMPLEX_H */

struct rpk_matrix_s;

/**
 * @brief Type alias to the rpk_matrix_s structure.
 */
typedef struct rpk_matrix_s rpk_matrix_t;

struct rpk_ctx_s;

/**
 * @brief Type alias to the rpk_matrix_s structure.
 */
typedef struct rpk_ctx_s rpk_ctx_t;

END_C_DECLS

#endif /* _rapack_datatypes_h_ */
