/**
 *
 * @file rpk_ztradd.c
 *
 * RAPACK triangular add routine
 *
 * @copyright 2024-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @comment This file has initially been extracted from CHAMELEON 1.2.1
 * @author Mathieu Faverge
 * @author Florent Pruvost
 * @author Abel Calluaud
 * @date 2024-02-16
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 ******************************************************************************
 *
 * @brief Add two triangular matrices together.
 *
 * rpk_ztradd adds two matrices together as in PBLAS pztradd.
 *
 *       B <- alpha * op(A)  + beta * B,
 *
 * where op(X) = X, X', or conj(X')
 *
 *******************************************************************************
 *
 * @param[in] uplo
 *          Specifies the shape of A and B matrices:
 *          = RapackUpperLower: A and B are general matrices.
 *          = RapackUpper: op(A) and B are upper trapezoidal matrices.
 *          = RapackLower: op(A) and B are lower trapezoidal matrices.
 *
 * @param[in] trans
 *         @arg RapackNoTrans:   No transpose, op( A ) = A;
 *         @arg RapackTrans:     Transpose, op( A ) = A';
 *         @arg RapackConjTrans: Conjugate Transpose, op( A ) = conj(A').
 *
 * @param[in] M
 *          Number of rows of the matrix B.
 *          Number of rows of the matrix A, if trans == RapackNoTrans, number of
 *          columns of A otherwise.
 *
 * @param[in] N
 *          Number of columns of the matrix B.
 *          Number of columns of the matrix A, if trans == RapackNoTrans, number
 *          of rows of A otherwise.
 *
 * @param[in] alpha
 *          Scalar factor of A.
 *
 * @param[in] A
 *          Matrix of size LDA-by-N, if trans == RapackNoTrans, LDA-by-M,
 *          otherwise.
 *
 * @param[in] LDA
 *          Leading dimension of the array A. LDA >= max(1,K).
 *          K = M if trans == RapackNoTrans, K = N otherwise.
 *
 * @param[in] beta
 *          Scalar factor of B.
 *
 * @param[inout] B
 *          Matrix of size LDB-by-N.
 *
 * @param[in] LDB
 *          Leading dimension of the array B. LDB >= max(1,M)
 *
 *******************************************************************************
 *
 * @retval RAPACK_SUCCESS successful exit
 * @retval <0 if -i, the i-th argument had an illegal value
 * @retval 1, not yet implemented
 *
 ******************************************************************************/
int
rpk_ztradd( rpk_uplo_t             uplo,
            rpk_trans_t            trans,
            rpk_int_t              M,
            rpk_int_t              N,
            rpk_complex64_t        alpha,
            const rpk_complex64_t *A,
            rpk_int_t              LDA,
            rpk_complex64_t        beta,
            rpk_complex64_t       *B,
            rpk_int_t              LDB )
{
    int i, j, minMN;

    if ( uplo == RapackUpperLower ) {
        int rc = rpk_zgeadd( trans, M, N, alpha, A, LDA, beta, B, LDB );
        if ( rc != RAPACK_SUCCESS )
            return rc - 1;
        else
            return rc;
    }

    if ( ( uplo != RapackUpper ) && ( uplo != RapackLower ) ) {
        fprintf( stderr, "illegal value of uplo" );
        return -1;
    }
    if ( !rpk_is_valid_trans( trans ) ) {
        fprintf( stderr, "illegal value of trans" );
        return -2;
    }
    if ( M < 0 ) {
        fprintf( stderr, "Illegal value of M" );
        return -3;
    }
    if ( N < 0 ) {
        fprintf( stderr, "Illegal value of N" );
        return -4;
    }
    if ( ( ( trans == RapackNoTrans ) && ( LDA < rpk_imax( 1, M ) ) && ( M > 0 ) ) ||
         ( ( trans != RapackNoTrans ) && ( LDA < rpk_imax( 1, N ) ) && ( N > 0 ) ) ) {
        fprintf( stderr, "Illegal value of LDA" );
        return -7;
    }
    if ( ( LDB < rpk_imax( 1, M ) ) && ( M > 0 ) ) {
        fprintf( stderr, "Illegal value of LDB" );
        return -9;
    }

    minMN = rpk_imax( M, N );

    if ( beta == (rpk_complex64_t)0. ) {
        LAPACKE_zlaset_work( LAPACK_COL_MAJOR, rpk_lapack_const(uplo),
                             M, N, 0., 0., B, LDB );
    }
    else if ( beta != (rpk_complex64_t)1. ) {
        LAPACKE_zlascl_work( LAPACK_COL_MAJOR, rpk_lapack_const(uplo),
                             0, 0, 1., beta, M, N, B, LDB );
    }

    /**
     * RapackLower
     */
    if ( uplo == RapackLower ) {
        if ( trans == RapackNoTrans ) {
            for ( j = 0; j < minMN; j++ ) {
                for ( i = j; i < M; i++, B++, A++ ) {
                    *B += alpha * ( *A );
                }
                B += LDB - M + j + 1;
                A += LDA - M + j + 1;
            }
        }
#if defined(PRECISION_z) || defined(PRECISION_c)
        else if ( trans == RapackConjTrans ) {
            for ( j = 0; j < minMN; j++, A++ ) {
                for ( i = j; i < M; i++, B++ ) {
                    *B += alpha * conj( A[LDA * i] );
                }
                B += LDB - M + j + 1;
            }
        }
#endif /* defined(PRECISION_z) || defined(PRECISION_c) */
        else {
            for ( j = 0; j < minMN; j++, A++ ) {
                for ( i = j; i < M; i++, B++ ) {
                    *B += alpha * A[LDA * i];
                }
                B += LDB - M + j + 1;
            }
        }
    }
    /**
     * RapackUpper
     */
    else {
        if ( trans == RapackNoTrans ) {
            for ( j = 0; j < N; j++ ) {
                int mm = rpk_imax( j + 1, M );
                for ( i = 0; i < mm; i++, B++, A++ ) {
                    *B += alpha * ( *A );
                }
                B += LDB - mm;
                A += LDA - mm;
            }
        }
#if defined(PRECISION_z) || defined(PRECISION_c)
        else if ( trans == RapackConjTrans ) {
            for ( j = 0; j < N; j++, A++ ) {
                int mm = rpk_imax( j + 1, M );
                for ( i = 0; i < mm; i++, B++ ) {
                    *B += alpha * conj( A[LDA * i] );
                }
                B += LDB - mm;
            }
        }
#endif /* defined(PRECISION_z) || defined(PRECISION_c) */
        else {
            for ( j = 0; j < N; j++, A++ ) {
                int mm = rpk_imax( j + 1, M );
                for ( i = 0; i < mm; i++, B++ ) {
                    *B += alpha * ( A[LDA * i] );
                }
                B += LDB - mm;
            }
        }
    }
    return RAPACK_SUCCESS;
}
