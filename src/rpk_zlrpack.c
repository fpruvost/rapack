/**
 *
 * @file rpk_zlrpack.c
 *
 * RAPACK low-rank kernel routines
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Nolan Bredel
 * @author Abel Calluaud
 * @date 2024-02-08
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"
#include "rpk_znancheck.h"

/**
 *******************************************************************************
 *
 * @brief Pack low-rank data by side
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix A.
 *
 * @param[in] N
 *          The number of columns of the matrix A.
 *
 * @param[in] A
 *          The low-rank representation of the matrix A.
 *
 * @param[inout] buffer
 *          Pointer on packed data
 *
 *******************************************************************************
 *
 * @return Pointer on packed data shifted to the next block
 *
 *******************************************************************************/
char *
rpk_zlrpack( rpk_int_t M, rpk_int_t N, const rpk_matrix_t *A, char *buffer )
{
    int   rk    = A->rk;
    int   rkmax = A->rkmax;
    void *u     = A->u;
    void *v     = A->v;
    int   ret;

    /* Store the rank */
    memcpy( buffer, &rk, sizeof( int ) );
    buffer += sizeof( int );

    if ( rk != -1 ) {
        /* Pack the u part */
        memcpy( buffer, u, rk * M * sizeof( rpk_complex64_t ) );
        buffer += rk * M * sizeof( rpk_complex64_t );

        /* Pack the v part */
        if ( rk == rkmax ) {
            memcpy( buffer, v, rk * N * sizeof( rpk_complex64_t ) );
            buffer += rk * N * sizeof( rpk_complex64_t );
        }
        else {
            ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', rk, N, v, rkmax,
                                      (rpk_complex64_t *)buffer, rk );
            assert( ret == 0 );
            buffer += rk * N * sizeof( rpk_complex64_t );
        }
    }
    else {
        memcpy( buffer, u, M * N * sizeof( rpk_complex64_t ) );
        buffer += M * N * sizeof( rpk_complex64_t );
    }

    (void)ret;

    return buffer;
}

/**
 *******************************************************************************
 *
 * @brief Unpack low rank data and fill the cblk concerned by the computation
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix A.
 *
 * @param[in] N
 *          The number of columns of the matrix A.
 *
 * @param[in] A
 *          The low-rank representation of the matrix A.
 *
 * @param[inout] buffer
 *          Pointer on packed data
 *
 *******************************************************************************
 *
 * @return Pointer on packed data shifted to the next block
 *
 *******************************************************************************/
char *
rpkx_zlrunpack( const rpk_ctx_t *ctx, rpk_int_t M, rpk_int_t N, rpk_matrix_t *A, char *buffer )
{
    int rk;
    memcpy( &rk, buffer, sizeof( int ) );
    buffer += sizeof( int );

    /* Make sure A can store the unpacked values */
    rpkx_zlrsze( ctx, 0, M, N, A, rk, rk, rk );

    if ( rk != -1 ) {
        /* Unpack U */
        memcpy( A->u, buffer, M * rk * sizeof( rpk_complex64_t ) );
        buffer += M * rk * sizeof( rpk_complex64_t );

        /* Unpack V */
        memcpy( A->v, buffer, N * rk * sizeof( rpk_complex64_t ) );
        buffer += N * rk * sizeof( rpk_complex64_t );
    }
    else {
        /* Unpack the full block */
        memcpy( A->u, buffer, M * N * sizeof( rpk_complex64_t ) );
        buffer += M * N * sizeof( rpk_complex64_t );
    }

    return buffer;
}

char *rpk_zlrunpack( rpk_int_t M, rpk_int_t N, rpk_matrix_t *A, char *buffer )
{
    return rpkx_zlrunpack( rpk_gctx, M, N, A, buffer );
}

/**
 *******************************************************************************
 *
 * @brief Unpack low rank data and fill the cblk concerned by the computation
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix A.
 *
 * @param[in] N
 *          The number of columns of the matrix A.
 *
 * @param[in] A
 *          The low-rank representation of the matrix A.
 *
 * @param[inout] input
 *          TODO
 *
 * @param[inout] outptr
 *          TODO
 *
 *******************************************************************************
 *
 * @return Pointer on packed data shifted to the next block
 *
 *******************************************************************************/
const char *
rpk_zlrunpack2( rpk_int_t M, rpk_int_t N, rpk_matrix_t *A, const char *input, char **outptr )
{
    char  *output = *outptr;
    size_t size;
    int    rk;

    rk = *((int *)input);
    input += sizeof( int );

    if ( rk != -1 ) {
        A->rk    = rk;
        A->rkmax = rk;

        /* Unpack U */
        size = M * rk * sizeof( rpk_complex64_t );
        A->u = output;

        memcpy( A->u, input, size );
        input  += size;
        output += size;

        /* Unpack V */
        size = N * rk * sizeof( rpk_complex64_t );
        A->v = output;

        memcpy( A->v, input, size );
        input  += size;
        output += size;
    }
    else {
        A->rk    = -1;
        A->rkmax = M;
        A->v     = NULL;

        /* Unpack the full block */
        size = M * N * sizeof( rpk_complex64_t );
        A->u = output;

        memcpy( A->u, input, size );
        input  += size;
        output += size;
    }

    *outptr = output;
    return input;
}
