/**
 *
 * @file rpk_zxx2lr.c
 *
 * RAPACK low-rank kernel routines that form the product of two matrices A and B
 * into a low-rank form for an update on a null or low-rank matrix.
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Gregoire Pichon
 * @author Pierre Ramet
 * @author Abel Calluaud
 * @date 2024-03-11
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

#ifndef DOXYGEN_SHOULD_SKIP_THIS
static rpk_complex64_t zone  = 1.0;
static rpk_complex64_t zzero = 0.0;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

/**
 *******************************************************************************
 *
 * @brief Perform the operation AB = op(A) * op(B), with A and B full-rank and AB
 * low-rank.
 *
 *******************************************************************************
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix contains the product AB aligned with its own
 *          dimensions.
 *          @sa rpk_zgemm_t
 *
 * @param[inout] AB
 *          The low-rank structure of the AB matrix in which to store the AB product.
 *
 * @param[inout] infomask
 *          The mask of informations returned by the rpk_zxx2lr() functions.
 *          - If AB.u is orthogonal on exit, then RAPACK_LRM3_ORTHOU is set.
 *          - If AB.u is allocated, then RAPACK_LRM3_ALLOCU is set.
 *          - If AB.v is allocated, then RAPACK_LRM3_ALLOCV is set.
 *          - If AB.v is inistialized as one of the given pointer and op(B) is not
 *          applyed, then RAPACK_LRM3_TRANSB is set.
 *
 * @param[in] Kmax
 *          The maximum K value for which the AB product is contructed as AB.u =
 *          A, and AB.v = B
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zfrfr2lr( const rpk_ctx_t *ctx,
               rpk_zgemm_t     *params,
               rpk_matrix_t    *AB,
               int             *infomask,
               rpk_int_t        Kmax )
{
    (void)ctx;
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_int_t    ldau, ldbu;
    rpk_fixdbl_t flops = 0.0;

    ldau = ( transA == RapackNoTrans ) ? M : K;
    ldbu = ( transB == RapackNoTrans ) ? K : N;

    /*
     * Everything is full rank
     */
    if ( K < Kmax ) {
        /*
         * Let's build a low-rank matrix of rank K
         */
        AB->rk    = K;
        AB->rkmax = K;
        AB->u     = A->u;
        AB->v     = B->u;
        *infomask |= ( RAPACK_LRM3_TRANSA | RAPACK_LRM3_TRANSB );
    }
    else {
        /*
         * Let's compute the product to form a full-rank matrix of rank
         * rpk_imin( M, N )
         */
        if ( ( work = rpk_zgemm_getws( params, M * N ) ) == NULL ) {
            work = malloc( M * N * sizeof( rpk_complex64_t ) );
            *infomask |= RAPACK_LRM3_ALLOCU;
        }
        AB->rk    = -1;
        AB->rkmax = M;
        AB->u     = work;
        AB->v     = NULL;

        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                     M, N, K,
                     CBLAS_SADDR(zone),  A->u,  ldau,
                                         B->u,  ldbu,
                     CBLAS_SADDR(zzero), AB->u, M );
        flops = flops_zgemm( M, N, K );
    }

    PASTE_RPK_ZLRMM_VOID;
    return flops;
}

/**
 *******************************************************************************
 *
 * @brief Perform the operation AB = op(A) * op(B), with A full-rank and B and AB
 * low-rank.
 *
 *******************************************************************************
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix contains the product AB aligned with its own
 *          dimensions.
 *          @sa rpk_zgemm_t
 *
 * @param[inout] AB
 *          The low-rank structure of the AB matrix in which to store the AB product.
 *
 * @param[inout] infomask
 *          The mask of informations returned by the rpk_zxx2lr() functions.
 *          - If AB.u is orthogonal on exit, then RAPACK_LRM3_ORTHOU is set.
 *          - If AB.u is allocated, then RAPACK_LRM3_ALLOCU is set.
 *          - If AB.v is allocated, then RAPACK_LRM3_ALLOCV is set.
 *          - If AB.v is inistialized as one of the given pointer and op(B) is not
 *          applyed, then RAPACK_LRM3_TRANSB is set.
 *
 * @param[in] Brkmin
 *          Threshold for which B->rk is considered as the final rank of AB
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zfrlr2lr( const rpk_ctx_t *ctx,
               rpk_zgemm_t     *params,
               rpk_matrix_t    *AB,
               int             *infomask,
               rpk_int_t        Brkmin )
{
    (void)ctx;
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_fixdbl_t     flops;
    rpk_complex64_t *Bu;
    rpk_int_t        ldBu;
    rpk_complex64_t *Bv;
    rpk_int_t        ldBv;

    /*
     * We want to compute AB = A * op(B) = A * op(B->u * B->v)
     * Let's define the matrices Bu and Bv such as op( B ) = op( Bu ) * op( Bv )
     */
    if ( transB == RapackNoTrans ) {
        Bu   = B->u;
        ldBu = K;
        Bv   = B->v;
        ldBv = B->rkmax;
    }
    else {
        Bu   = B->v;
        ldBu = B->rkmax;
        Bv   = B->u;
        ldBv = N;
    }

    rpk_int_t ldau = ( transA == RapackNoTrans ) ? M : K;

    /*
     *  A(M-by-K) * B( N-by-rb x rb-by-K )^t
     */
    if ( B->rk > Brkmin ) {
        /*
         * We are in a similar case to the _Cfr function, and we
         * choose the optimal number of flops.
         */
        rpk_fixdbl_t     flops1 = flops_zgemm( M, B->rk, K ) + flops_zgemm( M, N, B->rk );
        rpk_fixdbl_t     flops2 = flops_zgemm( K, N, B->rk ) + flops_zgemm( M, N, K );
        rpk_complex64_t *tmp;

        AB->rk    = -1;
        AB->rkmax = M;
        AB->v     = NULL;

        if ( flops1 <= flops2 ) {
            if ( ( work = rpk_zgemm_getws( params, M * B->rk + M * N ) ) == NULL ) {
                work = malloc( ( M * B->rk + M * N ) * sizeof( rpk_complex64_t ) );
                *infomask |= RAPACK_LRM3_ALLOCU;
            }

            /* AB->u will be destroyed later */
            AB->u = work;
            tmp   = work + M * N;

            /*
             *  (A * Bv) * Bu^t
             */
            cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                         M, B->rk, K,
                         CBLAS_SADDR(zone),  A->u, ldau,
                                             Bu, ldBu,
                         CBLAS_SADDR(zzero), tmp,  M );

            cblas_zgemm( CblasColMajor, CblasNoTrans, (CBLAS_TRANSPOSE)transB,
                         M, N, B->rk,
                         CBLAS_SADDR(zone),  tmp,   M,
                                             Bv,  ldBv,
                         CBLAS_SADDR(zzero), AB->u, M );

            flops = flops1;
        }
        else {
            if ( ( work = rpk_zgemm_getws( params, K * N + M * N ) ) == NULL ) {
                work = malloc( ( K * N + M * N ) * sizeof( rpk_complex64_t ) );
                *infomask |= RAPACK_LRM3_ALLOCU;
            }

            /* AB->u will be destroyed later */
            AB->u = work;
            tmp   = work + M * N;

            /*
             *  A * (Bu * Bv^t)^t
             */
            cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transB, (CBLAS_TRANSPOSE)transB,
                         K, N, B->rk,
                         CBLAS_SADDR(zone),  Bu, ldBu,
                                             Bv, ldBv,
                         CBLAS_SADDR(zzero), tmp,  K );

            cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                         M, N, K,
                         CBLAS_SADDR(zone),  A->u,  ldau,
                                             tmp,   K,
                         CBLAS_SADDR(zzero), AB->u, M );

            flops = flops2;
        }
    }
    else {
        /*
         * B->rk is the smallest rank
         */
        AB->rk    = B->rk;
        AB->rkmax = B->rkmax;
        AB->v     = Bv;
        *infomask |= RAPACK_LRM3_TRANSB;

        if ( ( work = rpk_zgemm_getws( params, M * B->rk ) ) == NULL ) {
            work = malloc( M * B->rk * sizeof( rpk_complex64_t ) );
            *infomask |= RAPACK_LRM3_ALLOCU;
        }
        AB->u = work;

        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                     M, B->rk, K,
                     CBLAS_SADDR(zone),  A->u,  ldau,
                                         Bu,  ldBu,
                     CBLAS_SADDR(zzero), AB->u, M );
        flops = flops_zgemm( M, B->rk, K );
    }

    PASTE_RPK_ZLRMM_VOID;
    return flops;
}

/**
 *******************************************************************************
 *
 * @brief Perform the operation AB = op(A) * op(B), with B full-rank and A and AB
 * low-rank.
 *
 *******************************************************************************
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix contains the product AB aligned with its own
 *          dimensions.
 *          @sa rpk_zgemm_t
 *
 * @param[inout] AB
 *          The low-rank structure of the AB matrix in which to store the AB product.
 *
 * @param[inout] infomask
 *          The mask of informations returned by the rpk_zxx2lr() functions.
 *          - If AB.u is orthogonal on exit, then RAPACK_LRM3_ORTHOU is set.
 *          - If AB.u is allocated, then RAPACK_LRM3_ALLOCU is set.
 *          - If AB.v is allocated, then RAPACK_LRM3_ALLOCV is set.
 *          - If AB.v is inistialized as one of the given pointer and op(B) is not
 *          applyed, then RAPACK_LRM3_TRANSB is set.
 *
 * @param[in] Arkmin
 *          Threshold for which A->rk is considered as the final rank of AB
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zlrfr2lr( const rpk_ctx_t *ctx,
               rpk_zgemm_t     *params,
               rpk_matrix_t    *AB,
               int             *infomask,
               rpk_int_t        Arkmin )
{
    (void)ctx;
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_int_t        ldbu;
    rpk_fixdbl_t     flops;
    rpk_complex64_t *Au;
    rpk_int_t        ldAu;
    rpk_complex64_t *Av;
    rpk_int_t        ldAv;

    ldbu = ( transB == RapackNoTrans ) ? K : N;

    if ( transA == RapackNoTrans ) {
        Au   = A->u;
        ldAu = M;
        Av   = A->v;
        ldAv = A->rkmax;
    }
    else {
        Au   = A->v;
        ldAu = A->rkmax;
        Av   = A->u;
        ldAv = K;
    }

    /*
     *  A( M-by-ra x ra-by-K ) * B(N-by-K)^t
     */
    if ( A->rk > Arkmin ) {
        /*
         * We are in a similar case to the _Cfr function, and we
         * choose the optimal number of flops.
         */
        rpk_fixdbl_t     flops1 = flops_zgemm( A->rk, N, K ) + flops_zgemm( M, N, A->rk );
        rpk_fixdbl_t     flops2 = flops_zgemm( M, K, A->rk ) + flops_zgemm( M, N, K );
        rpk_complex64_t *tmp;

        AB->rk    = -1;
        AB->rkmax = M;
        AB->v     = NULL;

        if ( flops1 <= flops2 ) {
            if ( ( work = rpk_zgemm_getws( params, A->rk * N + M * N ) ) == NULL ) {
                work = malloc( ( A->rk * N + M * N ) * sizeof( rpk_complex64_t ) );
                *infomask |= RAPACK_LRM3_ALLOCU;
            }

            /* AB->u will be destroyed later */
            AB->u = work;
            tmp   = work + M * N;

            /*
             *  Au * (Av^t * B^t)
             */
            cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                         A->rk, N, K,
                         CBLAS_SADDR(zone),  Av, ldAv,
                                             B->u, ldbu,
                         CBLAS_SADDR(zzero), tmp,  A->rk );

            cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, CblasNoTrans,
                         M, N, A->rk,
                         CBLAS_SADDR(zone),  Au,  ldAu,
                                             tmp,   A->rk,
                         CBLAS_SADDR(zzero), AB->u, M );

            flops = flops1;
        }
        else {
            if ( ( work = rpk_zgemm_getws( params, M * K + M * N ) ) == NULL ) {
                work = malloc( ( M * K + M * N ) * sizeof( rpk_complex64_t ) );
                *infomask |= RAPACK_LRM3_ALLOCU;
            }

            /* AB->u will be destroyed later */
            AB->u = work;
            tmp   = work + M * N;

            /*
             *  (Au * Av^t) * B^t
             */
            cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transA,
                         M, K, A->rk,
                         CBLAS_SADDR(zone),  Au, ldAu,
                                             Av, ldAv,
                         CBLAS_SADDR(zzero), tmp,  M );

            cblas_zgemm( CblasColMajor, CblasNoTrans, (CBLAS_TRANSPOSE)transB,
                         M, N, K,
                         CBLAS_SADDR(zone),  tmp,   M,
                                             B->u,  ldbu,
                         CBLAS_SADDR(zzero), AB->u, M );

            flops = flops2;
        }
    }
    else {
        /*
         * A->rk is the smallest rank
         */
        AB->rk    = A->rk;
        AB->rkmax = A->rk;
        AB->u     = Au;
        *infomask |= RAPACK_LRM3_ORTHOU;
        *infomask |= RAPACK_LRM3_TRANSA;

        if ( ( work = rpk_zgemm_getws( params, A->rk * N ) ) == NULL ) {
            work = malloc( A->rk * N * sizeof( rpk_complex64_t ) );
            *infomask |= RAPACK_LRM3_ALLOCV;
        }
        AB->v = work;

        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                     A->rk, N, K,
                     CBLAS_SADDR(zone),  Av,  ldAv,
                                         B->u,  ldbu,
                     CBLAS_SADDR(zzero), AB->v, AB->rkmax );

        flops = flops_zgemm( A->rk, N, K );
    }

    PASTE_RPK_ZLRMM_VOID;
    (void)infomask;
    return flops;
}

/**
 *******************************************************************************
 *
 * @brief Perform the operation AB = op(A) * op(B), with A, B, and AB low-rank.
 *
 *******************************************************************************
 *
 * @param[inout] params
 *          The LRMM structure that stores all the parameters used in the LRMM
 *          functions family.
 *          On exit, the C matrix contains the product AB aligned with its own
 *          dimensions.
 *          @sa rpk_zgemm_t
 *
 * @param[inout] AB
 *          The low-rank structure of the AB matrix in which to store the AB product.
 *
 * @param[inout] infomask
 *          The mask of informations returned by the rpk_zxx2lr() functions.
 *          - If AB.u is orthogonal on exit, then RAPACK_LRM3_ORTHOU is set.
 *          - If AB.u is allocated, then RAPACK_LRM3_ALLOCU is set.
 *          - If AB.v is allocated, then RAPACK_LRM3_ALLOCV is set.
 *          - If AB.v is inistialized as one of the given pointer and op(B) is not
 *          applyed, then RAPACK_LRM3_TRANSB is set.
 *
 *******************************************************************************
 *
 * @return The number of flops required to perform the operation.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zlrlr2lr( const rpk_ctx_t *ctx, rpk_zgemm_t *params, rpk_matrix_t *AB, int *infomask )
{
    PASTE_RPK_ZLRMM_PARAMS( params );
    rpk_complex64_t *work_Av_Bu;
    rpk_matrix_t     rArB;
    rpk_fixdbl_t     flops     = 0.0;
    int              allocated = 0;
    rpk_complex64_t *Au;
    rpk_int_t        ldAu;
    rpk_complex64_t *Av;
    rpk_int_t        ldAv;
    rpk_complex64_t *Bu;
    rpk_int_t        ldBu;
    rpk_complex64_t *Bv;
    rpk_int_t        ldBv;

    assert( A->rk <= A->rkmax && A->rk > 0 );
    assert( B->rk <= B->rkmax && B->rk > 0 );

    assert( A->rkmax != -1 );
    assert( B->rkmax != -1 );

    /*
     * We need to compute opA(A) * opB(B)
     * so we define Au, Av, Bu, Bv such that:
     *   op( A ) = op( A->u * A->v ) = op( Au ) * op( Av ),
     *   op( B ) = op( B->u * B->v ) = op( Bu ) * op( Bv )
     *
     * if opA == NoTrans then
     *     Au = A->u and is a M by A->rk matrix
     *     Av = A->v and is a A->rk by K matrix
     * else
     *     Au = A->v and is a A->rk by M matrix
     *     Av = A->u and is a K by A->rk matrix
     *
     * if opB == NoTrans then
     *     Bu = B->u and is a K by B->rk matrix
     *     Bv = B->v and is a B->rk by N matrix
     * else
     *     Bu = B->v and is a B->rk by K matrix
     *     Bv = B->u and is a N by B->rk matrix
     */

    if ( transA == RapackNoTrans ) {
        Au   = A->u;
        ldAu = M;
        Av   = A->v;
        ldAv = A->rkmax;
    }
    else {
        Au   = A->v;
        ldAu = A->rkmax;
        Av   = A->u;
        ldAv = K;
    }

    if ( transB == RapackNoTrans ) {
        Bu   = B->u;
        ldBu = K;
        Bv   = B->v;
        ldBv = B->rkmax;
    }
    else {
        Bu   = B->v;
        ldBu = B->rkmax;
        Bv   = B->u;
        ldBv = N;
    }

    *infomask = 0;

    if ( ( work_Av_Bu = rpk_zgemm_getws( params, A->rkmax * B->rkmax ) ) == NULL ) {
        work_Av_Bu = malloc( A->rkmax * B->rkmax * sizeof( rpk_complex64_t ) );
        allocated  = 1;
    }

    /*
     * Let's compute Av * Bu with the smallest ws
     */
    cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                 A->rk, B->rk, K,
                 CBLAS_SADDR(zone), Av,  ldAv, Bu, ldBu,
                 CBLAS_SADDR(zzero), work_Av_Bu, A->rkmax );

    flops = flops_zgemm( A->rk, B->rk, K );

    /*
     * Try to compress (Av * Bu)
     */
    flops += ctx->rpk_ge2lr( ctx, -1, A->rk, B->rk, work_Av_Bu, A->rkmax, &rArB );

    /*
     * The rank of AB is not smaller than min(rankA, rankB)
     */
    if ( rArB.rk == -1 ) {
        if ( A->rk <= B->rk ) {
            /*
             *    ABu = Au
             *    ABv = (Av * Bu) * Bv
             */
            if ( ( work = rpk_zgemm_getws( params, A->rk * N ) ) == NULL ) {
                work = malloc( A->rk * N * sizeof( rpk_complex64_t ) );
                *infomask |= RAPACK_LRM3_ALLOCV;
            }

            AB->rk    = A->rk;
            AB->rkmax = A->rkmax;
            AB->u     = Au;
            AB->v     = work;
            *infomask |= RAPACK_LRM3_ORTHOU;
            *infomask |= RAPACK_LRM3_TRANSA;

            cblas_zgemm( CblasColMajor, CblasNoTrans, (CBLAS_TRANSPOSE)transB,
                         A->rk, N, B->rk,
                         CBLAS_SADDR(zone),  work_Av_Bu, A->rkmax,
                                             Bv,  ldBv,
                         CBLAS_SADDR(zzero), AB->v, AB->rkmax );

            flops += flops_zgemm( A->rk, N, B->rk );
        }
        else {
            /*
             *    ABu = Au * (Av * Bu)
             *    ABv = Bv
             */
            if ( ( work = rpk_zgemm_getws( params, B->rkmax * M ) ) == NULL ) {
                work = malloc( B->rkmax * M * sizeof( rpk_complex64_t ) );
                *infomask |= RAPACK_LRM3_ALLOCU;
            }

            AB->rk    = B->rk;
            AB->rkmax = B->rk;
            AB->u     = work;
            AB->v     = Bv;

            *infomask |= RAPACK_LRM3_TRANSB;

            cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, CblasNoTrans,
                         M, B->rk, A->rk,
                         CBLAS_SADDR(zone),  Au,   ldAu,
                                             work_Av_Bu,  A->rkmax,
                         CBLAS_SADDR(zzero), AB->u,  M );
            flops += flops_zgemm( M, B->rk, A->rk );
        }
    }
    else if ( rArB.rk == 0 ) {
        AB->rk    = 0;
        AB->rkmax = 0;
        AB->u     = NULL;
        AB->v     = NULL;
        *infomask |= RAPACK_LRM3_ORTHOU;
    }
    /**
     * The rank of AB is smaller than min(rankA, rankB)
     */
    else {
        if ( ( work = rpk_zgemm_getws( params, ( M + N ) * rArB.rk ) ) == NULL ) {
            work = malloc( ( M + N ) * rArB.rk * sizeof( rpk_complex64_t ) );
            *infomask |= RAPACK_LRM3_ALLOCU;
        }

        /*
         * Let's compute ABu = Au * rArB.u and ABv = rArB.v * Bv
         */

        AB->rk    = rArB.rk;
        AB->rkmax = rArB.rk;
        AB->u     = work;
        AB->v     = work + M * rArB.rk;
        *infomask |= RAPACK_LRM3_ORTHOU;

        cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, CblasNoTrans,
                     M, rArB.rk, A->rk,
                     CBLAS_SADDR(zone),  Au,   ldAu,
                                         rArB.u, A->rk,
                     CBLAS_SADDR(zzero), AB->u,  M );

        cblas_zgemm( CblasColMajor, CblasNoTrans, (CBLAS_TRANSPOSE)transB,
                     rArB.rk, N, B->rk,
                     CBLAS_SADDR(zone),  rArB.v, rArB.rkmax,
                                         Bv,   ldBv,
                     CBLAS_SADDR(zzero), AB->v,  rArB.rk );

        flops += flops_zgemm( M, rArB.rk, A->rk ) + flops_zgemm( rArB.rk, N, B->rk );
    }
    rpk_zlrfree( &rArB );

    if ( allocated ) {
        free( work_Av_Bu );
    }
    PASTE_RPK_ZLRMM_VOID;

    return flops;
}
