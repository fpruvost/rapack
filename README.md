# RAPACK: Low **R**ank **A**lgebra **Pack**age

RAPACK is a C interface and library that provides linear algebra operations with low-rank matrices support.

Routines are named following the scheme from LAPACK `prec` `type` `op` [ _ `mod` ] where

- `prec` defines the numerical precision with s, d, c, z for simple, double,
simple complex and double complex precision.

- `type` reflect operand matrix types such as `ge` for **Ge**neral rectangular matrices
and `lr` for **L**ow **R**ank matrices.

- `op` is the operation such as `mm` for **M**atrix **M**ultiplication or `mc` for **M**atrix
**C**ompression.

- `mod` provides additional details on operation such as algorithm variant or
additional constraints on operands.

## Documentation

The latest Doxygen documentation is available [here](http://solverstack.gitlabpages.inria.fr/rapack).

## Installation

### Build and install with CMake

RAPACK can be built using [CMake](https://cmake.org/). This
installation requires to have some library dependencies already
installed on the system:

* BLAS (MKL, OpenBlas, ...) and CBLAS (sequential version required)
* LAPACK and LAPACKE (sequential version required, with TMG enabled for testing)

For instance, on debian-like systems, dependencies can be installed with the following command:

      sudo apt-get install cmake gcc gfortran libopenblas-dev liblapacke-dev

The main options to configure the PaStiX configuration build are:

* Classic cmake options:
  * CMAKE_BUILD_TYPE: Debug, RelWithDebInfo, Release, MinSizeRel; we recommend to use the Release, or RelWithDebInfo, for performance.
  * CMAKE_INSTALL_PREFIX: Specify the prefix directory to install the library
  * BUILD_SHARED_LIBS=[OFF]: Enable the shared libraries build. This option needs to be enabled for the Python wrapper.
* Integer type:
  * RAPACK_INT64[=ON]: Enable/disable int64_t for integer arrays (if enabled, you must switch to an 64 bits integer Blas/Lapack interface).
* Documentation:
  * BUILD_DOCUMENTATION[=OFF] to enable the Doxygen documentation generation.

## Get involved

### Reporting an issue

We strongly recommend all users to use the issue tracker to report any problems with the software, or for any feature request. We will try our best to answer them in a short time frame.

### Contributions

<https://gitlab.inria.fr/solverstack/rapack/blob/main/CONTRIBUTING.md>

## Authors

The following people contribute or contributed to the development of PaStiX:

* Mathieu Faverge, PI
* Pierre Ramet, PI
* Grégoire Pichon
* Florent Pruvost, CMake and Spack
* Claire Soyez-Martin
* Esragul Korkmaz
* Abel Calluaud

If we forgot your name, please let us know that we can fix that mistake.

## Licence

<https://gitlab.inria.fr/solverstack/rapack/blob/main/LICENSE>
