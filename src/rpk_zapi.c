/**
 *
 * @file rpk_zapi.c
 *
 * @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Abel Calluaud
 * @date 2024-03-11
 * @precisions normal z -> c d s
 *
 **/

#include "rapack.h"

int
rpk_zlrsze( int           copy,
            rpk_int_t     M,
            rpk_int_t     N,
            rpk_matrix_t *A,
            rpk_int_t     newrk,
            rpk_int_t     newrkmax,
            rpk_int_t     rklimit )
{
    return rpkx_zlrsze( rpk_gctx, copy, M, N, A, newrk, newrkmax, rklimit );
}

void
rpk_zlrcpy( rpk_trans_t         transA,
            rpk_complex64_t     alpha,
            rpk_int_t           M1,
            rpk_int_t           N1,
            const rpk_matrix_t *A,
            rpk_int_t           M2,
            rpk_int_t           N2,
            rpk_matrix_t       *B,
            rpk_int_t           offx,
            rpk_int_t           offy )
{
    return rpkx_zlrcpy( rpk_gctx, transA, alpha, M1, N1, A, M2, N2, B, offx, offy );
}

rpk_fixdbl_t
rpk_zlradd( rpk_zgemm_t *params, const rpk_matrix_t *AB, int infomask )
{
    return rpkx_zlradd( rpk_gctx, params, AB, infomask );
}

rpk_fixdbl_t
rpk_zgemm( rpk_zgemm_t *params )
{
    return rpkx_zgemm( rpk_gctx, params );
}

rpk_fixdbl_t
rpk_zge2lr_svd( rpk_int_t     rklimit,
                rpk_int_t     m,
                rpk_int_t     n,
                const void   *Avoid,
                rpk_int_t     lda,
                rpk_matrix_t *Alr )
{
    return rpkx_zge2lr_svd( rpk_gctx, rklimit, m, n, Avoid, lda, Alr );
}

rpk_fixdbl_t
rpk_zrradd_svd( rpk_trans_t         transAu,
                rpk_trans_t         transAv,
                const void         *alphaptr,
                rpk_int_t           M1,
                rpk_int_t           N1,
                const rpk_matrix_t *A,
                rpk_int_t           M2,
                rpk_int_t           N2,
                rpk_matrix_t       *B,
                rpk_int_t           offx,
                rpk_int_t           offy )
{
    return rpkx_zrradd_svd(
        rpk_gctx, transAu, transAv, alphaptr, M1, N1, A, M2, N2, B, offx, offy );
}

rpk_fixdbl_t
rpk_zge2lr_pqrcp( rpk_int_t     rklimit,
                  rpk_int_t     m,
                  rpk_int_t     n,
                  const void   *Avoid,
                  rpk_int_t     lda,
                  rpk_matrix_t *Alr )
{
    return rpkx_zge2lr_pqrcp( rpk_gctx, rklimit, m, n, Avoid, lda, Alr );
}

rpk_fixdbl_t
rpk_zrradd_pqrcp( rpk_trans_t         transAu,
                  rpk_trans_t         transAv,
                  const void         *alphaptr,
                  rpk_int_t           M1,
                  rpk_int_t           N1,
                  const rpk_matrix_t *A,
                  rpk_int_t           M2,
                  rpk_int_t           N2,
                  rpk_matrix_t       *B,
                  rpk_int_t           offx,
                  rpk_int_t           offy )
{
    return rpkx_zrradd_pqrcp(
        rpk_gctx, transAu, transAv, alphaptr, M1, N1, A, M2, N2, B, offx, offy );
}

rpk_fixdbl_t
rpk_zge2lr_rqrcp( rpk_int_t     rklimit,
                  rpk_int_t     m,
                  rpk_int_t     n,
                  const void   *Avoid,
                  rpk_int_t     lda,
                  rpk_matrix_t *Alr )
{
    return rpkx_zge2lr_rqrcp( rpk_gctx, rklimit, m, n, Avoid, lda, Alr );
}

rpk_fixdbl_t
rpk_zrradd_rqrcp( rpk_trans_t         transAu,
                  rpk_trans_t         transAv,
                  const void         *alphaptr,
                  rpk_int_t           M1,
                  rpk_int_t           N1,
                  const rpk_matrix_t *A,
                  rpk_int_t           M2,
                  rpk_int_t           N2,
                  rpk_matrix_t       *B,
                  rpk_int_t           offx,
                  rpk_int_t           offy )
{
    return rpkx_zrradd_rqrcp(
        rpk_gctx, transAu, transAv, alphaptr, M1, N1, A, M2, N2, B, offx, offy );
}

rpk_fixdbl_t
rpk_zge2lr_tqrcp( rpk_int_t     rklimit,
                  rpk_int_t     m,
                  rpk_int_t     n,
                  const void   *Avoid,
                  rpk_int_t     lda,
                  rpk_matrix_t *Alr )
{
    return rpkx_zge2lr_tqrcp( rpk_gctx, rklimit, m, n, Avoid, lda, Alr );
}

rpk_fixdbl_t
rpk_zrradd_tqrcp( rpk_trans_t         transAu,
                  rpk_trans_t         transAv,
                  const void         *alphaptr,
                  rpk_int_t           M1,
                  rpk_int_t           N1,
                  const rpk_matrix_t *A,
                  rpk_int_t           M2,
                  rpk_int_t           N2,
                  rpk_matrix_t       *B,
                  rpk_int_t           offx,
                  rpk_int_t           offy )
{
    return rpkx_zrradd_tqrcp(
        rpk_gctx, transAu, transAv, alphaptr, M1, N1, A, M2, N2, B, offx, offy );
}

rpk_fixdbl_t
rpk_zge2lr_rqrrt( rpk_int_t     rklimit,
                  rpk_int_t     m,
                  rpk_int_t     n,
                  const void   *Avoid,
                  rpk_int_t     lda,
                  rpk_matrix_t *Alr )
{
    return rpkx_zge2lr_rqrrt( rpk_gctx, rklimit, m, n, Avoid, lda, Alr );
}

rpk_fixdbl_t
rpk_zge2lr_qrcp( rpk_zrrqr_cp_t rrqrfct,
                 rpk_int_t      rklimit,
                 rpk_int_t      m,
                 rpk_int_t      n,
                 const void    *Avoid,
                 rpk_int_t      lda,
                 rpk_matrix_t  *Alr )
{
    return rpkx_zge2lr_qrcp( rpk_gctx, rrqrfct, rklimit, m, n, Avoid, lda, Alr );
}

rpk_fixdbl_t
rpk_zge2lr_qrrt( rpk_zrrqr_rt_t rrqrfct,
                 rpk_int_t      rklimit,
                 rpk_int_t      m,
                 rpk_int_t      n,
                 const void    *Avoid,
                 rpk_int_t      lda,
                 rpk_matrix_t  *Alr )
{
    return rpkx_zge2lr_qrrt( rpk_gctx, rrqrfct, rklimit, m, n, Avoid, lda, Alr );
}

rpk_fixdbl_t
rpk_zrradd_qr( rpk_zrrqr_cp_t      rrqrfct,
               rpk_trans_t         transAu,
               rpk_trans_t         transAv,
               const void         *alphaptr,
               rpk_int_t           M1,
               rpk_int_t           N1,
               const rpk_matrix_t *A,
               rpk_int_t           M2,
               rpk_int_t           N2,
               rpk_matrix_t       *B,
               rpk_int_t           offx,
               rpk_int_t           offy )
{
    return rpkx_zrradd_qr(
        rpk_gctx, rrqrfct, transAu, transAv, alphaptr, M1, N1, A, M2, N2, B, offx, offy );
}

rpk_fixdbl_t
rpk_ztrsm( rpk_side_t             side,
             rpk_uplo_t             uplo,
             rpk_trans_t            trans,
             rpk_diag_t             diag,
             rpk_int_t              m,
             rpk_int_t              n,
             rpk_complex64_t        alpha,
             const rpk_complex64_t *A,
             rpk_int_t              lda,
             rpk_matrix_t          *B )
{
    return rpkx_ztrsm(
        rpk_gctx, side, uplo, trans, diag, m, n, alpha, A, lda, B );
}

rpk_fixdbl_t
rpk_zherk( rpk_uplo_t          uplo,
           rpk_trans_t         trans,
           rpk_int_t           n,
           rpk_int_t           k,
           double              alpha,
           const rpk_matrix_t *A,
           double              beta,
           rpk_complex64_t    *C,
           int                 ldc,
           rpk_complex64_t    *W )
{
    return rpkx_zherk( rpk_gctx, uplo, trans, n, k, alpha, A, beta, C, ldc, W );
}
