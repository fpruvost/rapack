/**
 *
 * @file rpk_zlrcpy.c
 *
 * RAPACK low-rank kernel routines
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Nolan Bredel
 * @date 2024-01-08
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 *******************************************************************************
 *
 * @brief Copy a small low-rank structure into a large one
 *
 *******************************************************************************
 *
 * @param[in] lowrank
 *          The structure with low-rank parameters.
 *
 * @param[in] transAv
 *         @arg RapackNoTrans:   (A.v)' is stored transposed as usual
 *         @arg RapackTrans:      A.v is stored
 *         @arg RapackConjTrans:  A.v is stored
 *
 * @param[in] alpha
 *          The multiplier parameter: B = B + alpha * A
 *
 * @param[in] M1
 *          The number of rows of the matrix A.
 *
 * @param[in] N1
 *          The number of columns of the matrix A.
 *
 * @param[in] A
 *          The low-rank representation of the matrix A.
 *
 * @param[in] M2
 *          The number of rows of the matrix B.
 *
 * @param[in] N2
 *          The number of columns of the matrix B.
 *
 * @param[inout] B
 *          The low-rank representation of the matrix B.
 *
 * @param[in] offx
 *          The horizontal offset of A with respect to B.
 *
 * @param[in] offy
 *          The vertical offset of A with respect to B.
 *
 *******************************************************************************/
void
rpkx_zlrcpy( const rpk_ctx_t    *lowrank,
            rpk_trans_t         transAv,
            rpk_complex64_t     alpha,
            rpk_int_t           M1,
            rpk_int_t           N1,
            const rpk_matrix_t *A,
            rpk_int_t           M2,
            rpk_int_t           N2,
            rpk_matrix_t       *B,
            rpk_int_t           offx,
            rpk_int_t           offy )
{
    rpk_complex64_t *u, *v;
    rpk_int_t        ldau, ldav;
    int              ret;

    assert( ( M1 + offx ) <= M2 );
    assert( ( N1 + offy ) <= N2 );

    ldau = ( A->rk == -1 ) ? A->rkmax : M1;
    ldav = ( transAv == RapackNoTrans ) ? A->rkmax : N1;

    rpk_zlrfree( B );
    rpk_zlralloc( M2, N2, A->rk, B );
    u = B->u;
    v = B->v;

    B->rk = A->rk;

    if ( A->rk == -1 ) {
        if ( ( M1 != M2 ) || ( N1 != N2 ) ) {
            ret = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'A', M2, N2,
                                       0.0, 0.0, u, M2 );
            assert( ret == 0 );
        }
        ret = rpk_zgeadd( RapackNoTrans, M1, N1,
                          alpha, A->u,                 ldau,
                          0.0,   u + M2 * offy + offx, M2 );
        assert( ret == 0 );
    }
    else {
        if ( M1 != M2 ) {
            ret = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'A', M2, B->rk,
                                       0.0, 0.0, u, M2 );
            assert( ret == 0 );
        }
        ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', M1, A->rk,
                                   A->u,     ldau,
                                   u + offx, M2 );
        assert( ret == 0 );

        if ( N1 != N2 ) {
            ret = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'A', B->rk, N2,
                                       0.0, 0.0, v, B->rkmax );
            assert( ret == 0 );
        }
        ret = rpk_zgeadd( transAv, A->rk, N1,
                          alpha, A->v,                ldav,
                          0.0,   v + B->rkmax * offy, B->rkmax );
        assert( ret == 0 );
    }

    (void)lowrank;
    (void)ret;
}
