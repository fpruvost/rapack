/**
 *
 * @file rpk_zgeadd.c
 *
 * RAPACK kernel routines
 *
 * @copyright 2010-2015 Univ. of Tennessee, Univ. of California Berkeley and
 *                      Univ. of Colorado Denver. All rights reserved.
 * @copyright 2012-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Gregoire Pichon
 * @author Pierre Ramet
 * @date 2023-12-14
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 ******************************************************************************
 *
 * @brief Add two matrices together.
 *
 * Perform the operation:  B <- alpha * op(A) + B
 *
 *******************************************************************************
 *
 * @param[in] trans
 *         @arg RapackNoTrans:   No transpose, op( A ) = A;
 *         @arg RapackTrans:     Transpose, op( A ) = A';
 *         @arg RapackConjTrans: Conjugate Transpose, op( A ) = conj(A').
 *
 * @param[in] M
 *          Number of rows of the matrix B.
 *          Number of rows of the matrix A, if trans == RapackNoTrans, number of
 *          columns of A otherwise.
 *
 * @param[in] N
 *          Number of columns of the matrix B.
 *          Number of columns of the matrix A, if trans == RapackNoTrans, number
 *          of rows of A otherwise.
 *
 * @param[in] alpha
 *          Scalar factor of A.
 *
 * @param[in] A
 *          Matrix of size LDA-by-N, if trans == RapackNoTrans, LDA-by-M,
 *          otherwise.
 *
 * @param[in] LDA
 *          Leading dimension of the array A. LDA >= max(1,K).
 *          K = M if trans == RapackNoTrans, K = N otherwise.
 *
 * @param[in] beta
 *          Scalar factor of B.
 *
 * @param[inout] B
 *          Matrix of size LDB-by-N.
 *
 * @param[in] LDB
 *          Leading dimension of the array B. LDB >= max(1,M)
 *
 *******************************************************************************
 *
 * @retval RAPACK_SUCCESS successful exit
 * @retval <0 if -i, the i-th argument had an illegal value
 * @retval 1, not yet implemented
 *
 ******************************************************************************/
int
rpk_zgeadd( rpk_trans_t            trans,
            rpk_int_t              M,
            rpk_int_t              N,
            rpk_complex64_t        alpha,
            const rpk_complex64_t *A,
            rpk_int_t              LDA,
            rpk_complex64_t        beta,
            rpk_complex64_t       *B,
            rpk_int_t              LDB )
{
    int i, j;

#if !defined(NDEBUG)
    if ( (trans < RapackNoTrans)   ||
         (trans > RapackConjTrans) )
    {
        return -1;
    }

    if ( M < 0 ) {
        return -2;
    }
    if ( N < 0 ) {
        return -3;
    }
    if ( ((trans == RapackNoTrans) && (LDA < rpk_imax(1,M)) && (M > 0)) ||
         ((trans != RapackNoTrans) && (LDA < rpk_imax(1,N)) && (N > 0)) )
    {
        return -6;
    }
    if ( ( LDB < rpk_imax( 1, M ) ) && ( M > 0 ) ) {
        return -8;
    }
#endif

    switch ( trans ) {
#if defined(PRECISION_z) || defined(PRECISION_c)
        case RapackConjTrans:
            if ( alpha == 0.0 ) {
                for ( j = 0; j < N; j++ ) {
                    for ( i = 0; i < M; i++, B++ ) {
                        *B = beta * (*B);
                    }
                    B += LDB - M;
                }
            }
            else if ( beta == 0.0 ) {
                for ( j = 0; j < N; j++, A++ ) {
                    for ( i = 0; i < M; i++, B++ ) {
                        *B = alpha * conj( A[LDA * i] );
                    }
                    B += LDB - M;
                }
            }
            else {
                for ( j = 0; j < N; j++, A++ ) {
                    for ( i = 0; i < M; i++, B++ ) {
                        *B = beta * (*B) + alpha * conj( A[LDA * i] );
                    }
                    B += LDB - M;
                }
            }
            break;
#endif /* defined(PRECISION_z) || defined(PRECISION_c) */

        case RapackTrans:
            if ( alpha == 0.0 ) {
                for ( j = 0; j < N; j++ ) {
                    for ( i = 0; i < M; i++, B++ ) {
                        *B = beta * (*B);
                    }
                    B += LDB - M;
                }
            }
            else if ( beta == 0.0 ) {
                for ( j = 0; j < N; j++, A++ ) {
                    for ( i = 0; i < M; i++, B++ ) {
                        *B = alpha * A[LDA * i];
                    }
                    B += LDB - M;
                }
            }
            else {
                for ( j = 0; j < N; j++, A++ ) {
                    for ( i = 0; i < M; i++, B++ ) {
                        *B = beta * (*B) + alpha * A[LDA * i];
                    }
                    B += LDB - M;
                }
            }
            break;

        case RapackNoTrans:
        default:
            if ( alpha == 0.0 ) {
                for ( j = 0; j < N; j++ ) {
                    for ( i = 0; i < M; i++, B++ ) {
                        *B = beta * (*B);
                    }
                    B += LDB - M;
                }
            }
            else if ( beta == 0.0 ) {
                for ( j = 0; j < N; j++ ) {
                    for ( i = 0; i < M; i++, B++, A++ ) {
                        *B = alpha * (*A);
                    }
                    A += LDA - M;
                    B += LDB - M;
                }
            }
            else {
                for ( j = 0; j < N; j++ ) {
                    for ( i = 0; i < M; i++, B++, A++ ) {
                        *B = beta * (*B) + alpha * (*A);
                    }
                    A += LDA - M;
                    B += LDB - M;
                }
            }
    }
    return RAPACK_SUCCESS;
}
