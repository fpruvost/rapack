/**
 *
 * @file rpk_zherk.c
 *
 * RAPACK low-rank kernel routine to apply rank-k update to a Hermitian matrix
 * C by a rapack matrix A.
 *
 * @copyright 2024-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 * @version 1.0.0
 * @author Abel Calluaud
 * @author Mathieu Faverge
 * @date 2024-03-11
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

static rpk_complex64_t zone  = 1.0;
static rpk_complex64_t zzero = 0.0;

/**
 *******************************************************************************
 *
 * @brief Performs one of the Hermitian rank k operations
 *
 *    \f[ C = \alpha A \times A^H + \beta C, \f]
 *    or
 *    \f[ C = \alpha A^H \times A + \beta C, \f]
 *
 *  where alpha and beta are real scalars, C is an n-by-n Hermitian full-rank
 *  matrix, and A is an n-by-k low-rank matrix in the first case and a k-by-n
 *  low-rank matrix in the second case.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The RAPACK context to use.
 *
 * @param[in] uplo
 *          - RapackUpper: Upper triangle of C is stored;
 *          - RapackLower: Lower triangle of C is stored.
 *
 * @param[in] trans
 *          - RapackNoTrans:   \f[ C = \alpha A \times A^H + \beta C; \f]
 *          - RapackConjTrans: \f[ C = \alpha A^H \times A + \beta C. \f]
 *
 * @param[in] n
 *          The order of the matrix C. n >= 0.
 *
 * @param[in] k
 *          If trans = RapackNoTrans, number of columns of the A matrix;
 *          if trans = RapackConjTrans, number of rows of the A matrix.
 *
 * @param[in] alpha
 *          The scalar alpha.
 *
 * @param[in] A
 *          A is a Am-by-An low-rank matrix.
 *          If trans = RapackNoTrans,   Am = n and An = k;
 *          if trans = RapackConjTrans, Am = k and An = n.
 *
 * @param[in] beta
 *          The scalar beta.
 *
 * @param[in,out] C
 *          C is an ldc-by-n matrix.
 *          On exit, the uplo part of the matrix is overwritten
 *          by the uplo part of the updated matrix.
 *
 * @param[in] LDC
 *          The leading dimension of the array C. ldc >= max(1, n).
 *
 * @param[in,out] W
 *          Workspace of size at least ( A->rk * (A->rk + n) ). If W is NULL, a
 *          workspace is allocated withing the kernel.
 *
 *******************************************************************************
 *
 * @return -i, if the i^th parameter is incorrect
 *         the number of flops performed by the kernel if succeed.
 *
 */
rpk_fixdbl_t
rpkx_zherk( const rpk_ctx_t    *ctx,
            rpk_uplo_t          uplo,
            rpk_trans_t         trans,
            rpk_int_t           n,
            rpk_int_t           k,
            double              alpha,
            const rpk_matrix_t *A,
            double              beta,
            rpk_complex64_t    *C,
            int                 ldc,
            rpk_complex64_t    *W )
{
    (void)ctx;
    rpk_fixdbl_t     flops = 0.0;
    rpk_complex64_t *X, *Y, *W1, *W2;
    rpk_int_t        ldx, ldy;
    rpk_trans_t      trans2;

    if ( ( uplo != RapackUpper ) && ( uplo != RapackLower ) ) {
        fprintf( stderr, "illegal value of uplo" );
        return -1;
    }
    if ( ( trans != RapackNoTrans ) && ( trans != RapackConjTrans ) ) {
        fprintf( stderr, "illegal value of trans" );
        return -2;
    }
    if ( n < 0 ) {
        fprintf( stderr, "Illegal value of n" );
        return -3;
    }
    if ( k < 0 ) {
        fprintf( stderr, "Illegal value of k" );
        return -4;
    }
    if ( ( ldc < rpk_imax( 1, n ) ) && ( n > 0 ) ) {
        fprintf( stderr, "Illegal value of ldc" );
        return -9;
    }

    /*
     * A is full rank, it is a regular Hermitian rank-k update.
     */
    if ( A->rk == -1 ) {
        rpk_int_t lda = ( trans == RapackNoTrans ) ? n : k;

        cblas_zherk( CblasColMajor,
                     (CBLAS_UPLO)uplo,
                     (CBLAS_TRANSPOSE)trans,
                     n,
                     k,
                     alpha,
                     A->u,
                     lda,
                     beta,
                     C,
                     ldc );

        return flops_zherk( k, n );
    }

    /*
     * A is null, let's scale C by beta.
     */
    if ( A->rk == 0 ) {
        flops = rpk_zlascal( uplo, n, n, (rpk_complex64_t)beta, C, ldc );
        return flops;
    }

    /*
     * We need to compute: C = alpha * op(A) * op(A)^h + beta C
     *                       = alpha * op(A->u * A->v^h) * op(A->u * A->v^h)^h + beta C
     * so we have:
     *                     C = alpha * A->u * A->v^h * A->v * A->u^h + beta C
     *                  or C = alpha * A->v * A->u^h * A->u * A->v^h + beta C
     * let's rewrite both cases as:
     *                     C = alpha * X * Y * Y^h * X^h + beta C
     * Then, we can start by the computation of Y^h * Y which is supposed to
     * generate the smallest matrix by construction.
     */
    if ( trans == RapackNoTrans ) {
        X   = A->u;
        ldx = n;
        Y   = A->v;
        ldy = A->rkmax;

        trans2 = RapackConjTrans;
    }
    else {
        X   = A->v;
        ldx = A->rkmax;
        Y   = A->u;
        ldy = k;

        trans2 = RapackNoTrans;
    }

    if ( W == NULL ) {
        W1 = malloc( A->rk * ( A->rk + n ) * sizeof( rpk_complex64_t ) );
    }
    else {
        W1 = W;
    }
    W2 = W1 + A->rk * A->rk;

    /*
     * Compute Y * Y^h
     */
    cblas_zgemm( CblasColMajor,
                 (CBLAS_TRANSPOSE)trans,
                 (CBLAS_TRANSPOSE)trans2,
                 A->rk,
                 A->rk,
                 k,
                 CBLAS_SADDR( zone ),
                 Y,
                 ldy,
                 Y,
                 ldy,
                 CBLAS_SADDR( zzero ),
                 W1,
                 A->rk );
    flops += flops_zgemm( A->rk, A->rk, k );

    /*
     * Compute X * (Y * Y^h)
     */
    cblas_zgemm( CblasColMajor,
                 (CBLAS_TRANSPOSE)trans,
                 CblasNoTrans,
                 n,
                 A->rk,
                 A->rk,
                 CBLAS_SADDR( zone ),
                 X,
                 ldx,
                 W1,
                 A->rk,
                 CBLAS_SADDR( zzero ),
                 W2,
                 n );
    flops += flops_zgemm( n, A->rk, A->rk );

    /*
     * Compute the hermitian update alpha * (X * (Y * Y^h)) * X^h + beta * C
     */
    rpk_zgemmt( uplo,
                RapackNoTrans,
                trans2,
                n,
                n,
                A->rk,
                (rpk_complex64_t)alpha,
                W2,
                n,
                X,
                ldx,
                (rpk_complex64_t)beta,
                C,
                ldc,
                NULL );
    flops += flops_zgemmt( uplo, n, n, A->rk );

    if ( W == NULL ) {
        free( W1 );
    }

    return flops;
}
