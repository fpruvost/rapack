/**
 *
 * @file rpk_zlr2ge.c
 *
 * RAPACK low-rank kernel routines
 *
 * @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Nolan Bredel
 * @date 2023-12-14
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

#ifndef DOXYGEN_SHOULD_SKIP_THIS
static rpk_complex64_t zone  = 1.0;
static rpk_complex64_t zzero = 0.0;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

/**
 *******************************************************************************
 *
 * @brief Convert a low rank matrix into a dense matrix.
 *
 * Convert a low-rank matrix of size m-by-n into a full rank matrix.
 * A = op( u * v^t ) with op(A) = A or A^t
 *
 *******************************************************************************
 *
 * @param[in] trans
 *          @arg RapackNoTrans: returns A = u * v^t
 *          @arg RapackTrans: returns A = v * u^t
 *
 * @param[in] m
 *          Number of rows of the low-rank matrix Alr.
 *
 * @param[in] n
 *          Number of columns of the low-rank matrix Alr.
 *
 * @param[in] Alr
 *          The low rank matrix to be converted into a full rank matrix
 *
 * @param[inout] A
 *          The matrix of dimension lda-by-k in which to store the uncompressed
 *          version of Alr. k = n if trans == RapackNoTrans, m otherwise.
 *
 * @param[in] lda
 *          The leading dimension of the matrix A. lda >= max(1, m) if trans ==
 *          RapackNoTrans, lda >= max(1,n) otherwise.
 *
 *******************************************************************************
 *
 * @retval  0  in case of success.
 * @retval  -i if the ith parameter is incorrect.
 *
 *******************************************************************************/
int
rpk_zlr2ge( rpk_trans_t         trans,
            rpk_int_t           m,
            rpk_int_t           n,
            const rpk_matrix_t *Alr,
            rpk_complex64_t    *A,
            rpk_int_t           lda )
{
    int ret = 0;

#if !defined( NDEBUG )
    if ( m < 0 ) {
        return -1;
    }
    if ( n < 0 ) {
        return -2;
    }
    if ( ( Alr == NULL ) || ( Alr->rk > Alr->rkmax ) ) {
        return -3;
    }
    if ( (trans == RapackNoTrans && lda < m) ||
         (trans != RapackNoTrans && lda < n) )
    {
        return -5;
    }
    if ( Alr->rk == -1 ) {
        if ( ( Alr->u == NULL ) || ( Alr->v != NULL ) || ( Alr->rkmax < m ) ) {
            return -6;
        }
    }
    else if ( Alr->rk != 0 ) {
        if ( ( Alr->u == NULL ) || ( Alr->v == NULL ) ) {
            return -6;
        }
    }
#endif

    if ( trans == RapackNoTrans ) {
        if ( Alr->rk == -1 ) {
            ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', m, n,
                                       Alr->u, Alr->rkmax, A, lda );
            assert( ret == 0 );
        }
        else if ( Alr->rk == 0 ) {
            ret = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'A', m, n,
                                       0.0, 0.0, A, lda );
            assert( ret == 0 );
        }
        else {
            cblas_zgemm( CblasColMajor, CblasNoTrans, CblasNoTrans,
                         m, n, Alr->rk,
                         CBLAS_SADDR(zone),  Alr->u, m,
                                             Alr->v, Alr->rkmax,
                         CBLAS_SADDR(zzero), A,      lda);
        }
    }
    else {
        if ( Alr->rk == -1 ) {
            rpk_zgetmo( m, n, Alr->u, Alr->rkmax, A, lda );
        }
        else if ( Alr->rk == 0 ) {
            ret = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'A', n, m,
                                       0.0, 0.0, A, lda );
            assert( ret == 0 );
        }
        else {
            cblas_zgemm( CblasColMajor, CblasTrans, CblasTrans,
                         n, m, Alr->rk,
                         CBLAS_SADDR(zone),  Alr->v, Alr->rkmax,
                                             Alr->u, m,
                         CBLAS_SADDR(zzero), A,      lda );
        }
    }

    return ret;
}
