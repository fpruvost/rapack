"""
 @file local_subs.py

 Python RAPACK specific substitution rules for the Precision Generator script.

 @copyright 2017-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
                      Univ. Bordeaux. All rights reserved.

 @version 1.0.0
 @author Mathieu Faverge
 @author Abel Calluaud
 @date 2024-02-14

"""
subs = {
    # ------------------------------------------------------------
    # replacements applied to mixed precision files.
    'normal': [
        # pattern                single                  double                  single-complex          double-complex
        #'12345678901234567890', '12345678901234567890', '12345678901234567890', '12345678901234567890', '12345678901234567890')
        ('int',                  'float',                'double',               'rpk_complex32_t',     r'\brpk_complex64_t'   ),
        ('RapackPattern',        'RapackFloat',          'RapackDouble',         'RapackComplex32',     r'\bRapackComplex64'   ),
        ('RapackPattern',        'RapackFloat',          'RapackDouble',         'RapackFloat',         r'\bRapackDouble'      ),

        # ----- Variables
        (r'\b',                 r'szero\b',             r'dzero\b',             r'czero\b',             r'zzero\b'             ),
        (r'\b',                 r'sone\b',              r'done\b',              r'cone\b',              r'zone\b'              ),

        # ----- RAPACK Prefixes
        ('rpk_p',                'rpk_s',                'rpk_d',                'rpk_c',               r'\brpk_z'             ), # Protect rpk_[zd] to avoid rpk_diag_t matching
        ('rpkx_p',               'rpkx_s',               'rpkx_d',               'rpkx_c',              r'rpkx_z'              ),
        ('testrpk_p',            'testrpk_s',            'testrpk_d',            'testrpk_c',           r'testrpk_z'           ),
        ('_rpk_p',               '_rpk_s',               '_rpk_d',               '_rpk_c',              r'_rpk_z'              ), # Header define
        ('rpk_p',                'rpk_s',                'rpk_d',                'rpk_s',               r'\brpk_d'             ),
        ('rpkx_p',               'rpkx_s',               'rpkx_d',               'rpkx_s',              r'rpkx_d'              ),
        ('RPK_P',                'RPK_S',                'RPK_D',                'RPK_C',               r'RPK_Z'               ),
        ('RPK_P',                'RPK_S',                'RPK_D',                'RPK_S',               r'RPK_D'               ),
        ('prapack',              'srapack',              'drapack',              'crapack',             r'\bzrapack'           ),
    ]
}

exceptfrom = [ r'\brpk_diag_t' ]
