#!/usr/bin/env bash
###
#
#  @file build.sh
#  @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @version 1.0.0
#  @author Mathieu Faverge
#  @author Florent Pruvost
#  @date 2023-12-14
#
###

set -e
set -x

#
# Build the project
#
if [[ "$SYSTEM" != "windows" ]]
then
    if [[ "$SYSTEM" == "macosx" ]]
    then
        # clang is used on macosx and it is not compatible with MORSE_ENABLE_COVERAGE=ON
        cmake -B build -S . -DRAPACK_CI_VERSION=${VERSION} -DRAPACK_CI_BRANCH=${BRANCH} -C .gitlab/ci-test-initial-cache.cmake -DMORSE_ENABLE_COVERAGE=OFF
    else
        cmake -B build -S . -DRAPACK_CI_VERSION=${VERSION} -DRAPACK_CI_BRANCH=${BRANCH} -C .gitlab/ci-test-initial-cache.cmake
    fi
    cmake --build build -j 4
    cmake --install build
else
    cmake -GNinja -B build -S . -DCMAKE_INSTALL_PREFIX=$PWD/install-${VERSION} -DBUILD_SHARED_LIBS=ON
    cmake --build build -j 4
    cmake --install build
fi

#
# Check link to rapack
#
cd .gitlab/check_link/

# Set the compiler
if [[ "$SYSTEM" == "macosx" ]]; then
    export CC=clang
else
    export CC=gcc
fi
export FC=gfortran

# Set the path variables
if [[ "$SYSTEM" == "linux" ]]; then
    export LIBRARY_PATH=$PWD/../../install-${VERSION}/lib:$LIBRARY_PATH
    export LD_LIBRARY_PATH=$PWD/../../install-${VERSION}/lib:$LD_LIBRARY_PATH
elif [[ "$SYSTEM" == "macosx" ]]; then
    export LIBRARY_PATH=$PWD/../../install-${VERSION}/lib:$LIBRARY_PATH
    export DYLD_LIBRARY_PATH=$PWD/../../install-${VERSION}/lib:$DYLD_LIBRARY_PATH
elif [[ "$SYSTEM" == "windows" ]]; then
    export PATH="/c/Windows/WinSxS/x86_microsoft-windows-m..namespace-downlevel_31bf3856ad364e35_10.0.19041.1_none_21374cb0681a6320":$PATH
    export PATH=$PWD/../../install-${VERSION}/bin:$PATH
fi

# 1) Check using cmake:
./link_cmake.sh $PWD/../../install-${VERSION}
# 2) Check using pkg-config:
./link_pkgconfig.sh $PWD/../../install-${VERSION}

# Clean the check build
rm -r build
