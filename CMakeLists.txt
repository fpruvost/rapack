###
#
#  @copyright 2013-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @version 1.0.0
#  @author Mathieu Faverge
#  @author Florent Pruvost
#  @author Tony Delarue
#  @date 2024-01-08
#
###
cmake_minimum_required (VERSION 3.12)
project(RAPACK C Fortran)

# Check if compiled independently or within another project
if ( ${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_CURRENT_SOURCE_DIR})
  set( BUILD_AS_SUBPROJECT OFF )

  option(BUILD_SHARED_LIBS
    "Build shared libraries" OFF)
  if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build, options are None, Debug, Release, RelWithDebInfo and MinSizeRel." FORCE)
  endif(NOT CMAKE_BUILD_TYPE)

  if (IS_DIRECTORY ${CMAKE_SOURCE_DIR}/cmake_modules/morse_cmake/modules)
    list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake_modules/morse_cmake/modules)
    include(MorseInit)
  else()
    message(FATAL_ERROR "Submodule cmake_morse not initialized - run `git submodule update --init`")
  endif()

  ## Executable and tests
  enable_testing()
  include(CTest)

  include(CheckCCompilerFlag)

  # Set warnings for debug builds
  check_c_compiler_flag( "-Wall" HAVE_WALL )
  if( HAVE_WALL )
    set( C_WFLAGS "${C_WFLAGS} -Wall" )
  endif( HAVE_WALL )
  check_c_compiler_flag( "-Wextra" HAVE_WEXTRA )
  if( HAVE_WEXTRA )
    set( C_WFLAGS "${C_WFLAGS} -Wextra" )
  endif( HAVE_WEXTRA )

  set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_WFLAGS}" )

  # add gdb symbols in debug and relwithdebinfo
  check_c_compiler_flag( "-g3" HAVE_G3 )
  if( HAVE_G3 )
    set( CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -O0 -g3" )
    set( CMAKE_C_FLAGS_RELWITHDEBINFO "${CMAKE_C_FLAGS_RELWITHDEBINFO} -g3" )
  else()
    set( CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -O0" )
  endif( HAVE_G3 )

  # Enable testing when compiling RAPACK as a standalone project
  set( DEFAULT_RAPACK_ENABLE_TESTINGS ON )
else()
  set( BUILD_AS_SUBPROJECT ON )

  # Disable testing when compiling RAPACK as a subproject
  set( DEFAULT_RAPACK_ENABLE_TESTINGS OFF )
endif()

# Define a subproject name for ctest
set(CMAKE_DIRECTORY_LABELS "rapack")
set(CTEST_LABELS_FOR_SUBPROJECTS rapack)

option(RAPACK_INT64
  "Choose between int32 and int64 for integer representation" OFF)

option(RAPACK_MULTITHREAD
  "Choose between sequential and multi-threaded blas/lapack libraries" OFF)

# option(RAPACK_WITH_FORTRAN
#   "Enable Fortran files/interface/examples to be compiled" OFF)

option(RAPACK_DEBUG_NANCHECK
  "Enable nan check in LAPACKE calls" OFF)

# Option to enable testings (needs tmg available)
option(RAPACK_ENABLE_TESTINGS
  "Enable/Disable low-rank testings (Requires LAPACKE with TMG support)" ${DEFAULT_RAPACK_ENABLE_TESTINGS})

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules")
include(AddSourceFiles)

# The current version number
set( RAPACK_VERSION_MAJOR 1 )
set( RAPACK_VERSION_MINOR 0 )
set( RAPACK_VERSION_MICRO 0 )

set( RAPACK_VERSION "${RAPACK_VERSION_MAJOR}.${RAPACK_VERSION_MINOR}.${RAPACK_VERSION_MICRO}" )

# Define precision supported by MAGMA_MORSE
# -----------------------------------------
set( RP_RAPACK_DICTIONNARY ${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules/local_subs.py )
set( RP_RAPACK_PRECISIONS  "s;d;c;z" )
include(RulesPrecisions)

### System parameter detection
include(CheckSystem)

# Set the RPATH config
# --------------------
set(CMAKE_MACOSX_RPATH 1)

# For fPIC when build static
set( CMAKE_POSITION_INDEPENDENT_CODE ON )

# use, i.e. don't skip the full RPATH for the build tree
set(CMAKE_SKIP_BUILD_RPATH FALSE)

# when building, don't use the install RPATH already
# (but later on when installing)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)

# the RPATH to be used when installing
list(APPEND CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

# RAPACK depends on check libm
#-----------------------------
find_package(M REQUIRED)
morse_export_imported_target(MORSE M m rapack)

# RAPACK depends on Lapacke and CBLAS
#---------------------------------
set( CBLAS_MT ${RAPACK_MULTITHREAD} )
find_package(CBLAS REQUIRED)
morse_export_imported_target(MORSE CBLAS cblas rapack)

## Tests requires TMG lib to generate random matrices.
set( LAPACKE_MT ${RAPACK_MULTITHREAD} )
if (RAPACK_ENABLE_TESTINGS)
  find_package(LAPACKE REQUIRED
    COMPONENTS TMG )
else()
  find_package(LAPACKE REQUIRED)
endif()
morse_export_imported_target(MORSE LAPACKE lapacke rapack)

# Configuration header
#---------------------
configure_file (
  "${RAPACK_SOURCE_DIR}/include/rapack/config.h.in"
  "${RAPACK_BINARY_DIR}/include/rapack/config.h")

install( FILES "${RAPACK_BINARY_DIR}/include/rapack/config.h"
  DESTINATION include/rapack )

### reset variables
set(generated_headers "")

### Generate the headers in all precisions
set(HEADERS
  include/rapack/rpk_zapi.h
  include/rapack/rpkx_zapi.h
)

precisions_rules_py(generated_headers
  "${HEADERS}"
  TARGETDIR  "include/rapack"
  PRECISIONS "s;d;c;z")

set(rapack_headers
  ${generated_headers}
  include/rapack.h
  )

# install the headers
install(FILES
  include/rapack.h
  DESTINATION include)

# install the main API header
install(FILES
  include/rapack/const.h
  include/rapack/datatypes.h
  DESTINATION include/rapack)

# Install generated headers
foreach( hdr_file ${generated_headers} )
  install( FILES
    ${CMAKE_CURRENT_BINARY_DIR}/${hdr_file}
    DESTINATION include/rapack )
endforeach()

# to set the dependency librapack -> rapack_headers_tgt
add_custom_target(rapack_headers_tgt
  DEPENDS ${rapack_headers} )

# Add files to the documentation
add_documented_files(
  include/rapack.h
  include/rapack/const.h
  include/rapack/datatypes.h
  )
add_documented_files(
  DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  ${generated_headers}
  )

### RAPACK library
add_subdirectory(src)

### Testing executables
add_subdirectory(testing)

# ### Wrappers
# add_subdirectory(wrappers)

###############################################################################
# Export targets #
##################

# see https://cmake.org/cmake/help/latest/module/CMakePackageConfigHelpers.html
include(CMakePackageConfigHelpers)

set(BIN_INSTALL_DIR "bin/"     CACHE STRING "where to install executables relative to prefix" )
set(INC_INSTALL_DIR "include/" CACHE STRING "where to install headers relative to prefix"     )
set(LIB_INSTALL_DIR "lib/"     CACHE STRING "where to install libraries relative to prefix"   )

configure_package_config_file(cmake_modules/RAPACKConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/RAPACKConfig.cmake
  INSTALL_DESTINATION ${LIB_INSTALL_DIR}/cmake/rapack
  PATH_VARS BIN_INSTALL_DIR INC_INSTALL_DIR LIB_INSTALL_DIR)
write_basic_package_version_file(RAPACKConfigVersion.cmake
  VERSION ${RAPACK_VERSION}
  COMPATIBILITY AnyNewerVersion)

# Install config files
install(FILES
  ${CMAKE_CURRENT_BINARY_DIR}/RAPACKConfig.cmake
  ${CMAKE_CURRENT_BINARY_DIR}/RAPACKConfigVersion.cmake
  DESTINATION lib/cmake/rapack)

### Build pkg-config and environment file
include(GenPkgConfig)
list(APPEND RAPACK_PKGCONFIG_LIBS_PRIVATE
  ${LAPACKE_LIBRARIES}
  ${CBLAS_LIBRARIES}
  ${M_LIBRARIES}
  )

generate_pkgconfig_files(
  "${CMAKE_CURRENT_SOURCE_DIR}/tools/rapack.pc.in"
  PROJECTNAME RAPACK )

# if (RAPACK_WITH_FORTRAN)
#   # reset variables set in rapack
#   set(RAPACK_PKGCONFIG_CFLAGS)
#   set(RAPACK_PKGCONFIG_LIBS)
#   set(RAPACK_PKGCONFIG_REQUIRED)
#   generate_pkgconfig_files(
#     "${CMAKE_CURRENT_SOURCE_DIR}/tools/rapackf.pc.in"
#     PROJECTNAME RAPACK )
# endif (RAPACK_WITH_FORTRAN)

generate_env_file( PROJECTNAME RAPACK )

# Build documentation
# -------------------
add_documented_files(
  README.md
  )
add_subdirectory(docs)
