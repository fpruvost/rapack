/**
 *
 * @file testrpk_zgemm_tests.c
 *
 * Tests and validate the rpk_zgemm() routine.
 *
 * @copyright 2015-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Mathieu Faverge
 * @author Abel Calluaud
 * @date 2024-03-11
 *
 * @precisions normal z -> c d s
 *
 **/
#include "tests.h"
#include "testrpk_z.h"
#include <math.h>

#define PRINT_RES(_ret_)                        \
    if ( _ret_ == -1 ) {                        \
        printf( "UNDEFINED\n" );                \
    }                                           \
    else if( _ret_ > 0 ) {                      \
        printf( "FAILED(%d)\n", _ret_ );        \
        err++;                                  \
    }                                           \
    else {                                      \
        printf( "SUCCESS\n" );                  \
    }

int main( int argc, char **argv )
{
    rpk_int_t        m, n, k, Cm, Cn, offx, offy, use_reltol;
    double           eps       = LAPACKE_dlamch_work( 'e' );
    double           tolerance = sqrt( eps );
    double           threshold = tolerance * tolerance;
    test_matrix_t    A, B, C;
    rpk_complex64_t *Cfr;
    rpk_complex64_t  alpha = -3.48;
    rpk_complex64_t  beta  = 1.;
    rpk_ctx_t        ctx   = rpk_ctx_default;
    int              ranks[3], r, s, i, j, l, meth;
    int              err  = 0;
    int              ret  = 0;
    int              mode = 0;

    ctx.use_reltol = 0;
    ctx.tolerance  = tolerance;
    ctx.rpk_ge2lr  = rpkx_zge2lr_svd;
    ctx.rpk_rradd  = rpkx_zrradd_svd;

    for ( use_reltol = 0; use_reltol < 2; use_reltol++ ) {
        ctx.use_reltol = use_reltol;
        for ( s = 100; s <= 200; s = 2 * s ) {
            ranks[0] = s + 1;
            ranks[1] = 16;
            ranks[2] = 2;

            m = s / 2;
            n = s / 4;
            k = s / 6;

            offx = 1;
            offy = 2;

            Cm = s;
            Cn = s;

            /* Matrix A */
            for ( i = 0; i < 3; i++ ) {
                A.m  = m;
                A.n  = k;
                A.ld = m;
                A.rk = rpk_imin( m, k ) / ranks[i];
                testrpk_zgenmat_comp( &ctx, mode, threshold, &A );

                /* Matrix B */
                for ( j = 0; j < 3; j++ ) {
                    B.m  = n;
                    B.n  = k;
                    B.ld = n;
                    B.rk = rpk_imin( n, k ) / ranks[j];
                    testrpk_zgenmat_comp( &ctx, mode, threshold, &B );

                    /* Matrix C */
                    for ( l = 0; l < 3; l++ ) {
                        C.m  = Cm;
                        C.n  = Cn;
                        C.ld = Cm;
                        C.rk = rpk_imin( Cm, Cn ) / ranks[l];
                        testrpk_zgenmat_comp( &ctx, mode, threshold, &C );

                        printf( "  -- Test LRMM Cm=%ld, Cn=%ld, m=%ld, n=%ld, k=%ld, rA=%ld, rB=%ld, rC=%ld (%s)\n",
                                (long)Cm, (long)Cn, (long)m, (long)n, (long)k,
                                (long)(A.lr.rk), (long)(B.lr.rk), (long)(C.lr.rk),
                                use_reltol ? "Relative": "Absolute" );

                        /* Compute the full rank GEMM */
                        Cfr = C.fr;
                        cblas_zgemm( CblasColMajor, CblasNoTrans, CblasConjTrans,
                                     m, n, k,
                                     CBLAS_SADDR( alpha ), A.fr, A.ld,
                                                           B.fr, B.ld,
                                     CBLAS_SADDR( beta ),  Cfr + C.ld * offy + offx, C.ld );

                        /* Update the norm of C with the norm of the result */
                        C.norm = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f', Cm, Cn,
                                                      Cfr, C.ld, NULL );

                        fprintf( stdout, "%7s %4s %12s %12s %12s %12s\n",
                                 "Method", "Rank", "Time", "||C||_f", "||c(C)-C||_f",
                                 "||c(C)-C||_f/(||C||_f * eps)" );

                        ret = 0;

                        /* Let's test all methods we have */
                        for ( meth = 0; meth < RapackCompressMethodNbr; meth++ ) {
                            ctx.rpk_ge2lr = rpk_ge2lr_functions[meth][RapackComplex64 - 2];
                            ctx.rpk_rradd = rpk_rradd_functions[meth][RapackComplex64 - 2];

                            r = testrpk_zcheck_gemm( &ctx, meth, offx, offy,
                                                     alpha, &A, &B, beta, &C );
                            ret += r * (1 << meth);
                        }
                        ctx.rpk_ge2lr = rpk_ge2lr_functions[RapackCompressMethodSVD][RapackComplex64-2];
                        ctx.rpk_rradd = rpk_rradd_functions[RapackCompressMethodSVD][RapackComplex64-2];

                        rpk_zlrfree( &(C.lr) );
                        free( C.fr );
                        PRINT_RES( ret );
                    }

                    rpk_zlrfree( &(B.lr) );
                    free( B.fr );
                }
                rpk_zlrfree( &(A.lr) );
                free( A.fr );
            }
        }
    }

    (void)argc;
    (void)argv;

    if ( err == 0 ) {
        printf( " -- All tests PASSED --\n" );
        return EXIT_SUCCESS;
    }
    else {
        printf( " -- %d tests FAILED --\n", err );
        return EXIT_FAILURE;
    }
}
