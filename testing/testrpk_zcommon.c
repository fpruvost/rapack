/**
 *
 * @file testrpk_zcommon.c
 *
 * Test functions for the low-rank kernels.
 *
 * @copyright 2015-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Mathieu Faverge
 * @author Esragul Korkmaz
 * @author Abel Calluaud
 * @date 2024-03-11
 *
 * @precisions normal z -> z c d s
 *
 **/
#ifndef DOXYGEN_SHOULD_SKIP_THIS
#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
#include "common.h"
#include "tests.h"
#include "testrpk_z.h"
#include <math.h>

#define PI 3.14159265358979323846

static rpk_complex64_t zone  = 1.0;
static rpk_complex64_t zzero = 0.0;

static int ISEED[4] = { 42, 15, 314, 666 }; /* initial seed for zlarnv() */

/**
 *******************************************************************************
 *
 * @brief Generate random matrices with given singular values in dense format.
 *
 *******************************************************************************
 *
 * @param[in] mode
 *          Defines the distribution of the singular values of the generated
 *          matrix.
 *          There are three experimental cases issued from the paper:
 *          "Martinsson, P. G. (2015). _Blocked rank-revealing QR factorizations
 *          : How randomized sampling can be used to avoid single-vector
 *          pivoting_" where we test different singular value distributions based
 *          on mode value:
 *          0) A is an i.i.d normalized Gaussian matrix, so singular values
 *             should decrease slowly and drop at the end
 *          2) A = U D V^H where U,V are random orthonormal matrices. The
 *             singular values are flat up to a point, after decrease rapidly
 *             and then level out again to threshold. (Uses cubic spline
 *             interpolation to describe the curve)
 *          3) A = U D V^H where U,V are random orthonormal matrices. The
 *             singular values are flat up to a point, after decrease rapidly
 *             and then level out again to threshold= tolerance^2. (Uses cosinus
 *             to describe the curve)
 *          -) A = U D V^H where U,V are random orthonormal matrices, and
 *             D(i,i) = max( exp(log(tolerance) / rank) ^ i, threshold )
 *
 * @param[in] tolerance
 *          Define the value of the middle point in the singular values
 *          distribution.
 *
 * @param[in] threshold
 *          Defines the value of the plateau value in the singular values
 *          distribution.
 *
 * @param[in,out] tA
 *          The test matrix to generate.
 *          On entry, m, n, ld, and rk must be defined.
 *          On output, tA->fr is filled with the generated random matrix of size
 *          m-by-n.
 *
 *******************************************************************************/
int
testrpk_zgenmat( int            mode,
                 double         tolerance,
                 double         threshold,
                 test_matrix_t *tA )
{
    rpk_int_t        rank  = tA->rk;
    rpk_int_t        m     = tA->m;
    rpk_int_t        n     = tA->n;
    rpk_complex64_t *A     = tA->fr;
    rpk_int_t        lda   = tA->ld;
    rpk_int_t        minMN = rpk_imin( m, n );
    double           rcond = (double)minMN;
    double           dmax  = 1.0;
    rpk_complex64_t *work;
    double          *S;
    int              i, rc;
    int              SEED[4] = { 26, 67, 52, 197 };

    if ( m < 0 ) {
        fprintf( stderr, "Invalid m parameter\n" );
        return -4;
    }
    if ( n < 0 ) {
        fprintf( stderr, "Invalid n parameter\n" );
        return -5;
    }
    if ( lda < m ) {
        fprintf( stderr, "Invalid lda parameter\n" );
        return -6;
    }
    if ( rank > rpk_imin( m, n ) ) {
        fprintf( stderr, "Invalid rank parameter\n" );
        return -3;
    }

    if ( rank == 0 ) {
        rc = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'A', m, n,
                                  0., 0., A, lda );
        assert( rc == 0 );
        tA->norm = 0.;
        return 0;
    }

    S    = malloc( minMN * sizeof( double ) );
    work = malloc( 3 * rpk_imax( m, n ) * sizeof( rpk_complex64_t ) );

    if ( ( !S ) || ( !work ) ) {
        fprintf( stderr, "Out of Memory\n" );
        free( S );
        free( work );
        return -2;
    }

    /*
     * There are three experimental cases issued from the paper: "Martinsson,
     * P. G. (2015). _Blocked rank-revealing QR factorizations : How randomized
     * sampling can be used to avoid single-vector pivoting_"
     * where we test different singular value distributions based on mode value:
     *    0) A is an i.i.d normalized Gaussian matrix, so singular values should
     *       decrease slowly and drop at the end
     *    2) A = U D V^H where U,V are random orthonormal matrices. The singular
     *       values are flat up to a point, after decrease rapidly and then
     *       level out again to threshold. (Uses cubic spline interpolation to
     *       describe the curve)
     *    3) A = U D V^H where U,V are random orthonormal matrices. The singular
     *       values are flat up to a point, after decrease rapidly and then
     *       level out again to threshold= tolerance^2. (Uses cosinus to
     *       describe the curve)
     *    -) A = U D V^H where U,V are random orthonormal matrices, and
     *           D(i,i) = max( exp(log(tolerance) / rank) ^ i, threshold )
     */
    switch ( mode ) {
    case 0: {
        /* Generate  a random matrix of rank rank */
        rpk_complex64_t *O1     = malloc( m * rank * sizeof( rpk_complex64_t ) );
        rpk_complex64_t *O2     = malloc( n * rank * sizeof( rpk_complex64_t ) );
        rpk_complex64_t *A2     = malloc( n * lda  * sizeof( rpk_complex64_t ) );
        double          *superb = malloc( minMN    * sizeof( double ) );
        double           alpha;

        rc = LAPACKE_zlarnv_work( 3, SEED, m * rank, O1 );
        assert( rc == 0 );
        rc = LAPACKE_zlarnv_work( 3, SEED, n * rank, O2 );
        assert( rc == 0 );

        cblas_zgemm( CblasColMajor, CblasNoTrans, CblasNoTrans,
                     m, n, rank,
                     CBLAS_SADDR( zone ),  O1, m,
                                           O2, rpk_imax(rank, 1),
                     CBLAS_SADDR( zzero ), A,  lda );

        rc = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', m, n, A, lda, A2, lda );
        assert( rc == 0 );

        rc = LAPACKE_zgesvd(
            LAPACK_COL_MAJOR, 'n', 'n', m, n, A2, lda, S, NULL, 1, NULL, 1, superb );
        assert( rc == 0 );

        rc = LAPACKE_zlascl_work( LAPACK_COL_MAJOR, 'g', -1, -1, 1., S[0], m, n, A, lda );
        assert( rc == 0 );

        alpha = 1. / S[0];
        cblas_dscal( minMN, alpha, S, 1 );

        free( superb );
        free( A2 );
        free( O1 );
        free( O2 );
    }
        break;

    case 2: {
        int    s1 = rpk_imax( 0, (rank / 2) - 1 );
        int    s2 = rpk_imin( rpk_imax( rank + ( rank / 2 ) - 1, rank ),
                              minMN );
        double p1 = log( 1. / tolerance );
        double p2 = log( threshold / tolerance );

        /**
         * a3( p1, p2 ) = abs(p1) <= abs(p2) ? -     p1           :  3  * p2 + 2. * p1
         * a2( p1, p2 ) = abs(p1) <= abs(p2) ? -3. * p1           :  6. * p2 + 3. * p1
         * a1( p1, p2 ) = abs(p1) <= abs(p2) ? -3. * p1           :  3. * p2
         * b3( p1, p2 ) = abs(p1) <= abs(p2) ? -3  * p1 - 2. * p2 :       p2
         * b2( p1, p2 ) = abs(p1) <= abs(p2) ?  6. * p1 + 3. * p2 : -3. * p2
         * b1( p1, p2 ) = abs(p1) <= abs(p2) ? -3. * p1           :  3. * p2
         *
         * A(p1, p2, t) = a3(p1,p2) * t*t*t + a2(p1,p2) *t*t + a1(p1, p2) * t
         * B(p1, p2, t) = b3(p1,p2) * t*t*t + b2(p1,p2) *t*t + b1(p1, p2) * t
         *
         * t(x) = (x-100) / 50
         * f(p1, p2, x) = x < 100 ? A(p1, p2, t(x)) : B(p1, p2, t(x))
         */
        double a1, a2, a3, b1, b2, b3;
        if ( fabs( p1 ) < fabs( p2 ) ) {
            a3 = -p1;
            a2 = -3. * p1;
            a1 = -3. * p1;
            b3 = -3 * p1 - 2. * p2;
            b2 = 6. * p1 + 3. * p2;
            b1 = -3. * p1;
        }
        else {
            a3 = 3 * p2 + 2. * p1;
            a2 = 6. * p2 + 3. * p1;
            a1 = 3. * p2;
            b3 = p2;
            b2 = -3. * p2;
            b1 = 3. * p2;
        }

        for ( i = 0; i <= s1; i++ ) {
            S[i] = 1.;
        }

        for ( i = s1; i <= rank-1; i++ ) {
            double x  = ( (double)( 2 * i - s1 - s2 ) / (double)( s2 - s1 ) );
            double Ax = x * ( a1 + x * ( a2 + a3 * x ) );
            S[i]      = tolerance * exp( Ax );
        }

        for ( ; i < s2; i++ ) {
            double x  = ( (double)( 2 * i - s1 - s2 ) / (double)( s2 - s1 ) );
            double Bx = x * ( b1 + x * ( b2 + b3 * x ) );
            S[i]      = tolerance * exp( Bx );
        }

        for ( ; i < minMN; i++ ) {
            S[i] = threshold;
        }
    }
        break;

    case 3: {
        int    s1    = rank / 2;
        int    s2    = rpk_imin( rank + ( rank / 2 ), minMN );
        double tol2  = tolerance * tolerance;
        double alpha = exp( 2. * log( tolerance ) / rank );

        for ( i = 0; i <= s1; i++ ) {
            S[i] = 1.;
        }

        for ( ; i <= rpk_imin(s2,minMN-1); i++ ) {
            S[i] = S[i - 1] * alpha;
        }

        for ( i = s1; i < s2; i++ ) {
            double x    = ( (double)( i - s1 ) / (double)rank );
            double cosx = -cos( x * PI );
            S[i]        = tolerance * exp( log( tolerance ) * cosx );
        }

        for ( ; i < minMN; i++ ) {
            S[i] = tol2;
        }
    }
        break;

    default: {
        double alpha;
        alpha = exp( log( tolerance ) / rank );
        S[0] = alpha;
        for ( i = 1; i < minMN; i++ ) {
            if ( rank == i ) {
                alpha = exp( 2 * log( threshold/tolerance ) / rank );
            }
            S[i] = S[i - 1] * alpha;
            if ( S[i] <= threshold ) {
                alpha = 1.;
            }
        }
    }
    }

    /* Initialize A */
    rc = LAPACKE_zlatms_work( LAPACK_COL_MAJOR, m, n, 'U', ISEED, 'N', S, 0,
                              rcond, dmax, m, n, 'N', A, lda, work );
    assert( rc == 0 );

    tA->norm = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f', m, n, A, lda, NULL );

    /* Let's store the singular values in a file */
    {
        char *filename;
        FILE *f;

        rc = asprintf( &filename,
                       "singular_values_N%d_mode%d_rank%d_tol%e.txt",
                       (int)minMN, (int)mode, (int)rank, tolerance );
        f = fopen( filename, "w" );
        free( filename );

        for ( i = 0; i < minMN; i++ ) {
            fprintf( f, "%4d %e %e\n", (int)(i+1), S[i], cblas_dnrm2( minMN - i, S + i, 1 ) / (tA->norm) );
        }
        fclose( f );
    }

    free( S );
    free( work );
    (void)rc;
    return 0;
}

/**
 *******************************************************************************
 *
 * @brief Generate a random matrix with given singular values in both dense and
 * low-rank format.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The data structure that defines the kernels used for the
 *          compression. It also defines, the tolerance of the low-rank
 *          representation and if absolute or relative tolerance is applied.
 *
 * @param[in] mode
 *          Defines the distribution of the singular values of the generated
 *          matrix.
 *          There are three experimental cases issued from the paper:
 *          "Martinsson, P. G. (2015). _Blocked rank-revealing QR factorizations
 *          : How randomized sampling can be used to avoid single-vector
 *          pivoting_" where we test different singular value distributions based
 *          on mode value:
 *          0) A is an i.i.d normalized Gaussian matrix, so singular values
 *             should decrease slowly and drop at the end
 *          2) A = U D V^H where U,V are random orthonormal matrices. The
 *             singular values are flat up to a point, after decrease rapidly
 *             and then level out again to threshold. (Uses cubic spline
 *             interpolation to describe the curve)
 *          3) A = U D V^H where U,V are random orthonormal matrices. The
 *             singular values are flat up to a point, after decrease rapidly
 *             and then level out again to threshold= tolerance^2. (Uses cosinus
 *             to describe the curve)
 *          -) A = U D V^H where U,V are random orthonormal matrices, and
 *             D(i,i) = max( exp(log(tolerance) / rank) ^ i, threshold )
 *
 * @param[in] threshold
 *          Defines the value of the plateau value in the singular values
 *          distribution.
 *
 * @param[in,out] A
 *          The test matrix to generate.
 *          On entry, m, n, ld, and rk must be defined.
 *          On output, A->fr is filled with the generated random matrix of size
 *          m-by-n, A->lr is the low-rank representation of A->fr with the
 *          compression parameters from lowrank.
 *
 *******************************************************************************/
void
testrpk_zgenmat_comp( const rpk_ctx_t *ctx,
                      int              mode,
                      double           threshold,
                      test_matrix_t   *A )
{
    A->fr = malloc( A->ld * A->n * sizeof(rpk_complex64_t) );
    testrpk_zgenmat( mode, ctx->tolerance, threshold, A );
    ctx->rpk_ge2lr( ctx, rpk_imin( A->m, A->n ),
                    A->m, A->n, A->fr, A->ld, &(A->lr) );
}

/**
 *******************************************************************************
 *
 * @brief Check that a compression kernel is correct.
 *
 * This routine compress a dense matrix into a low-rank form according to the
 * lowrank parameters, and check that the compressed form of the matrix respects
 * the relation:
 * \f$ || A - c(A) || < \tau \f$, with
 *     \f$ \tau = \eps \f$ in absolute compression, and
 *     \f$ \tau = \eps * ||A|| \f$ in relative compression.
 *
 *******************************************************************************
 *
 * @param[in] lowrank
 *          The data structure that defines the kernels used for the
 *          compression. It also defines, the tolerance of the low-rank
 *          representation and if absolute or relative tolerance is applied.
 *
 * @param[in,out] A
 *          The matrix to test.
 *          On entry, m, n, ld, rk, and fr must be defined.
 *          On output, A is unchanged, but A->lr is used during the test to
 *          temporarily store the compressed form of the matrix.
 *
 *******************************************************************************
 *
 * @retval 0 on success
 * @retval <0, if one of the parameter is incorrect
 * @retval >0, if one or more of the tests failed.
 *
 *******************************************************************************/
int
testrpk_zcheck_ge2lr( rpk_ctx_t     *ctx,
                      rpk_compmeth_t compmeth,
                      test_matrix_t *A )
{
    rpk_int_t        m  = A->m;
    rpk_int_t        n  = A->n;
    rpk_int_t        ld = A->ld;
    rpk_complex64_t *A2;
    rpk_int_t        minMN = rpk_imin( m, n );
    Clock            timer, total_timer = 0.;
    double           resid, errbound, norm_residual;
    rpk_int_t        i, rankA, nb_tests = 1;
    int              rc = 0;

    if ( m < 0 ) {
        fprintf( stderr, "Invalid m parameter\n" );
        return -4;
    }
    if ( n < 0 ) {
        fprintf( stderr, "Invalid n parameter\n" );
        return -5;
    }
    if ( ld < m ) {
        fprintf( stderr, "Invalid ld parameter\n" );
        return -6;
    }
    A2 = malloc( n * ld * sizeof( rpk_complex64_t ) );

    for ( i = 0; i < nb_tests; i++ ) {
        /*
         * Compress and then uncompress
         */
        timer = clockGetLocal();
        ctx->rpk_ge2lr( ctx, minMN, m, n, A->fr, ld, &(A->lr) );
        timer = clockGetLocal() - timer;
        assert( timer >= 0. );
        total_timer += timer;

        /*
         * Let's check the result
         */
        rpk_zlr2ge( RapackNoTrans, m, n, &(A->lr), A2, ld );

        rpk_zgeadd( RapackNoTrans, m, n,
                    -1., A->fr, ld,
                     1., A2,    ld );

        norm_residual = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f', m, n,
                                             A2, ld, NULL );

        errbound = ctx->tolerance;
        if ( (ctx->use_reltol) && (A->norm > 0.) ) {
            errbound *= A->norm;
        }
        if ( A->lr.rk > 0 ) {
            errbound *= (double)(A->lr.rk);
        }
        resid = norm_residual / errbound;
        if ( resid > 10. ) {
            rc += 1;
        }

        rankA = A->lr.rk;
        rpk_zlrfree( &(A->lr) );
    }

    total_timer /= nb_tests;

    fprintf( stdout, "%7s %4d %e %e %e %e %7s\n",
             rpk_compmeth_shnames[compmeth], (int)rankA, total_timer, A->norm, norm_residual, resid,
             (rc != 0) ? "FAILED" : "SUCCESS" );

    free( A2 );

    return rc;
}

/**
 *******************************************************************************
 *
 * @brief Check the operation \f$ B = \alpha A + B \f$
 *
 * This routine checks the addition of two low-rank matrices together.
 *
 *  Let's have \f[ A = c(A) + r(A) \f$, such that \f$ || A - c(A) || < \tau_A \f],
 *             \f[ B = c(B) + r(B) \f$, such that \f$ || B - c(B) || < \tau_B \f]
 *
 * We want to test the correctness of \f$ c(C) = \alpha c(A) + c(B) \f$ with respect to
 * \f$ C = \alpha A + B \f$
 *
 * For that let's check:
 *    \f[ || C - c(C) || = || \alpha A + B - \alpha c(A) - c(B) || = || \alpha r(A) + r(B) || \f]
 * So, we consider that the test is correct if:
 *    \f[ || C - c(C) || < |\alpha| * ||r(A)|| + ||r(B)|| = \alpha * \tau_A + \tau_B \f]
 *
 * Note that if we consider absolute compression, \f$ \tau_A = \eps \f$,
 * otherwise \f$ \tau_A = \eps * ||A|| \f$, and respectively for B.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The data structure that defines the kernels used for the
 *          compression. It also defines, the tolerance of the low-rank
 *          representation and if absolute or relative tolerance is applied.
 *
 * @param[in] offx
 *          The row offset of the matrix A into the matrix B.
 *          offx >= 0 and A->m + offx <= B->m.
 *
 * @param[in] offy
 *          The column offset of the matrix A into the matrix B.
 *          offy >= 0 and A->n + offy <= B->n.
 *
 * @param[in] alpha
 *          The scalar alpha that multiplies A.
 *
 * @param[in] A
 *          The input matrix A to test.
 *          On entry, m, n, ld, rk, fr, and lr must be defined.
 *          A-lr must be the lowrank representation of A->lr.
 *
 * @param[in] B
 *          The input matrix B to test.
 *          On entry, m, n, ld, rk, fr, and lr must be defined.
 *          B-lr must be the lowrank representation of B->lr.
 *
 * @param[in,out] C
 *          The matrix to test.
 *          On entry, m, n, ld, rk, and fr must be defined. C->fr must be set
 *          as the result of \f$ \alpha A + B \f$ in the full-rank form.
 *          During the test, C->lr is modified to store the low-rank form of
 *          alpha c(A) + c(B), and is freed on exit.
 *
 *******************************************************************************
 *
 * @retval 0 on success
 * @retval <0, if one of the parameter is incorrect
 * @retval >0, if one or more of the tests failed.
 *
 *******************************************************************************/
int
testrpk_zcheck_rradd( rpk_ctx_t           *ctx,
                      rpk_compmeth_t       compmeth,
                      rpk_int_t            offx,
                      rpk_int_t            offy,
                      rpk_complex64_t      alpha,
                      const test_matrix_t *A,
                      const test_matrix_t *B,
                      test_matrix_t       *C )
{
    rpk_complex64_t *Clr;
    double              norm_diff, res;
    Clock               timer;
    rpk_int_t        rkCmax;
    rpk_int_t        rankmax = ctx->get_rklimit( ctx, B->m, B->n );
    int                 rc = 0;

    assert( C->m == B->m );
    assert( C->n == B->n );
    assert( (A->m + offx) <= B->m );
    assert( (A->n + offy) <= B->n );

    rkCmax = A->lr.rk + B->lr.rk;

    if ( ( A->lr.rk == -1 ) || ( B->lr.rk == -1 ) ) {
        printf( "Operation not supported\n" );
        return 0;
    }

    /* Init lrC */
    memset( &(C->lr), 0, sizeof( rpk_matrix_t ) );

    /* Backup B into C */
    rpk_zlrcpy( RapackNoTrans, 1.,
                B->m, B->n, &(B->lr),
                B->m, B->n, &(C->lr), 0, 0 );

    /* Perform C = B - A in LR format */
    timer = clockGetLocal();
    ctx->rpk_rradd( ctx, RapackNoTrans, RapackNoTrans, &alpha,
                    A->m, A->n, &(A->lr),
                    C->m, C->n, &(C->lr),
                    offx, offy );
    timer = clockGetLocal() - timer;

    /*
     * Check \f[ || (\alpha * A+B) - c( \alpha * c(A) + c(B) ) || < tol * (|\alpha| + 1)         \f] (Absolute)
     *    or \f[ || (\alpha * A+B) - c( \alpha * c(A) + c(B) ) || < tol * (|\alpha| ||A||+||B||) \f] (Relative)
     */
    Clr = malloc( C->m * C->n * sizeof( rpk_complex64_t ) );

    /* Uncompress the low-rank sum */
    rpk_zlr2ge( RapackNoTrans, C->m, C->n,
                &(C->lr), Clr, C->m );

    /* Compute the diff */
    rpk_zgeadd( RapackNoTrans, C->m, C->n,
                -1., C->fr, C->ld,
                 1., Clr,   C->m );

    norm_diff = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f', C->m, C->n,
                                     Clr, C->m, NULL );

    if ( ( A->lr.rk != 0 ) || ( B->lr.rk != 0 ) ) {
        double errbound = ctx->use_reltol ? ( cabs(alpha) * A->norm + B->norm ) : cabs(alpha) + 1.;
        res = norm_diff / ( ctx->tolerance * errbound );
    }
    else {
        res = norm_diff;
    }

    free( Clr );

    /* Check the correctness of the result */
    if ( res > 10.0 ) {
        rc += 1;
    }

    /* Check that final matrix is not full rank, we should have exited before */
    if ( ( C->lr.rk == -1 ) && ( rkCmax <= rankmax ) ) {
        rc += 2;
    }

    /* Check that final rank does not exceed the sum of the ranks */
    if ( C->lr.rk > rkCmax ) {
        rc += 4;
    }

    /* Check that final rank is larger than the minimal rank (rA, rB, abs(rA-rb)) */
    if ( ( C->lr.rk != -1 ) &&
         ( C->lr.rk < rpk_imin( rpk_imin( A->lr.rk, B->lr.rk ), abs( A->lr.rk - B->lr.rk ) ) ) )
    {
        rc += 8;
    }

    fprintf( stdout, "%7s %4d %e %e %e %e ",
             rpk_compmeth_shnames[compmeth],
             (int)C->lr.rk, clockVal(timer), C->norm, norm_diff, res );

    if ( rc == 0 ) {
        fprintf( stdout, "SUCCESS\n" );
    }
    else {
        fprintf( stdout, "FAILED(%d)\n", rc );
    }

    rpk_zlrfree( &(C->lr) );
    return ( rc > 0 ) ? 1 : 0;
}

/**
 *******************************************************************************
 *
 * @brief Check the operation \f$ C = \alpha A B + \beta C \f$
 *
 * This routine checks the addition of two low-rank matrices together.
 *
 *  Let's have \f[ A = c(A) + r(A) \f$, such that \f$ || A - c(A) || < \tau_A \f]
 *
 * We want to test the correctness of \f$ \alpha c(A) c(B) + \beta c(C) \f$ with respect to
 * \f$ \alpha A B + \beta C \f$
 *
 * For that let's check:
 *    \f[ || C - c(C) || = \alpha * [ c(A)r(B) + r(A)c(B) + rA)r(B) ] + \beta r(C) \f]
 * So, we consider that the test is correct if:
 *    \f[ || C - c(C) || < |\alpha| * ... + \beta \tau_C \f]
 *
 * Note that if we consider absolute compression, \f$ \tau_A = \eps \f$,
 * otherwise \f$ \tau_A = \eps * ||A|| \f$.
 *
 *******************************************************************************
 *
 * @param[in] lowrank
 *          The data structure that defines the kernels used for the
 *          compression. It also defines, the tolerance of the low-rank
 *          representation and if absolute or relative tolerance is applied.
 *
 * @param[in] offx
 *          The row offset of the matrix A into the matrix B.
 *          offx >= 0 and A->m + offx <= B->m.
 *
 * @param[in] offy
 *          The column offset of the matrix A into the matrix B.
 *          offy >= 0 and A->n + offy <= B->n.
 *
 * @param[in] alpha
 *          The scalar alpha that multiplies A B.
 *
 * @param[in] A
 *          The input matrix A to test.
 *          On entry, m, n, ld, rk, fr, and lr must be defined.
 *          A-lr must be the lowrank representation of A->lr.
 *
 * @param[in] B
 *          The input matrix B to test.
 *          On entry, m, n, ld, rk, fr, and lr must be defined.
 *          B-lr must be the lowrank representation of B->lr.
 *
 * @param[in] beta
 *          The scalar beta that multiplies C.
 *
 * @param[in] C
 *          The matrix to test.
 *          On entry, m, n, ld, rk, and fr must be defined. C->fr must be set
 *          as the result of \f$ \alpha A B + \beta C \f$ in the full-rank form.
 *          C->lr must store the low-rank form of the matrix C before any
 *          computation.
 *
 *******************************************************************************
 *
 * @retval 0 on success
 * @retval <0, if one of the parameter is incorrect
 * @retval >0, if one or more of the tests failed.
 *
 *******************************************************************************/
int
testrpk_zcheck_gemm( rpk_ctx_t           *ctx,
                     rpk_compmeth_t       compmeth,
                     rpk_int_t            offx,
                     rpk_int_t            offy,
                     rpk_complex64_t      alpha,
                     const test_matrix_t *A,
                     const test_matrix_t *B,
                     rpk_complex64_t      beta,
                     const test_matrix_t *C )
{
    rpk_matrix_t     lrC2;
    rpk_complex64_t *Clr;
    double           norm_diff, res;
    Clock            timer;
    int              rc = 0;

    /* Init lrC */
    memset( &lrC2, 0, sizeof( rpk_matrix_t ) );

    /* Backup C into C2 */
    rpk_zlrcpy( RapackNoTrans, 1., C->m, C->n, &(C->lr), C->m, C->n, &lrC2, 0, 0 );

    /* Compute the low-rank matrix-matrix */
    {
        rpk_zgemm_t zgemm_params;
        zgemm_params.transA = RapackNoTrans;
        zgemm_params.transB = RapackConjTrans;
        zgemm_params.M      = A->m;
        zgemm_params.N      = B->m;
        zgemm_params.K      = A->n;
        zgemm_params.Cm     = C->m;
        zgemm_params.Cn     = C->n;
        zgemm_params.offx   = offx;
        zgemm_params.offy   = offy;
        zgemm_params.alpha  = alpha;
        zgemm_params.A      = &(A->lr);
        zgemm_params.B      = &(B->lr);
        zgemm_params.beta   = beta;
        zgemm_params.C      = &lrC2;
        zgemm_params.work   = NULL;
        zgemm_params.lwork  = -1;
        zgemm_params.lwused = -1;
        zgemm_params.lock   = NULL;

        timer = clockGetLocal();
        rpkx_zgemm( ctx, &zgemm_params );
        timer = clockGetLocal() - timer;
    }

    /*
     * Check || (A+B) - (c(A)+c(B)) || < tol * || A+B ||
     * What we really want is || (A+B) - c(A+B) || < tol * || A+B ||
     */
    Clr = malloc( C->m * C->n * sizeof( rpk_complex64_t ) );

    /* Uncompress the low-rank sum */
    rpk_zlr2ge( RapackNoTrans, C->m, C->n,
                &lrC2, Clr, C->ld );

    /* Compute the diff */
    rpk_zgeadd( RapackNoTrans, C->m, C->n,
                -1., C->fr, C->ld,
                 1., Clr,   C->ld );

    norm_diff = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f', C->m, C->n,
                                     Clr, C->ld, NULL );

    if ( ( C->lr.rk != 0.0 ) && ( ( A->lr.rk + B->lr.rk ) != 0 ) ) {
        double errbound = ctx->use_reltol ? C->norm : 1.;
        res             = norm_diff / ( ctx->tolerance * errbound );
    }
    else {
        res = norm_diff;
    }

    fprintf( stdout, "%7s %4d %e %e %e %e ",
             rpk_compmeth_shnames[compmeth],
             (int)lrC2.rk, clockVal(timer), C->norm, norm_diff, res );

    free( Clr );
    rpk_zlrfree( &lrC2 );

    /* Check the correctness of the result */
    if ( res > 10.0 ) {
        rc += 1;
    }

    if ( rc == 0 ) {
        fprintf( stdout, "SUCCESS\n" );
    }
    else {
        fprintf( stdout, "FAILED(%d)\n", rc );
    }

    return rc;
}
