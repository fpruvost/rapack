/**
 *
 * @file rpk_zgemmt.c
 *
 * RAPACK general matrix multiplication with trapezoidal update.
 *
 * @copyright 2024-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Abel Calluaud
 * @date 2024-02-19
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

static rpk_complex64_t zone  = 1.0;
static rpk_complex64_t zzero = 0.0;

/**
 *******************************************************************************
 *
 * @brief Performs a partial matrix-matrix operation on a trinagular matrix C.
 *
 *    \f[ C = \alpha [op( A )\times op( B )] + \beta C, \f]
 *
 *  where op( X ) is one of:
 *    \f[ op( X ) = X,   \f]
 *    \f[ op( X ) = X^T, \f]
 *    \f[ op( X ) = X^H, \f]
 *
 *  alpha and beta are scalars, and A, B and C are matrices, with op( A )
 *  an m-by-k matrix, op( B ) a k-by-n matrix and C an m-by-n matrix.
 *
 * This kernel should be used when it is known that the matrix product A-by-B
 * produces an hermitian or symmetric matrix, and thus only half of the
 * computations are required. C is considered hermitian or symmetric and only
 * the uplo part of the matrix is accessed and updated.
 * This appears in low-rank factorizations whand applyin rank-k update on the
 * diagonal matrix.
 *
 *******************************************************************************
 *
 * @param[in] uplo
 *          - RapackUpper: The upper part of C is updated, lower part is not referenced.
 *          - RapackLower: The lower part of C is updated, upper part is not referenced.
 *
 * @param[in] transA
 *          - RapackNoTrans:   A is not transposed,
 *          - RapackTrans:     A is transposed,
 *          - RapackConjTrans: A is conjugate transposed.
 *
 * @param[in] transB
 *          - RapackNoTrans:   B is not transposed,
 *          - RapackTrans:     B is transposed,
 *          - RapackConjTrans: B is conjugate transposed.
 *
 * @param[in] M
 *          The number of rows of the matrix op( A ) and of the matrix C.
 *          m >= 0.
 *
 * @param[in] N
 *          The number of columns of the matrix op( B ) and of the matrix C.
 *          n >= 0.
 *
 * @param[in] K
 *          The number of columns of the matrix op( A ) and the number of rows
 *          of the matrix op( B ). k >= 0.
 *
 * @param[in] alpha
 *          The scalar alpha.
 *
 * @param[in] A
 *          An lda-by-ka matrix, where ka is k when transa = RapackNoTrans,
 *          and is m otherwise.
 *
 * @param[in] LDA
 *          The leading dimension of the array A.
 *          When transa = RapackNoTrans, lda >= max(1,m),
 *          otherwise, lda >= max(1,k).
 *
 * @param[in] B
 *          An ldb-by-kb matrix, where kb is n when transb = RapackNoTrans,
 *          and is k otherwise.
 *
 * @param[in] LDB
 *          The leading dimension of the array B.
 *          When transb = RapackNoTrans, ldb >= max(1,k),
 *          otherwise, ldb >= max(1,n).
 *
 * @param[in] beta
 *          The scalar beta.
 *
 * @param[in,out] C
 *          An ldc-by-n matrix. On exit, the upper (respectively lower) part of
 *          the m-by-n array is overwritten by the m-by-n upper (resp. lower)
 *          part of the matrix ( alpha*op( A )*op( B ) + beta*C ).
 *
 * @param[in] LDC
 *          The leading dimension of the array C. ldc >= max(1,m).
 *
 * @param[in,out] W
 *          If cblas_zgemmt kernel is availabale (HAVE_CBLAS_GEMMT
 *          defined), W is unused, otherwise, it has to be NULL for local
 *          allocation, or a workspace of size m-by-n to avoid allocation in the
 *          kernel.
 */
void
rpk_zgemmt( rpk_uplo_t             uplo,
            rpk_trans_t            transA,
            rpk_trans_t            transB,
            rpk_int_t              m,
            rpk_int_t              n,
            rpk_int_t              k,
            rpk_complex64_t        alpha,
            const rpk_complex64_t *A,
            rpk_int_t              lda,
            const rpk_complex64_t *B,
            rpk_int_t              ldb,
            rpk_complex64_t        beta,
            rpk_complex64_t       *C,
            rpk_int_t              ldc,
            rpk_complex64_t       *W )
{
#if defined(HAVE_CBLAS_GEMMT)
    cblas_zgemmt( CblasColMajor, (CBLAS_UPLO)uplo,
                  (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                  m, n, k,
                  CBLAS_SADDR(alpha), A, lda,
                                      B, ldb,
                  CBLAS_SADDR(beta),  C, ldc );
#else
    int allocated = 0;

    if ( W == NULL ) {
        W = (rpk_complex64_t *)malloc( m * n * sizeof( rpk_complex64_t ) );
        allocated = 1;
    }

    cblas_zgemm( CblasColMajor, (CBLAS_TRANSPOSE)transA, (CBLAS_TRANSPOSE)transB,
                 m, n, k,
                 CBLAS_SADDR(alpha), A, lda,
                                     B, ldb,
                 CBLAS_SADDR(zzero), W, m );

    rpk_ztradd( uplo, RapackNoTrans, m, n, zone, W, m, beta, C, ldc );

    if ( allocated ) {
        free( W );
    }
#endif

    return;
}
