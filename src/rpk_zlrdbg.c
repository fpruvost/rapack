/**
 *
 * @file rpk_zlrdbg.c
 *
 * RAPACK low-rank kernel debug routines that may be call within gdb.
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @date 2023-12-14
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"
#include "rpk_znancheck.h"

/**
 *******************************************************************************
 *
 * @brief Print the svd values of the given matrix.
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix A.
 *
 * @param[in] N
 *          The number of columns of the matrix A.
 *
 * @param[in] A
 *          The matrix A to study of size lda-by-N
 *
 * @param[in] lda
 *          The leading dimension of the matrix A. lda = max( 1, M )
 *
 *******************************************************************************/
void
rpk_zlrdbg_printsvd( rpk_int_t M, rpk_int_t N, const rpk_complex64_t *A, rpk_int_t lda )
{
    rpk_int_t        i, ret;
    rpk_int_t        minMN  = rpk_imin( M, N );
    size_t           lrwork = 2 * minMN;
    size_t           lzwork = M * N;
    rpk_complex64_t *W;
    double          *s, *superb;

    W      = malloc( lzwork * sizeof( rpk_complex64_t ) + lrwork * sizeof( double ) );
    s      = (double *)( W + M * N );
    superb = s + minMN;

    ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', M, N, A, lda, W, M );
    assert( ret == 0 );
    ret = LAPACKE_zgesvd( LAPACK_COL_MAJOR, 'N', 'N', M, N, W, M, s, NULL, 1, NULL, 1, superb );
    assert( ret == 0 );

    for ( i = 0; i < minMN; i++ ) {
        fprintf( stderr, "%e ", s[i] );
    }
    fprintf( stderr, "\n" );

    (void)ret;
    free( W );
}

/**
 *******************************************************************************
 *
 * @brief Check the orthogonality of the matrix A
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix A.
 *
 * @param[in] N
 *          The number of columns of the matrix A.
 *
 * @param[in] A
 *          The matrix A to study of size lda-by-N
 *
 * @param[in] lda
 *          The leading dimension of the matrix A. lda = max( 1, M )
 *
 *******************************************************************************
 *
 * @retval 0 if the matrix A is orthogonal
 * @retval 1 if the matrix A is not orthogonal
 *
 *******************************************************************************/
int
rpk_zlrdbg_check_orthogonality( rpk_int_t M, rpk_int_t N, const rpk_complex64_t *A, rpk_int_t lda )
{
    rpk_complex64_t *Id;
    double           alpha, beta;
    double           normQ, res;
    rpk_int_t        info_ortho, ret;
    rpk_int_t        minMN = rpk_imin( M, N );
    rpk_int_t        maxMN = rpk_imax( M, N );
    double           eps   = LAPACKE_dlamch_work( 'e' );

    alpha = 1.0;
    beta  = -1.0;

    /* Build the identity matrix */
    Id  = malloc( minMN * minMN * sizeof( rpk_complex64_t ) );
    ret = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'A', minMN, minMN,
                              0., 1., Id, minMN );
    assert( ret == 0 );

    if ( M > N ) {
        /* Perform Id - Q'Q */
        cblas_zherk( CblasColMajor, CblasUpper, CblasConjTrans, N, M,
                     alpha, A,  lda,
                     beta,  Id, minMN );
    }
    else {
        /* Perform Id - QQ' */
        cblas_zherk( CblasColMajor, CblasUpper, CblasNoTrans, M, N,
                     alpha, A,  lda,
                     beta,  Id, minMN );
    }

    normQ = LAPACKE_zlanhe_work( LAPACK_COL_MAJOR, 'M', 'U', minMN, Id, minMN, NULL );
    res   = normQ / ( maxMN * eps );

    if ( isnan( res ) || isinf( res ) || ( res > 60.0 ) ) {
        fprintf( stderr,
                 "Check Orthogonality: || I - Q*Q' || = %e, ||Id-Q'*Q||_oo / (N*eps) = %e : \n",
                 normQ, res );
        info_ortho = 1;
    }
    else {
        info_ortho = 0;
    }

    free( Id );
    (void)ret;
    return info_ortho;
}

/**
 *******************************************************************************
 *
 * @brief Check the orthogonality of the matrix A relatively to the matrix B
 *
 * Check that A^t B = 0
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix A.
 *
 * @param[in] NA
 *          The number of columns of the matrix A.
 *
 * @param[in] NB
 *          The number of columns of the matrix B.
 *
 * @param[in] A
 *          The matrix A to study of size lda-by-NA
 *
 * @param[in] lda
 *          The leading dimension of the matrix A. lda = max( 1, M )
 *
 * @param[in] B
 *          The matrix B to study of size ldb-by-NB
 *
 * @param[in] ldb
 *          The leading dimension of the matrix B. ldb = max( 1, M )
 *
 *******************************************************************************
 *
 * @retval 0 if the matrices A and B are orthogonal
 * @retval 1 if the matrices A anb B are not orthogonal
 *
 *******************************************************************************/
int
rpk_zlrdbg_check_orthogonality_AB( rpk_int_t              M,
                                   rpk_int_t              NA,
                                   rpk_int_t              NB,
                                   const rpk_complex64_t *A,
                                   rpk_int_t              lda,
                                   const rpk_complex64_t *B,
                                   rpk_int_t              ldb )
{
    rpk_complex64_t *Zero;
    double           norm, res;
    rpk_int_t        info_ortho, ret;
    double           eps   = LAPACKE_dlamch_work( 'e' );
    rpk_complex64_t  zone  = 1.0;
    rpk_complex64_t  zzero = 0.0;

    /* Build the null matrix */
    Zero = malloc( NA * NB * sizeof( rpk_complex64_t ) );
    ret  = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'A', NA, NB, 0., 0., Zero, NA );
    assert( ret == 0 );

    cblas_zgemm(CblasColMajor, CblasConjTrans, CblasNoTrans,
                 NA, NB, M,
                 CBLAS_SADDR(zone),  A,    lda,
                                     B,    ldb,
                 CBLAS_SADDR(zzero), Zero, NA );

    norm = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'M', NA, NB, Zero, NA, NULL );
    res  = norm / ( M * eps );

    if ( isnan( res ) || isinf( res ) || ( res > 60.0 ) ) {
        fprintf( stderr,
                 "Check Orthogonality: || A' B || = %e, || A' B ||_oo / (M*eps) = %e : \n",
                 norm,
                 res );
        info_ortho = 1;
    }
    else {
        info_ortho = 0;
    }

    free( Zero );
    (void)ret;
    return info_ortho;
}
