/**
 *
 * @file rpk_zlrgetsize.c
 *
 * RAPACK low-rank kernel routines
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Nolan Bredel
 * @date 2023-12-14
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 *******************************************************************************
 *
 * @brief Compute the size of a block to send in LR
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix A.
 *
 * @param[in] N
 *          The number of columns of the matrix A.
 *
 * @param[in] A
 *          The low-rank representation of the matrix A.
 *
 *******************************************************************************
 *
 * @return Size of a block to send in LR
 *
 *******************************************************************************/
size_t
rpk_zlrgetsize( rpk_int_t M, rpk_int_t N, rpk_matrix_t *A )
{
    if ( A->rk != -1 ) {
        return A->rk * ( M + N );
    }
    else {
        return M * N;
    }
}
