/**
 *
 * @file rpk_ztrsm.c
 *
 * RAPACK low-rank kernel routines to compute triangular solve with a RAPACK
 * right-hand-side.
 *
 * @copyright 2024-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Abel Calluaud
 * @author Mathieu Faverge
 * @date 2024-03-11
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 *******************************************************************************
 *
 *  @brief Solves a linear system of equation with the RHS being a low-rank matrix.
 *
 *    \f[ op( A )\times X  = \alpha B, \f] or
 *    \f[ X \times op( A ) = \alpha B, \f]
 *
 *  where op( A ) is one of:
 *    \f[ op( A ) = A,   \f]
 *    \f[ op( A ) = A^T, \f]
 *    \f[ op( A ) = A^H, \f]
 *
 *  alpha is a scalar, X and B are m-by-n low-rank matrices, and
 *  A is a unit or non-unit, upper or lower triangular matrix.
 *  The matrix X overwrites B.
 *
 *******************************************************************************
 *
 * @param[in] side
 *          - RapackLeft:  op(A)*X = B,
 *          - RapackRight: X*op(A) = B.
 *
 * @param[in] uplo
 *          - RapackUpper: A is upper triangular,
 *          - RapackLower: A is lower triangular.
 *
 * @param[in] trans
 *          - RapackNoTrans:   A is not transposed,
 *          - RapackTrans:     A is transposed,
 *          - RapackConjTrans: A is conjugate transposed.
 *
 * @param[in] diag
 *          - RapackNonUnit: A has non-unit diagonal,
 *          - RapackUnit:    A has unit diagonal.
 *
 * @param[in] M
 *          The number of rows of the matrix B. m >= 0.
 *
 * @param[in] N
 *          The number of columns of the matrix B. n >= 0.
 *
 * @param[in] alpha
 *          The scalar alpha.
 *
 * @param[in] A
 *          The lda-by-ka triangular matrix,
 *          where ka = m if side = RapackLeft,
 *            and ka = n if side = RapackRight.
 *          If uplo = RapackUpper, the leading k-by-k upper triangular part
 *          of the array A contains the upper triangular matrix, and the
 *          strictly lower triangular part of A is not referenced.
 *          If uplo = RapackLower, the leading k-by-k lower triangular part
 *          of the array A contains the lower triangular matrix, and the
 *          strictly upper triangular part of A is not referenced.
 *          If diag = RapackUnit, the diagonal elements of A are also not
 *          referenced and are assumed to be 1.
 *
 * @param[in] lda
 *          The leading dimension of the array A. lda >= max(1,k).
 *
 * @param[in,out] B
 *          On entry, the low-rank m-by-n right hand side matrix B.
 *          On exit, if return value = 0, the low-rank m-by-n solution matrix X.
 *
 * @return  -i, if the i^th parameter is incorrect.
 *          The number of flops required to solve the system on success.
 *
 */
rpk_fixdbl_t
rpkx_ztrsm( const rpk_ctx_t       *ctx,
            rpk_side_t             side,
            rpk_uplo_t             uplo,
            rpk_trans_t            trans,
            rpk_diag_t             diag,
            rpk_int_t              m,
            rpk_int_t              n,
            rpk_complex64_t        alpha,
            const rpk_complex64_t *A,
            rpk_int_t              lda,
            rpk_matrix_t          *B )
{
    (void)ctx;

    if ( ( side != RapackRight ) && ( side != RapackLeft ) ) {
        fprintf( stderr, "rpkx_ztrsm: illegal value of side\n" );
        return -1.;
    }
    if ( ( uplo != RapackUpper ) && ( uplo != RapackLower ) ) {
        fprintf( stderr, "rpkx_ztrsm: illegal value of uplo\n" );
        return -2.;
    }
    if ( !rpk_is_valid_trans( trans ) ) {
        fprintf( stderr, "rpkx_ztrsm: illegal value of trans\n" );
        return -3.;
    }
    if ( ( diag != RapackUnit ) && ( diag != RapackNonUnit ) ) {
        fprintf( stderr, "rpkx_ztrsm: illegal value of diag\n" );
        return -4.;
    }
    if ( m < 0 ) {
        fprintf( stderr, "rpkx_ztrsm: illegal value of m\n" );
        return -5.;
    }
    if ( n < 0 ) {
        fprintf( stderr, "rpkx_ztrsm: illegal value of n\n" );
        return -6.;
    }

    if ( B->rk == 0 ) {
        /*
         * The low-rank matrix is zero, nothing to do
         */
        return 0.;
    }

    if ( B->rk == -1 ) {
        /*
         * This is a dense triangular solve, call BLAS
         */
        cblas_ztrsm( CblasColMajor, (CBLAS_SIDE)side, (CBLAS_UPLO)uplo,
                     (CBLAS_TRANSPOSE)trans, (CBLAS_DIAG)diag, m, n,
                     CBLAS_SADDR(alpha), A,    lda,
                                         B->u, m );
        return flops_ztrsm( side, m, n );
    }

    switch ( side ) {
        case RapackLeft:
            /*
             * The objective is to solve op(A) * X = alpha * Bu * Bv^h
             * If we let X = Y * Bv^h, we can reduce the problem to solve:
             *        op(A) * Y * Bv^h = alpha * Bu * Bv^h,
             * so :   op(A) * Y        = alpha * Bu
             */
            cblas_ztrsm( CblasColMajor, (CBLAS_SIDE)side, (CBLAS_UPLO)uplo,
                         (CBLAS_TRANSPOSE)trans, (CBLAS_DIAG)diag, m, B->rk,
                         CBLAS_SADDR(alpha), A,    lda,
                                             B->u, m );
            return flops_ztrsm( side, m, B->rk );

        case RapackRight:
            /*
             * The objective is to solve X * op(A) = alpha * Bu * Bv^h
             * if we let X = Bu * Y, we can reduce the problem to solve:
             *        Bu * Y * op(A) = alpha * Bu * Bv^h
             * so :        Y * op(A) = alpha *      Bv^h
             */
            cblas_ztrsm( CblasColMajor, (CBLAS_SIDE)side, (CBLAS_UPLO)uplo,
                         (CBLAS_TRANSPOSE)trans, (CBLAS_DIAG)diag, B->rk, n,
                         CBLAS_SADDR(alpha), A,    lda,
                                             B->v, B->rkmax );
            return flops_ztrsm( side, B->rk, n );
    }

    return (rpk_fixdbl_t)-100.;
}
