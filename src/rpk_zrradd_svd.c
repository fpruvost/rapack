/**
 *
 * @file rpk_zrradd_svd.c
 *
 * RAPACK low-rank kernel routines using SVD based on Lapack ZGESVD.
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Nolan Bredel
 * @author Abel Calluaud
 * @date 2024-02-08
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"
#include "rpk_znancheck.h"
#include "frobeniusupdate.h"

#ifndef DOXYGEN_SHOULD_SKIP_THIS
static rpk_complex64_t zone  = 1.0;
static rpk_complex64_t zzero = 0.0;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

/**
 *******************************************************************************
 *
 * @brief Add two LR structures A=(-u1) v1^T and B=u2 v2^T into u2 v2^T
 *
 *    u2v2^T - u1v1^T = (u2 u1) (v2 v1)^T
 *    Compute QR decomposition of (u2 u1) = Q1 R1
 *    Compute QR decomposition of (v2 v1) = Q2 R2
 *    Compute SVD of R1 R2^T = u sigma v^T
 *    Final solution is (Q1 u sigma^[1/2]) (Q2 v sigma^[1/2])^T
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The structure with low-rank parameters.
 *
 * @param[in] infomask
 *
 * @param[in] alphaptr
 *          alpha * A is add to B
 *
 * @param[in] M1
 *          The number of rows of the matrix A.
 *
 * @param[in] N1
 *          The number of columns of the matrix A.
 *
 * @param[in] A
 *          The low-rank representation of the matrix A.
 *
 * @param[in] M2
 *          The number of rows of the matrix B.
 *
 * @param[in] N2
 *          The number of columns of the matrix B.
 *
 * @param[in] B
 *          The low-rank representation of the matrix B.
 *
 * @param[in] offx
 *          The horizontal offset of A with respect to B.
 *
 * @param[in] offy
 *          The vertical offset of A with respect to B.
 *
 *******************************************************************************
 *
 * @return  The new rank of u2 v2^T or -1 if ranks are too large for recompression
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zrradd_svd( const rpk_ctx_t    *ctx,
                 rpk_trans_t         transAu,
                 rpk_trans_t         transAv,
                 const void         *alphaptr,
                 rpk_int_t           M1,
                 rpk_int_t           N1,
                 const rpk_matrix_t *A,
                 rpk_int_t           M2,
                 rpk_int_t           N2,
                 rpk_matrix_t       *B,
                 rpk_int_t           offx,
                 rpk_int_t           offy )
{
    rpk_int_t        rank, M, N, minU, minV;
    rpk_int_t        i, ret, lwork, new_rank;
    rpk_int_t        ldau, ldav, ldbu, ldbv;
    rpk_complex64_t *u1u2, *v1v2, *R, *u, *v;
    rpk_complex64_t *tmp, *zbuf, *tauU, *tauV;
    rpk_complex64_t  querysize;
    rpk_complex64_t  alpha = *( (rpk_complex64_t *)alphaptr );
    double          *s;
    size_t           wzsize, wdsize;
    rpk_fixdbl_t     tol = ctx->tolerance;
    rpk_fixdbl_t     flops, total_flops = 0.0;

    assert( transAu == RapackNoTrans );
    rank = ( A->rk == -1 ) ? rpk_imin( M1, N1 ) : A->rk;
    rank += B->rk;
    M    = rpk_imax( M2, M1 );
    N    = rpk_imax( N2, N1 );
    minU = rpk_imin( M, rank );
    minV = rpk_imin( N, rank );

    assert( B->rk != -1 );

    assert( A->rk <= A->rkmax );
    assert( B->rk <= B->rkmax );

    if ( ( ( M1 + offx ) > M2 ) ||
         ( ( N1 + offy ) > N2 ) )
    {
        rapack_print_error( "Dimensions are not correct" );
        assert( 0 /* Incorrect dimensions */ );
        return total_flops;
    }

    /*
     * A is rank null, nothing to do
     */
    if ( A->rk == 0 ) {
        return total_flops;
    }

    ldau = ( transAu == RapackNoTrans ) ? M1 : A->rkmax;
    ldav = ( transAv == RapackNoTrans ) ? A->rkmax : N1;
    ldbu = M;
    ldbv = B->rkmax;

    /*
     * Let's handle case where B is a null matrix
     *   B = alpha A
     */
    if ( B->rk == 0 ) {
        rpkx_zlrcpy( ctx, transAv, alpha,
                     M1, N1, A, M2, N2, B,
                     offx, offy );
        return total_flops;
    }

    /*
     * The rank is too big, let's try to compress
     */
    if ( rank > rpk_imin( M, N ) ) {
        assert( 0 );
    }

    /*
     * Let's compute the size of the workspace
     */
    /* u1u2 and v1v2 */
    wzsize = ( M + N ) * rank;
    /* tauU and tauV */
    wzsize += minU + minV;

    /* Storage of R, u and v */
    wzsize += 3 * rank * rank;

    /* QR/LQ workspace */
    lwork = rpk_imax( M, N ) * 32;

    /* Workspace needed for the gesvd */
#if defined(RAPACK_DEBUG_LR_NANCHECK)
    querysize = rank;
#else
    {
        /*
         * rworkfix is a fix to an internal mkl bug see:
         * https://community.intel.com/t5/Intel-oneAPI-Math-Kernel-Library/MKL-2021-4-CoreDump-with-LAPACKE-cgesvd-work-or-LAPACKE-zgesvd/m-p/1341228
         */
        double rwork;
        ret = MYLAPACKE_zgesvd_work( LAPACK_COL_MAJOR, 'S', 'S',
                                     rank, rank, NULL, rank,
                                     NULL, NULL, rank, NULL, rank,
                                     &querysize, -1, &rwork );
        (void)rwork;
    }
#endif
    lwork = rpk_imax( lwork, querysize );
    wzsize += lwork;

    wdsize = rank;
#if defined(PRECISION_z) || defined(PRECISION_c)
    wdsize += 5 * rank;
#endif

    zbuf = malloc( wzsize * sizeof(rpk_complex64_t) + wdsize * sizeof(double) );
    s    = (double *)( zbuf + wzsize );

    u1u2 = zbuf + lwork;
    tauU = u1u2 + M * rank;
    v1v2 = tauU + minU;
    tauV = v1v2 + N * rank;
    R    = tauV + minV;

    /*
     * Concatenate U2 and U1 in u1u2
     *  [ u2  0  ]
     *  [ u2  u1 ]
     *  [ u2  0  ]
     */
    rpk_zlrconcatenate_u( alpha,
                          M1, N1, A,
                          M2,     B,
                          offx, u1u2 );

    /*
     * Perform QR factorization on u1u2 = (Q1 R1)
     */
    ret = LAPACKE_zgeqrf_work( LAPACK_COL_MAJOR, M, rank,
                               u1u2, M, tauU, zbuf, lwork );
    assert( ret == 0 );
    total_flops += flops_zgeqrf( M, rank );

    /*
     * Concatenate V2 and V1 in v1v2
     *  [ v2^h v2^h v2^h ]
     *  [ 0    v1^h 0    ]
     */
    rpk_zlrconcatenate_v( transAv, alpha,
                          M1, N1, A,
                              N2, B,
                          offy, v1v2 );

    /*
     * Perform LQ factorization on v1v2 = (L2 Q2)
     */
    ret = LAPACKE_zgelqf_work( LAPACK_COL_MAJOR, rank, N,
                               v1v2, rank, tauV, zbuf, lwork );
    assert( ret == 0 );
    total_flops += flops_zgelqf( rank, N );

    /*
     * Compute R = alpha R1 L2
     */
    u = R + rank * rank;
    v = u + rank * rank;

    memset( R, 0, rank * rank * sizeof(rpk_complex64_t) );

    ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'U', rank, rank,
                               u1u2, M, R, rank );
    assert( ret == 0 );

    cblas_ztrmm(CblasColMajor,
                 CblasRight, CblasLower,
                 CblasNoTrans, CblasNonUnit,
                 rank, rank, CBLAS_SADDR(zone),
                 v1v2, rank, R, rank);
    total_flops += flops_ztrmm( RapackRight, rank, rank );

    if ( ctx->use_reltol ) {
        /**
         * In relative tolerance, we can choose two solutions:
         *  1) The first one, more conservative, is to compress relatively to
         *  the norm of the final matrix \f$ \alpha A + B \f$. In this kernel, we
         *  exploit the fact that the previous computed product contains all the
         *  information of the final matrix to do it as follow:
         *
         * double norm = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f', rank, rank,
         *                                    R, rank, NULL );
         * tol = tol * norm;
         *
         *  2) The second solution, less conservative, will allow to reduce the
         *  rank more efficiently. Since A and B have been compressed relatively
         *  to their respective norms, there is no reason to compress the sum
         *  relatively to its own norm, but it is more reasonable to compress it
         *  relatively to the norm of A and B. For example, A-B would be full
         *  with the first criterion, and rank null with the second.
         *  Note that here, we can only have an estimation that once again
         *  reduces the conservation of the criterion.
         *  \f[ || \alpha A + B || <= |\alpha| ||A|| + ||B|| <= |\alpha| ||U_aV_a|| + ||U_bV_b|| \f]
         *
         */
        double normA, normB;
        normA = rpk_zlrnrm( RapackFrobeniusNorm, transAv,       M1, N1, A );
        normB = rpk_zlrnrm( RapackFrobeniusNorm, RapackNoTrans, M2, N2, B );
        tol   = tol * ( cabs( alpha ) * normA + normB );
    }

    /*
     * Compute svd(R) = u sigma v^t
     */
    /* Missing the flops of the u and v generation */
    flops = flops_zgeqrf( rank, rank ) + flops_zgelqf( rank, ( rank - 1 ) );

    ret = MYLAPACKE_zgesvd_work( LAPACK_COL_MAJOR, 'S', 'S',
                                 rank, rank, R, rank,
                                 s, u, rank, v, rank,
                                 zbuf, lwork, s + rank );
    if ( ret != 0 ) {
        rapack_print_error( "LAPACKE_zgesvd FAILED" );
    }

    /*
     * Let's compute the new rank of the result
     */
    tmp = v;

    for ( i = 0; i < rank; i++, tmp += 1 ) {
        double frob_norm;

        /*
         * There are two different stopping criteria for SVD to decide the
         * compression rank:
         *	1) The 2-norm:
         *         Compare the singular values to the threshold
         *      2) The Frobenius norm:
         *         Compare the Frobenius norm of the trailing singular values to
         *         the threshold. Note that we use a reverse accumulation of the
         *         singular values to avoid accuracy issues.
         */
#if defined(RAPACK_SVD_2NORM)
        frob_norm = s[i];
#else
        frob_norm = rpk_dlassq( rank - i, s + rank - 1, -1 );
#endif

        if ( frob_norm < tol ) {
            break;
        }
        cblas_zdscal( rank, s[i], tmp, rank );
    }
    new_rank = i;
    total_flops += flops;

    /*
     * First case: The rank is too big, so we decide to uncompress the result
     */
    if ( new_rank > ctx->get_rklimit( ctx, M, N ) ) {
        rpk_matrix_t Bbackup = *B;

        rpk_zlralloc( M, N, -1, B );
        u = B->u;

        /* Uncompress B */
        flops = flops_zgemm( M, N, Bbackup.rk );
        cblas_zgemm( CblasColMajor, CblasNoTrans, CblasNoTrans,
                     M, N, Bbackup.rk,
                     CBLAS_SADDR(zone),  Bbackup.u, ldbu,
                                         Bbackup.v, ldbv,
                     CBLAS_SADDR(zzero), u,         M );
        total_flops += flops;

        /* Add A into it */
        if ( A->rk == -1 ) {
            flops = 2 * M1 * N1;
            rpk_zgeadd( transAv, M1, N1,
                        alpha, A->u, A->rkmax,
                        zone, u + offy * M + offx, M );
        }
        else {
            flops = flops_zgemm( M1, N1, A->rk );
            cblas_zgemm( CblasColMajor, CblasNoTrans, (CBLAS_TRANSPOSE)transAv,
                         M1, N1, A->rk,
                         CBLAS_SADDR(alpha), A->u,                ldau,
                                             A->v,                ldav,
                         CBLAS_SADDR(zone),  u + offy * M + offx, M);
        }
        total_flops += flops;
        rpk_zlrfree( &Bbackup );
        free( zbuf );
        zbuf = NULL;
        return total_flops;
    }
    else if ( new_rank == 0 ) {
        rpk_zlrfree( B );
        free( zbuf );
        zbuf = NULL;
        return total_flops;
    }

    /*
     * We need to reallocate the buffer to store the new compressed version of B
     * because it wasn't big enough
     */
    ret = rpkx_zlrsze( ctx, 0, M, N, B, new_rank, -1, -1 );
    assert( ret != -1 );
    assert( B->rkmax >= new_rank );
    assert( B->rkmax >= B->rk );

    ldbv = B->rkmax;

    /*
     * Let's now compute the final U = Q1 ([u] sigma)
     *                                     [0]
     */
#if defined(RAPACK_DEBUG_LR_NANCHECK)
    ret = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'A', M, new_rank,
                               0.0, 0.0, B->u, ldbu );
    assert( ret == 0 );
    ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', rank, new_rank,
                               u, rank, B->u, ldbu );
    assert( ret == 0 );
#else
    tmp = B->u;
    for ( i = 0; i < new_rank; i++, tmp += ldbu, u += rank ) {
        memcpy( tmp, u,              rank * sizeof(rpk_complex64_t) );
        memset( tmp + rank, 0, (M - rank) * sizeof(rpk_complex64_t) );
    }
#endif

    flops = flops_zunmqr( M, new_rank, minU, RapackLeft ) +
            flops_zunmlq( new_rank, N, minV, RapackRight );
    ret = LAPACKE_zunmqr_work( LAPACK_COL_MAJOR, 'L', 'N',
                               M, new_rank, minU,
                               u1u2, M, tauU,
                               B->u, ldbu,
                               zbuf, lwork);
    assert( ret == 0 );

    /*
     * And the final V^T = [v^t 0 ] Q2
     */
    tmp = B->v;
    ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', new_rank, rank,
                               v, rank, B->v, ldbv );
    assert( ret == 0 );

    ret = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'A', new_rank, N-rank,
                               0.0, 0.0, tmp + ldbv * rank, ldbv );
    assert( ret == 0 );

    ret = LAPACKE_zunmlq_work( LAPACK_COL_MAJOR, 'R', 'N',
                               new_rank, N, minV,
                               v1v2, rank, tauV,
                               B->v, ldbv,
                               zbuf, lwork );
    assert( ret == 0 );
    total_flops += flops;

    (void)ret;
    free( zbuf );
    zbuf = NULL;
    return total_flops;
}
