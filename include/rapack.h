/**
 * @file rapack.h
 *
 * RAPACK main header.
 *
 * @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Esragul Korkmaz
 * @author Gregoire Pichon
 * @author Abel Calluaud
 * @date 2024-02-08
 *
 */
#ifndef _rapack_h_
#define _rapack_h_

#include <stdlib.h>
#include <stdio.h>
#include "rapack/config.h"
#include "rapack/const.h"
#include "rapack/datatypes.h"

#include "rapack/rpk_zapi.h"
#include "rapack/rpk_capi.h"
#include "rapack/rpk_dapi.h"
#include "rapack/rpk_sapi.h"

#include "rapack/rpkx_zapi.h"
#include "rapack/rpkx_capi.h"
#include "rapack/rpkx_dapi.h"
#include "rapack/rpkx_sapi.h"

BEGIN_C_DECLS

/**
 *
 * @addtogroup rapack
 * @{
 *    This module contains all the internal functions for low-rank kernels
 */

typedef rpk_int_t (*fct_rklimit_t)( const rpk_ctx_t *, rpk_int_t, rpk_int_t );

/**
 * @brief Type of the functions to compress a dense block into a low-rank form.
 */
typedef rpk_fixdbl_t (*fct_ge2lr_t)( const rpk_ctx_t *, rpk_int_t, rpk_int_t, rpk_int_t,
                                     const void *, rpk_int_t, rpk_matrix_t * );

/**
 * @brief Type of the functions to add two low-rank blocks together.
 */
typedef rpk_fixdbl_t (*fct_rradd_t)( const rpk_ctx_t *, rpk_trans_t, rpk_trans_t,
                                     const void *, rpk_int_t, rpk_int_t,
                                     const rpk_matrix_t *, rpk_int_t, rpk_int_t,
                                     rpk_matrix_t *, rpk_int_t, rpk_int_t );

/**
 * @brief Type of the functions to lock/unlock a matrix.
 */
typedef void ( *fct_lock_t )( const rpk_ctx_t *, void * );
typedef void ( *fct_unlock_t )( const rpk_ctx_t *, void * );

/**
 * @brief Rapack context structure to define the type of function to use for the low-rank
 *        kernels and their parameters.
 */
typedef struct rpk_ctx_s {
    double         minratio;    /**< Define the minimal ratio for which we accept to
                                     compress a matrix into a low-rank form or  not.      */
    rpk_orthmeth_t orthmeth;    /**< Define the orthogonalization method.                 */
    int            use_reltol;  /**< Enable/disable relative tolerance vs absolute one    */
    double         tolerance;   /**< Absolute compression tolerance                       */
    fct_rklimit_t  get_rklimit; /**< Return the maximum rank for compression              */
    fct_rradd_t    rpk_rradd;   /**< Recompression function                               */
    fct_ge2lr_t    rpk_ge2lr;   /**< Compression function                                 */
    fct_lock_t     lock;        /**< Lock function                                        */
    fct_unlock_t   unlock;      /**< Unlock function                                      */
} rpk_ctx_t;

/**
 * @brief Default context for the RAPACK library
 */
extern const rpk_ctx_t rpk_ctx_default;

/**
 * @brief Global context for the RAPACK library
 */
extern const rpk_ctx_t *rpk_gctx;

/**
 *******************************************************************************
 *
 * @brief Compute the maximal rank accepted for a given matrix size for testings
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix
 *
 * @param[in] N
 *          The number of columns of the matrix
 *
 *******************************************************************************
 *
 * @return The maximal rank accepted for this matrix size.
 *
 *******************************************************************************/
static inline rpk_int_t
rpkx_get_rklimit_max( const rpk_ctx_t *rpkctx __attribute__((unused)),
                      rpk_int_t M, rpk_int_t N )
{
    return rpk_imin( M, N );
}

/**
 *******************************************************************************
 *
 * @brief Compute the maximal rank accepted for a given matrix size for Just-In-Time strategy
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix
 *
 * @param[in] N
 *          The number of columns of the matrix
 *
 *******************************************************************************
 *
 * @return The maximal rank accepted for this matrix size.
 *
 *******************************************************************************/
static inline rpk_int_t
rpkx_get_rklimit_end( const rpk_ctx_t *rpkctx, rpk_int_t M, rpk_int_t N ) {
    return ( rpkctx->minratio * rpk_imin( M, N ) ) / 4;
}

/**
 *******************************************************************************
 *
 * @brief Compute the maximal rank accepted for a given matrix size for Minimal-Memory strategy
 *
 *******************************************************************************
 *
 * @param[in] M
 *          The number of rows of the matrix
 *
 * @param[in] N
 *          The number of columns of the matrix
 *
 *******************************************************************************
 *
 * @return The maximal rank accepted for this matrix size.
 *
 *******************************************************************************/
static inline rpk_int_t
rpkx_get_rklimit_begin( const rpk_ctx_t *rpkctx, rpk_int_t M, rpk_int_t N ) {
    return ( rpkctx->minratio * M * N ) / ( M + N );
}

/**
 * @brief The RAPACK structure to hold a matrix in low-rank form
 */
typedef struct rpk_matrix_s {
    int   rk;    /**< Rank of the low-rank matrix: -1 is dense, otherwise rank-rk matrix           */
    int   rkmax; /**< Leading dimension of the matrix u                                            */
    void *u;     /**< Contains the dense matrix if rk=-1, or the u factor from u vT representation */
    void *v;     /**< Not referenced if rk=-1, otherwise, the v factor                             */
} rpk_matrix_t;

/**
 * @brief List of short names for the compression kernels
 */
extern const char *rpk_compmeth_shnames[RapackCompressMethodNbr];

/**
 * @brief List of long names for the compression kernels
 */
extern const char *rpk_compmeth_lgnames[RapackCompressMethodNbr];

/**
 * @brief Array of pointers to the multiple arithmetic and algorithmic variants of ge2lr
 */
extern const fct_ge2lr_t rpk_ge2lr_functions[RapackCompressMethodNbr][4];

/**
 * @brief Array of pointers to the multiple arithmetic and algorithmic variants of rradd
 */
extern const fct_rradd_t rpk_rradd_functions[RapackCompressMethodNbr][4];

/**
 * @}
 */
END_C_DECLS

#endif /* _rapack_h_ */
