/**
 *
 * @file rpk_zlralloc.c
 *
 * RAPACK low-rank kernel routines
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Nolan Bredel
 * @author Abel Calluaud
 * @date 2024-02-08
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 *******************************************************************************
 *
 * @brief Allocate a low-rank matrix.
 *
 *******************************************************************************
 *
 * @param[in] M
 *          Number of rows of the matrix A.
 *
 * @param[in] N
 *          Number of columns of the matrix A.
 *
 * @param[in] rkmax
 *         @arg -1: the matrix is allocated tight to its rank.
 *         @arg >0: the matrix is allocated to the minimum of rkmax and its maximum rank.
 *
 * @param[out] A
 *          The allocated low-rank matrix
 *
 *******************************************************************************/
void
rpk_zlralloc( rpk_int_t M, rpk_int_t N, rpk_int_t rkmax, rpk_matrix_t *A )
{
    rpk_complex64_t *u, *v;

    if ( rkmax == -1 ) {
        u = malloc( M * N * sizeof( rpk_complex64_t ) );
        memset( u, 0, M * N * sizeof( rpk_complex64_t ) );
        A->rk    = -1;
        A->rkmax = M;
        A->u     = u;
        A->v     = NULL;
    }
    else if ( rkmax == 0 ) {
        A->rk    = 0;
        A->rkmax = 0;
        A->u     = NULL;
        A->v     = NULL;
    }
    else {
        rpk_int_t rk = rpk_imin( M, N );
        rkmax        = rpk_imin( rkmax, rk );

#if defined(RAPACK_DEBUG_LR)
        u = malloc( M * rkmax * sizeof(rpk_complex64_t) );
        v = malloc( N * rkmax * sizeof(rpk_complex64_t) );

        /* To avoid uninitialised values in valgrind. Lapacke doc (xgesvd) is not correct */
        memset( u, 0, M * rkmax * sizeof(rpk_complex64_t) );
        memset( v, 0, N * rkmax * sizeof(rpk_complex64_t) );
#else
        u = malloc( ( M + N ) * rkmax * sizeof(rpk_complex64_t));

        /* To avoid uninitialised values in valgrind. Lapacke doc (xgesvd) is not correct */
        memset( u, 0, ( M + N ) * rkmax * sizeof(rpk_complex64_t) );

        v = u + M * rkmax;
#endif

        A->rk    = 0;
        A->rkmax = rkmax;
        A->u     = u;
        A->v     = v;
    }
}

/**
 *******************************************************************************
 *
 * @brief Free a low-rank matrix.
 *
 *******************************************************************************
 *
 * @param[inout] A
 *          The low-rank matrix that will be desallocated.
 *
 *******************************************************************************/
void
rpk_zlrfree( rpk_matrix_t *A )
{
    if ( A->rk == -1 ) {
        free( A->u );
        A->u = NULL;
    }
    else {
        free( A->u );
#if defined(RAPACK_DEBUG_LR)
        free( A->v );
#endif
        A->u = NULL;
        A->v = NULL;
    }
    A->rk    = 0;
    A->rkmax = 0;
}
