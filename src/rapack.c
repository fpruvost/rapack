/**
 *
 * @file rapack.c
 *
 * RAPACK low-rank kernel routines
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Nolan Bredel
 * @date 2024-01-08
 *
 **/
#include "common.h"

const char *rpk_compmeth_shnames[RapackCompressMethodNbr] = {
    "SVD",
    "PQRCP",
    "RQRCP",
    "TQRCP",
    "RQRRT",
};

const char *rpk_compmeth_lgnames[RapackCompressMethodNbr] = {
    "Singular Values Decomposition",
    "Partial QR with Column Pivoting",
    "Randomized QR with Column Pivoting",
    "Truncated QR with Column Pivoting",
    "Randomized QR with QR rotation",
};

const fct_ge2lr_t rpk_ge2lr_functions[RapackCompressMethodNbr][4] = {
    { rpkx_sge2lr_svd,   rpkx_dge2lr_svd,   rpkx_cge2lr_svd,   rpkx_zge2lr_svd   },
    { rpkx_sge2lr_pqrcp, rpkx_dge2lr_pqrcp, rpkx_cge2lr_pqrcp, rpkx_zge2lr_pqrcp },
    { rpkx_sge2lr_rqrcp, rpkx_dge2lr_rqrcp, rpkx_cge2lr_rqrcp, rpkx_zge2lr_rqrcp },
    { rpkx_sge2lr_tqrcp, rpkx_dge2lr_tqrcp, rpkx_cge2lr_tqrcp, rpkx_zge2lr_tqrcp },
    { rpkx_sge2lr_rqrrt, rpkx_dge2lr_rqrrt, rpkx_cge2lr_rqrrt, rpkx_zge2lr_rqrrt },
};

const fct_rradd_t rpk_rradd_functions[RapackCompressMethodNbr][4] = {
    { rpkx_srradd_svd,   rpkx_drradd_svd,   rpkx_crradd_svd,   rpkx_zrradd_svd   },
    { rpkx_srradd_pqrcp, rpkx_drradd_pqrcp, rpkx_crradd_pqrcp, rpkx_zrradd_pqrcp },
    { rpkx_srradd_rqrcp, rpkx_drradd_rqrcp, rpkx_crradd_rqrcp, rpkx_zrradd_rqrcp },
    { rpkx_srradd_tqrcp, rpkx_drradd_tqrcp, rpkx_crradd_tqrcp, rpkx_zrradd_tqrcp },
    { rpkx_srradd_pqrcp, rpkx_drradd_pqrcp, rpkx_crradd_pqrcp, rpkx_zrradd_pqrcp }
};

static void rpk_lock_nop( const rpk_ctx_t * ctx, void *lock )
{
    (void)ctx;
    (void)lock;
    return;
}

const rpk_ctx_t rpk_ctx_default = {
    .minratio    = 1.0,
    .orthmeth    = 0,
    .use_reltol  = 0,
    .get_rklimit = rpkx_get_rklimit_end,
    .rpk_rradd   = NULL,
    .rpk_ge2lr   = NULL,
    .lock        = rpk_lock_nop,
    .unlock      = rpk_lock_nop,
};

const rpk_ctx_t *rpk_gctx = &rpk_ctx_default;
