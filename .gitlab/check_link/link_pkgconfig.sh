#!/usr/bin/env sh
###
#
#  @file link_pkgconfig.sh
#  @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @version 1.0.0
#  @author Mathieu Faverge
#  @author Abel Calluaud
#  @date 2023-12-14
#
# Check that linking with the project is ok when using pkg-config.
#
###
set -ex

static=""
project_path=""

while [ $# -gt 0 ]
do
    case $1 in
        --static | -static | -s )
            static="--static"
            ;;
        * )
            project_path=$1
            ;;
    esac
    shift
done

if [ -z $project_path ]
then
    echo """
usage: ./link_pkgconfig.sh path_to_project_install [--static|-static|-s]
   use the --static parameter if project is static (.a)
   env. var. CC and FC must be defined to C and Fortran90 compilers
"""
    exit 1
fi

export PKG_CONFIG_PATH=$project_path/lib/pkgconfig:$PKG_CONFIG_PATH

mkdir -p build
cd build

FLAGS=`pkg-config $static --cflags rapack`
if [[ "$SYSTEM" == "macosx" ]]; then
  FLAGS="-Wl,-rpath,$project_path/lib $FLAGS"
fi
LIBS=`pkg-config $static --libs rapack`
$CC $FLAGS ../test_rapack.c $LIBS -o link_rapack_c
./link_rapack_c -t 2 --lap 100

# FLAGS=`pkg-config $static --cflags rapackf`
# if [[ "$SYSTEM" == "macosx" ]]; then
#   FLAGS="-Wl,-rpath,$project_path/lib $FLAGS"
# fi
# LIBS=`pkg-config $static --libs rapackf`
# $FC $FLAGS ../../../wrappers/fortran90/examples/rapackf_driver.F90 $LIBS -o link_rapack_f
# ./link_rapack_f --lap 100
