/**
 *
 * @file rpk_zge2lr_qrrt.c
 *
 * RAPACK low-rank kernel routines
 *
 * @copyright 2016-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Gregoire Pichon
 * @author Esragul Korkmaz
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Nolan Bredel
 * @author Abel Calluaud
 * @date 2024-02-08
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 *******************************************************************************
 *
 * @brief Template to convert a full rank matrix into a low rank matrix through
 * QR decompositions
 *
 * This version is only used when rotational method is used.  Only difference
 * from rpk_zge2lr_qr is the V calculation part, That is: Instead of applying
 * inverse permutation on V, here inverse rotation is applied
 *
 *******************************************************************************
 *
 * @param[in] rrqrfct
 *          QR decomposition function used to compute the rank revealing
 *          factorization and create the low-rank form of A.
 *
 * @param[in] rklimit
 *          The maximum rank to store the matrix in low-rank format. If
 *          -1, set to min(m, n) / RAPACK_LR_MINRATIO.
 *
 * @param[in] m
 *          Number of rows of the matrix A, and of the low rank matrix Alr.
 *
 * @param[in] n
 *          Number of columns of the matrix A, and of the low rank matrix Alr.
 *
 * @param[in] Avoid
 *          The matrix of dimension lda-by-n that needs to be compressed
 *
 * @param[in] lda
 *          The leading dimension of the matrix A. lda >= max(1, m)
 *
 * @param[out] Alr
 *          The low rank matrix structure that will store the low rank
 *          representation of A
 *
 *******************************************************************************
 *
 * @return  TODO
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zge2lr_qrrt( const rpk_ctx_t *ctx,
                  rpk_zrrqr_rt_t   rrqrfct,
                  rpk_int_t        rklimit,
                  rpk_int_t        m,
                  rpk_int_t        n,
                  const void      *Avoid,
                  rpk_int_t        lda,
                  rpk_matrix_t    *Alr )
{
    int              ret, newrk;
    rpk_int_t        nb = 32;
    rpk_complex64_t *A  = (rpk_complex64_t *)Avoid;
    rpk_complex64_t *Acpy;
    rpk_int_t        lwork;
    rpk_complex64_t *work, *tau, *B, *tau_b, zzsize;
    rpk_int_t       *jpvt;
    rpk_int_t        zsize, bsize;
    rpk_complex64_t *zwork;
    rpk_fixdbl_t     flops;
    int              use_reltol = ctx->use_reltol;
    rpk_fixdbl_t     tol        = ctx->tolerance;

    char trans;
#if defined(PRECISION_c) || defined(PRECISION_z)
    trans = 'C';
#else
    trans = 'T';
#endif

    double norm = LAPACKE_zlange_work( LAPACK_COL_MAJOR, 'f', m, n,
                                       A, lda, NULL );

    if ( ( norm == 0. ) && ( tol >= 0. ) ) {
        rpk_zlralloc( m, n, 0, Alr );
        return 0.;
    }

    /* work */
    rklimit = ( rklimit < 0 ) ? ctx->get_rklimit( ctx, m, n ) : rklimit;
    if ( tol < 0. ) {
        tol = -1.;
    }
    else if ( use_reltol ) {
        tol = tol * norm;
    }

    ret = rrqrfct( tol, rklimit, nb,
                   m, n,
                   NULL, m, NULL,
                   NULL, n, NULL,
                   &zzsize, -1, norm );

    lwork = (rpk_int_t)zzsize;
    zsize = lwork;
    bsize = n * rklimit;
    /* Acpy */
    zsize += m * n;
    /* tau */
    zsize += n;
    /* B and tau_b */
    zsize += bsize + n;

#if defined(RAPACK_DEBUG_LR)
    (void)zsize;
    zwork = NULL;
    Acpy  = malloc( m * n * sizeof(rpk_complex64_t) );
    tau   = malloc( n     * sizeof(rpk_complex64_t) );
    B     = malloc( bsize * sizeof(rpk_complex64_t) );
    tau_b = malloc( n     * sizeof(rpk_complex64_t) );
    work  = malloc( lwork * sizeof(rpk_complex64_t) );
#else
    zwork = malloc( zsize * sizeof(rpk_complex64_t) );
    Acpy  = zwork;
    tau   = Acpy + m * n;
    B     = tau + n;
    tau_b = B + bsize;
    work  = tau_b + n;
#endif

    jpvt = malloc( n * sizeof(rpk_int_t) );

    /**
     * Backup A into Acpy to try to compress
     */
    ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', m, n,
                               A, lda, Acpy, m );
    assert( ret == 0 );

    newrk = rrqrfct( tol, rklimit, nb,
                     m, n,
                     Acpy, m, tau,
                     B, n, tau_b,
                     work, lwork, norm );
    if ( newrk == -1 ) {
        flops = flops_zgeqrf( m, n );
    }
    else {
        flops = flops_zgeqrf( m, newrk ) + flops_zunmqr( m, n - newrk, newrk, RapackLeft );
    }

    /**
     * It was not interesting to compress, so we restore the dense version in Alr
     */
    rpk_zlralloc( m, n, newrk, Alr );
    Alr->rk = newrk;

    if ( newrk == -1 ) {
        ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', m, n,
                                   A, lda, Alr->u, Alr->rkmax );
        assert( ret == 0 );
    }
    else if ( newrk > 0 ) {
        /**
         * We compute U and V
         */
        rpk_complex64_t *U, *V;
        rpk_int_t        d, rk = 0;

        U = Alr->u;
        V = Alr->v;

        /* Compute the final U form */
        ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'A', m, Alr->rk,
                                   Acpy, m, U, m );
        assert( ret == 0 );

        ret = LAPACKE_zungqr_work( LAPACK_COL_MAJOR, m, Alr->rk, Alr->rk,
                                   U, m, tau, work, lwork );
        assert( ret == 0 );
        flops += flops_zungqr( m, Alr->rk, Alr->rk );

        /* Compute the final V form */
        ret = LAPACKE_zlacpy_work( LAPACK_COL_MAJOR, 'U', Alr->rk, n,
                                   Acpy, m, V, Alr->rk );
        assert( ret == 0 );
        ret = LAPACKE_zlaset_work( LAPACK_COL_MAJOR, 'L', Alr->rk-1, Alr->rk-1,
                                   0.0, 0.0, V + 1, Alr->rk );
        assert(ret == 0);
        /*
         * Apply inverse rotations to V^T
         */
        {
            /*
             * Householders are applied in the reverse order of before
             */
            rk = ( Alr->rk / nb ) * nb;
            while ( rk >= 0 ) {
                d   = rpk_imin( nb, Alr->rk - rk );
                ret = LAPACKE_zunmqr_work( LAPACK_COL_MAJOR, 'R', trans,
                                           Alr->rk - rk, n - rk, d,
                                           B + rk * n       + rk, n, tau_b + rk,
                                           V + rk * Alr->rk + rk, Alr->rk,
                                           work, lwork );
                assert( ret == 0 );
                rk -= nb;
            }
        }
    }

#if defined(RAPACK_DEBUG_LR)
    if ( Alr->rk > 0 ) {
        int rc = rpk_zlrdbg_check_orthogonality( m, Alr->rk, Alr->u, m );
        if ( rc == 1 ) {
            fprintf( stderr, "Failed to compress a matrix and generate an orthogonal u\n" );
        }
    }
#endif

    free( zwork );
    free( jpvt );
#if defined(RAPACK_DEBUG_LR)
    free( Acpy );
    free( tau );
    free( B );
    free( tau_b );
    free( work );
#endif
    (void)ret;
    return flops;
}
