/**
 *
 * @file rpk_zlrscl.c
 *
 * RAPACK low-rank kernel to scale a low rank block.
 *
 * @copyright 2024-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Abel Calluaud
 * @date 2024-02-08
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "rpk_z.h"

/**
 *******************************************************************************
 *
 * @brief Scale a submatrix of a RAPACK matrix A.
 *
 *******************************************************************************
 *
 * @param[in] ctx
 *          The RAPACK context to use.
 *
 * @param[in] alpha
 *          The scaling factor.
 *
 * @param[in] m
 *          Number of rows of the submatrix A.
 *
 * @param[in] n
 *          Number of columns of the submatrix A.
 *
 * @param[in] lm
 *          Number of rows of the matrix B.
 *
 * @param[in] ln
 *          Number of columns of the matrix B.
 *
 * @param[inout] Ara
 *          The matrix A in RAPACK format.
 *
 * @param[in] i
 *          The column offset of the submatrix A.
 *
 * @param[in] j
 *          The row offset of the submatrix A.
 *
 *******************************************************************************
 *
 * @return The number of floating point operations performed.
 *
 *******************************************************************************/
rpk_fixdbl_t
rpkx_zlrscl( const rpk_ctx_t *ctx,
             rpk_complex64_t  alpha,
             rpk_int_t        m,
             rpk_int_t        n,
             rpk_int_t        lm,
             rpk_int_t        ln,
             rpk_matrix_t    *Ara,
             rpk_int_t        i,
             rpk_int_t        j )
{
    rpk_fixdbl_t     flops = 0.0;
    rpk_complex64_t *Ala;
    rpk_int_t        rklimit = -1;

    if ( ( alpha == 1.0 ) || ( Ara->rk == 0 ) ) {
        /*
         * Nothing to do when scaling by 1 or scaling a null matrix.
         */
        return 0.0;
    }

    if ( Ara->rk == -1 ) {
        /*
         * A is a full rank matrix.
         * Let's cast the u pointer and shift it to the head of the submatrix for scaling.
         */
        rpk_complex64_t *Ala = Ara->u;
        Ala += j * lm + i;
        rpk_zlascal( RapackUpperLower, m, n, alpha, Ala, lm );
        return (rpk_fixdbl_t)( m * n );
    }

    if ( ( i == 0 ) && ( j == 0 ) && ( m == lm ) && ( n == ln ) ) {
        /*
         * In this case, the full matrix is scaled, no need to uncompress,
         * the scaling can be apply solely on the v part.
         */
        rpk_zlascal( RapackUpperLower, Ara->rk, n, alpha, Ara->v, Ara->rkmax );
        return (rpk_fixdbl_t)( ln * Ara->rk );
    }

    /*
     * We need to uncompress the full matrix to scale the smaller matrix.
     */
    Ala = (rpk_complex64_t *)malloc( lm * ln * sizeof( rpk_complex64_t ) );
    flops += rpk_zlr2ge( RapackNoTrans, lm, ln, Ara, Ala, lm );

    /*
     * Apply scaling on the submatrix of A.
     */
    rpk_zlascal( RapackUpperLower, m, n, alpha, Ala + j * lm + i, lm );
    flops += (rpk_fixdbl_t)( m * n );

    /*
     * Try to recompress the matrix.
     */
    rpk_zlrfree( Ara );
    flops += ctx->rpk_ge2lr( ctx, rklimit, lm, ln, Ala, lm, Ara );

    free( Ala );

    return flops;
}
