/**
 * @file rpk_z.h
 *
 * RAPACK double complex precision internal header.
 *
 * @copyright 2023-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Esragul Korkmaz
 * @author Nolan Bredel
 * @author Abel Calluaud
 * @date 2024-03-11
 * @precisions normal z -> c d s
 *
 */
#ifndef _rpk_z_h_
#define _rpk_z_h_

/**
 *
 * @addtogroup kernel_lr
 * @{
 *    This module contains all the low-rank kernels working on rpk_ctx_t
 *    matrix representations.
 *
 *    @name RapackComplex64 low-rank kernels
 *    @{
 */
/**
 * @brief Function to get a workspace pointer if space is available in the one provided
 * @param[inout] params  The parameters structure for rpk_zgemm family functions
 * @param[in]    newsize The required workspace size in number of elements
 * @return The pointer to the workspace if enough space available, NULL otherwise.
 */
static inline rpk_complex64_t *
rpk_zgemm_getws( rpk_zgemm_t *params, ssize_t newsize )
{
    rpk_complex64_t *work = NULL;
    if ( ( params->lwused + newsize ) <= params->lwork ) {
        work = params->work + params->lwused;
        params->lwused += newsize;
    }
    /* else */
    /* { */
    /*     if ( (params->work == NULL) || (params->lwused == 0) ) */
    /*     { */
    /*         params->work = realloc( params->work, newsize * sizeof(rpk_complex64_t) ); */
    /*         params->lwork  = newsize; */
    /*         params->lwused = newsize; */
    /*         work = params->work; */
    /*     } */
    /* } */
    return work;
}

/**
 *    @}
 *    @name update_fr Functions to perform the update on a full-rank matrix
 *    @{
 */
rpk_fixdbl_t rpkx_zfrfr2fr( const rpk_ctx_t *ctx, rpk_zgemm_t *params );
rpk_fixdbl_t rpkx_zfrlr2fr( const rpk_ctx_t *ctx, rpk_zgemm_t *params );
rpk_fixdbl_t rpkx_zlrfr2fr( const rpk_ctx_t *ctx, rpk_zgemm_t *params );
rpk_fixdbl_t rpkx_zlrlr2fr( const rpk_ctx_t *ctx, rpk_zgemm_t *params );

/**
 *    @}
 *    @name update_lr Functions to prepare the AB product for an update on a low-rank matrix
 *    @{
 */
rpk_fixdbl_t rpkx_zfrfr2lr( const rpk_ctx_t *ctx, rpk_zgemm_t *params, rpk_matrix_t *AB, int *infomask, rpk_int_t Kmax );
rpk_fixdbl_t rpkx_zfrlr2lr( const rpk_ctx_t *ctx, rpk_zgemm_t *params, rpk_matrix_t *AB, int *infomask, rpk_int_t Brkmin );
rpk_fixdbl_t rpkx_zlrfr2lr( const rpk_ctx_t *ctx, rpk_zgemm_t *params, rpk_matrix_t *AB, int *infomask, rpk_int_t Arkmin );
rpk_fixdbl_t rpkx_zlrlr2lr( const rpk_ctx_t *ctx, rpk_zgemm_t *params, rpk_matrix_t *AB, int *infomask );

/**
 *    @}
 *    @name add_lr Functions to add the AB contribution in a low-rank format to any C matrix
 *    @{
 */
rpk_fixdbl_t rpk_zlrorthu_fullqr( rpk_int_t        M,
                                  rpk_int_t        N,
                                  rpk_int_t        rank,
                                  rpk_complex64_t *U,
                                  rpk_int_t        ldu,
                                  rpk_complex64_t *V,
                                  rpk_int_t        ldv );
rpk_fixdbl_t rpk_zlrorthu_partialqr( rpk_int_t        M,
                                     rpk_int_t        N,
                                     rpk_int_t        r1,
                                     rpk_int_t       *r2ptr,
                                     rpk_int_t        offx,
                                     rpk_int_t        offy,
                                     rpk_complex64_t *U,
                                     rpk_int_t        ldu,
                                     rpk_complex64_t *V,
                                     rpk_int_t        ldv );
rpk_fixdbl_t rpk_zlrorthu_cgs( rpk_int_t        M1,
                               rpk_int_t        N1,
                               rpk_int_t        M2,
                               rpk_int_t        N2,
                               rpk_int_t        r1,
                               rpk_int_t       *r2ptr,
                               rpk_int_t        offx,
                               rpk_int_t        offy,
                               rpk_complex64_t *U,
                               rpk_int_t        ldu,
                               rpk_complex64_t *V,
                               rpk_int_t        ldv );
/**
 *    @}
 * @}
 *
 * @addtogroup kernel_lr_debug
 * @{
 *    This is the debug routines for the low rank kernels.
 *
 *    @name RapackComplex64 low-rank debug functions
 *    @{
 */
void rpk_zlrdbg_printsvd( rpk_int_t M, rpk_int_t N, const rpk_complex64_t *A, rpk_int_t lda );

int rpk_zlrdbg_check_orthogonality( rpk_int_t              M,
                                    rpk_int_t              N,
                                    const rpk_complex64_t *A,
                                    rpk_int_t              lda );

int rpk_zlrdbg_check_orthogonality_AB( rpk_int_t              M,
                                       rpk_int_t              NA,
                                       rpk_int_t              NB,
                                       const rpk_complex64_t *A,
                                       rpk_int_t              lda,
                                       const rpk_complex64_t *B,
                                       rpk_int_t              ldb );

/**
 *    @}
 * @}
 */

#endif /* _rpk_z_h_ */
